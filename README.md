THIS IS THE OLD DOCUMENTATION. IT'S BEEN DEPRECATED.

THE NEW DOCUMENTATION LIVES AT [https://gitlab.com/fablabbcn-projects/learning/educational-docs]


<s>
# Fablab Barcelona Local Documentation

This repository contains the main source of documentation for Fablab Barcelona local Fabacademy program. This is an iterative documentation, so it's updated every year. 

View this documentation:
https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/

Fabacademy 2020 documentation: 
http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/
</s>