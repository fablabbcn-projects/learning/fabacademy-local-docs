# VIDEO TUTORIALS ON MACHINE/EQUIPMENT

This section is a compilation of the video tutorials we produced to easy up some time our repetitive work explaining our most used machines.


#### How to use the step CNC Raptor X-SL - [CNC]

<iframe width="640" height="350" src="https://www.youtube.com/embed/wKqFPtVuM48" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### How to use the Roland milling CNC SRM 20 - [Precision CNC]

<iframe width="640" height="350" src="https://www.youtube.com/embed/rFRuc0VPWDM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### How to use the Silhouette Cameo - [Vynil Cutter]

<iframe width="640" height="350" src="https://www.youtube.com/embed/oVkKy82pk-Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### How to use the GX 24 - [Vynil Cutter]

<iframe width="640" height="350" src="https://www.youtube.com/embed/8oCOzy_Zx2o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### How to use the TROTEC 400 - [Laser Cutter]

<iframe width="640" height="350" src="https://www.youtube.com/embed/5GTeIffOlzg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
