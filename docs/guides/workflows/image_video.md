# Image and video flows

VIDEO, AUDIO and IMAGE EDITING *in the command line* :)

![](http://www.brianlinkletter.com/wp-content/uploads/2015/12/logo_liquid-60.gif)

# VIDEO EDITING WITH FFMPEG

**FFMPEG** is a free software project that produces libraries and programs for handling multimedia data. It can record, convert and stream digital audio and video in numerous formats. FFmpeg can be used to convert many multimedia formats to one another. It is a command line tool that is composed of a collection of free software / open source libraries. The name of the project comes from the MPEG video standards group, together with "FF" for "fast forward".

## 1. Install FFMPEG

It's cross platform. Please visit the [official website](https://www.ffmpeg.org/download.html) and follow the steps to install in MacOS, Windows or Linux.

## 2. Basic FFMPEG commands

*note: Execute commands in Terminal*

### Convert a video to any format
```
ffmpeg -i input.mp4 output.avi
```
- Converts an MP4 video file to an AVI video file
- If you want to know which formats and codecs come with ffmpeg execute one of these commands:

```
ffmpeg -codecs
ffmpeg -formats  
```

### Compress a video with a good quality

```
ffmpeg -i input.mp4 -c:v libx264 -crf 24 -preset slow output.mp4
```

- *Understand the command:*

    - i `<input_file>.<format>`
    - c codec (a:audio v:video)
    - crf is the quality, lower means better quality, 15-25 is usually good
    - preset `<compression quality>`

- The range of the CRF scale is 0–51, where 0 is lossless, 23 is the default, and 51 is worst quality possible. A lower value generally leads to higher quality, and a subjectively sane range is 17–28. Consider 17 or 18 to be visually lossless or nearly so; it should look the same or nearly the same as the input but it isn't technically lossless.
The range is exponential, so increasing the CRF value +6 results in roughly half the bitrate / file size, while -6 leads to roughly twice the bitrate.
Choose the highest CRF value that still provides an acceptable quality. If the output looks good, then try a higher value. If it looks bad, choose a lower value.

- A **preset** is a collection of options that will provide a certain encoding speed to compression ratio. A *slower* preset will provide better compression (compression is quality per filesize). This means that, for example, if you target a certain file size or constant bit rate, you will achieve better quality with a slower preset. Similarly, for constant quality encoding, you will simply save bitrate by choosing a slower preset.

### Extract images frame by frame from a video file

Very useful for *[photogrammetry](http://www.photogrammetry.com/)* !

![](https://i.imgur.com/TyEaAaJ.png)

```
ffmpeg -i coding_robots.mp4 thumb%04d.jpg
```

- *Understand the command:*

    - i with the input file `coding_robots.mp4`
    - `thumb%04d.jpg` this is the output file. In this case it is a jpg file so ffmpeg will output jpg image files. As we have specified thumb%04d.jpg with the pattern "%04d" inside the file name, FFMPEG will extract a serie of images thumb0000.jpg, thumb0001.jpg, thumb0002.jpg, thumb0003.jpg and so on... We are indicating ffmpeg to output files with 4 decimal numbers with the "%04d" pattern but we can also use more numbers (for example to use 6 decimal numbers we can change the pattern to "%06d". My recommendations is use .jpg but another formats like **png or bmp** are also allowed.

![](https://i.imgur.com/oiucZuc.png)

**Extract only one frame**
```
ffmpeg -i coding_robots.mp4 -ss 00:01:07.000 -vframes 1 oneframe.jpg
```
![](https://i.imgur.com/RNaxV06.jpg)

- *Understand the command:*

    - ss we are using this parameter with a time (00:01:07.000 is the minute 7second 7 from the video in this case). This parameter makes ffmpeg to seek for that second to start its processing, so it will extract frames from that moment.
    - vframes with this parameter we are telling ffmpeg the number of frames to extract (1 in this case). FFMPEG will extract only one image and it will use oneframe.jpg as output file.

**Extract an image every X seconds**

- Get one frame every second from the video file called coding_robots.mp4

```
ffmpeg -i coding_robots.mp4 -vf fps=1 coding%04d.jpg
```

- *Understand the command:*

    - vf this parameter is used to apply video filters with ffmpeg. We can use a lot of filters. In this example we are using "fps=1" so ffmpeg will filter the video and extract one image (1 frame per second) for the output.

### Inserting a Watermark

You can apply filters to whole audio or video streams or only to certain parts, use them to merge several streams into one in interesting ways, and do much more.

Next example show you how to use a logo to watermark a video. It's the logo ( [Future Learning Unit ](https://twitter.com/futurelearningu) ):

![](https://i.imgur.com/SW7bAqo.png)

```
ffmpeg -i coding_robots.mp4 -i logo_flu.png -filter_complex "overlay" -codec:a copy coding_marked.mp4
```
FFmpeg takes two inputs, the coding_robots.mp4 video file and the logo_flu.png file, and outputs them together – the second placed on top of the first – to coding_marked.mp4

![](https://i.imgur.com/iT0I5Q5.png)

![](https://i.imgur.com/DcIq89n.jpg)

The logo is in the upper-left corner. If you prefer in the lower right:

```
ffmpeg -i coding_robots.mp4 -i logo_flu.png -filter_complex "overlay=W-w-10:H-h-10" -codec:a copy coding_marked.mp4
```

W is the width of the first input (the bottom layer), and w is the width of the second input (the top layer). This means that W-w-10 will place the top overlay layer 10 pixels from the left-most edge of the bottom video layer. The same goes for H-h-10, but in the vertical axis.

![](https://i.imgur.com/28jykJ1.jpg)

If the logo is too big, you can scale too:

```
ffmpeg -i example.mp4 -i LM_logo.pngcoding_robots.mp4 -i logo_flu.png -filter_complex "[1:v] scale=150:-1 [ol], [0:v] [ol] overlay=W-w-10:H-h-10" -codec:a copy coding_marked.mp4
```

In the previous command you tell scale to make the stream 150 pixels wide and to scale its height proportionally.

# IMAGE EDITING WITH IMAGEMAGICK

**Imagemagick** is a robust collection of tools and libraries offered under a usage license to read, write, and manipulate an image in many image formats (over 87 major formats) including popular formats like TIFF, JPEG, PNG, PDF, PhotoCD, and GIF. With ImageMagick you can create images dynamically, making it suitable for Web applications. You can also resize, rotate, sharpen, color reduce, or add special effects to an image or image sequence and save your completed work in the same or differing image format. Image processing operations are available from the command line, as well as through C, C++, Perl, or Java programming interfaces.

## 1. ImageMagick can do

- Convert an image from one format to another.
- Resize, rotate, sharpen, color reduce, or add special effects to an image.
- Create a montage of image thumbnails.
- Turn a group of images into a GIF animation sequence.
- Create a composite image by combining several separate images.
- Draw shapes or text on an image.

## 2. Install ImageMagick

It's cross platform. Please visit the [official website](https://imagemagick.org/script/download.php) and follow the steps to install in MacOS, Windows or Linux.

## 3. ImageMagick basic commands

### Convert an image from one format to another

```
convert [ options ... ] input_file output_file

convert high1.jpg high1.png   ->1 file
convert *.jpg *.png -> a bath of images

```

### Turn a group of images into a .GIF animation sequence

`convert -resize 120 -delay 15 -loop 0 *.jpg animation.gif`

### Create thumbails from batch images

```
convert -thumbnail '500x500>' *.jpg thumb.jpg

```

### Images file size

```
BATCH IMAGES
mogrify -path ../ImageMagick/ -define jpeg:extent=2MB

1 IMAGE
mogrify high2.jpg -define jpeg:extent=2MB high2.jpg
```

### Resize image dimension and change format

```
convert *.jpg -resize 1024 .png

convert original_file.jpg -resize "160x160!" resized_file.jpg (ignore aspect ratio)
```

### Rotate an image

```
convert high3.jpg -rotate -90 rotated_ccw.jpg
```
