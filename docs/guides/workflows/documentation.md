# What, why, how, where and when to document

!!! warning ""

    This text it's a copy to the **fabzero** course developed by [Francisco Sanchez](http://fabacademy.org/archives/2014/projects/sanchez.francisco/index.html)

> Learning by doing: Get a notebook for Fab Zero program and write down your first entry. Answer in one sentence: Why are you here?

The most important thing to understand when it comes to documenting your work is that you are not writing a tutorial, a step by step guide or telling others what to do. You are telling your personal story, your journey into the learning process. While you still have to provide details about your findings, you have to become a storyteller.

Documenting is not about the final result, it's about the learning journey. What was the goal, what did you had to research, how did you approach the solution, and what was the outcome. Document both failures and successes. There is a tendency to hide failures inherited from our educational system. In traditional education failure is negative. But there is much more you can learn from documenting your failures than documenting your sucesses.

The simplest explanation I can give you about why do you have to document is that you have less recall memory than a goldfish. Actually you will remember less than 60% after just 20 minutes (read more). Document what you do while you do it. Exercise the habit of documenting every day.

!!! Note
    You can find more information [here](https://gitlab.fabcloud.org/fabzero/fabzero)

## Tips

> It’s all about **documentation**. Does not matter what you did, only what you documented

**Document while you work**

- If you are in a trouble, it will help your local or remote tutor, a friend or the class to understand what you did.
- It helps yourself during your research to keep track of what you did
- It helps you to acquire a good habit useful in the future, not only for Fab
Academy
- *Tell your story, not a step by step tutorial*. Is what you did, not what to do

**Grading criteria and final project requirements**

- [Assessment](http://fabacademy.org/2020/docs/FabAcademy-Assessment/)

**Documenting in Markdown**

- Rising tendence
- Easy to write, fast and clean compared to HTML
- 10 minutes tutorial http://markdowntutorial.com/
- Ubuntu app ReText
- Online editor and publisher https://stackedit.io
- Online HTML to Markdown converter
http://domchristie.github.io/tomarkdown/
- Online Markdown to HTML converter http://dillinger.io/ (keep both .md and
.html files always)

**Photography**

- Blurred images
- Size (web size). There is something even worse than a blurred image, and it
is a 16 Mpx blurred image.
- Crop images
- Scale down images in right click menu in Nautilus: nautilusimageconverter
- Backgrounds
- Composition: Rule of thirds
- Lighting. Beware of flash.
- Prepare the scene

**Videos**

- Do not upload videos in the archive. Host them in Vimeo or Youtube
- Consider GIF for short videos (~5sec)

**Mockups**

- Useful for final project presentation or interface application week
- http://www.mockupworld.co/allmockups/

**Principles of good design**

- Simplicity http://vanseodesign.com/webdesign/simplicity/
- Typography http://fontpair.co/
- Color harmony http://paletton.com/

## Evaluation comments

![](../../assets/documentation-c60977a8.png)

### Project Management and Principals and Practices

![](../../assets/documentation-5271a346.png)
![](../../assets/documentation-c765e60d.png)

* Explain how did you structure your web, folders, files, etc....
* Add link to the programs and people you mention in your web
* Explain which strategies you use to manage the size of your web. Images, files, etc…
* Explain which strategies you use to manage the size of your web. Images, files, etc...For example: explain imagemagick or ffmpeg, how to optimize images with gimp/photoshop or how you check the size of your webpage with this command line du -sk * | sort -n
* TIP! I recommend you to change the  "z-index Property" in order to have the header in front of the content. Here a link: https://www.w3schools.com/cssref/pr_pos_z-index.asp
* Explain the meaning of git commands.
* Add screenshots from your computer of the git push process
* Explain how you generate and why you created the YML file in Gitlab
* Which editor are you using for coding the website ? Please, explain it and add a link to it.
* Explain what do you understand as a version control protocol (git) and why we use it in fab academy
* Your Repo it's about 400mb. It's recomended to not exced 10mb per week. This make around 200-250mb in total. You can store bigger files outside of the week, compress 3D files or resies image quality. Explain which strategies you use to manage the size of your web. Images, files, etc…


### Computer-aided design

![](../../assets/documentation-d95a601d.jpg)
![](../../assets/documentation-6a9b2b22.jpg)

* Missing 2d design files.
* You should explain better what did you test, what you learn, and what are your conclusions.
* You should explain better what what you learn, and what are your conclusions. For example, why Solidworks is a parametric 3D software?
* You must explain deeper the key commands and tools you used to generate the files, steps, etc.
* I like a lot your programs analisis! Very good.
* Missing files.
* Would be nice to include the image & Illustrator file, as well as in the original editable format (antimony, solidworks, eagle, CAD, svg, etc).
* Not clear what did you design during week.
* No 2D software?? You should document at list one.
* What happen with Grasshopper? If you are not gonna update, better remove
* All your design and fabrication files should be in the archive (Gitlab). Google drive it’s not allowed as a storage service
* I miss more deeper reflexion about design possibilities, learning outcomes...

### Computer-Controlled Cutting

![](../../assets/documentation-407defc6.jpg)
![](../../assets/documentation-31465cbb.jpg)

**GROUP ASSIGNMENT**

- You should write the power, speed, frequency setting for every test… But in general very good!
- Not clear what are the group assignments and wich your assignments. Are the same?
- List your group team, and add the links to their webpage
- Missing files.
- Explain the conclusions. I'm missing your own learning comments
- List the basic characteristic of the machine you are working with (name, dimension, power, etc..). Add link to software and machine webpage. Also to the manual.
- Add link to the test files (pngs) or link to the archive page.
- Add pictures of the results
- This comment maybe will apply to all the group assignments. Would be nice if you can add the link to software and machine webpage. Also to the manual.

**INDIVIDUAL ASSIGNMENT**

* Missing parametric design.
* Explain better how are the steps in the camstudio from the vinyl cutter (adjust blade, set the material, update dimensions through the serial port….)
* Your design is not parametric. Re-design and explain how you parametrically design the files.
* Explain you did group assignment alone.
* External Files should be linked directly.
* Missing photos of “laser cut frame”
* Group assignment: Add link to software, machine or people you mention. Ej: Speedy 400.
* I understand you will add later but you are missing all pictures ;)
* Group assignment: Nice machine documentation. I like you add a link to the user manual, but are super hard to see! can you change you css to make more visible the link ;)
* Group assignment: Missing keft laser cut test.
* Missing Vinyl assignment
* Missing Pressfit construction kit
* Good job explaining the problems with freecad.
* Remember to upload the final version of the files that you have been doing for the different programs. Even if they are test or not definitive.
* Add a screenshot of the Changing parameters to show how did you paramtrize your design.
* Missing a final picture with your pressfit construction kit....
* You must explain the key commands and tools you used to generate the Ilustrator design, steps, etc.
* Design files must be present in the archive in generically readable forms such as DXFs, STLs, etc., as well as in the original editable format (antimony, solidworks, eagle, CAD, etc). Is missing Fusion file and the svg or dxf version of AI file.
* Is your design parametric? Explain how you parametrically design the files
* Which settings you use to cut your press fit design. Which Kerf you have?
* Missing kerf for laser cut. Can you explain what it is and which was the ones apply to your material?
* Can you add the files you used to test the lasercut?
* Would be nice to have a Conclusion part, in each week where you summarize the learning outcomes out of your practice.

### Electronics Production

![](../../assets/documentation-979e4b2a.jpg)

* Group assignment: List your group team, and add the links to their webpage.
* Group assignment: List the basic characteristic of the machine you are working with (name, dimension, power, etc..). Add link to software and machine webpage. Also to the manual.
* Group assignment: Did you have the opportunity to work in the group assignment? if yes please document it?
* Group assignment: You should extract conclusion testing the Roland modela.
* Group assignment: This comment probably will apply to all the weeks. I like you link Group project to your doc. But I miss reflection out of it. What did you learn out of the exercises? What conclusion do you extract?
* You should check the links of your images, all of them are broken...
* Missing screenshots of the programming process, together with the commands
* Missing final board picture.
* Included a ‘hero shot’ of your board
* Needs documentation.
* Did you have any problems? Drivers problems, solder problems, etc ... It is as important to document the successes as the errors. Remember that if you document errors, you have to document how you tried to solve them.
* Nice gif images! I love them. But you should explain also with words ;)
* Explain how you generated the CAM files for the SRM-20, with words and pictures. Also, include those files in your archive.
* Explain the differences between MODS and Fabmodules.
* We don't use FR4 in the Fablabbcn, we use FR1. Please take a look of the refenrece (https://support.bantamtools.com/hc/en-us/articles/115001671734-FR-1-PCB-Blanks-) and check Neil's class again, and correct your documentation.
* No content…
* I recommend you take a look at [Assignments and Assessment doc](http://docs.academany.org/FabAcademy-Assessment/_book/)
* Missing screenshot where shows the computer recognize the FabISP
* Explain what you learn from the group assignment test.
* Please add link to person you mention (ex:Kris)
* Will be nice to see a picture of the problem "small amount of solder in one place"
* In general the work is done and it is seen in the images, but you need to be much more specify in your documentation.


### 3D Scanning and Printing

![](../../assets/documentation-8d551037.png)
![](../../assets/documentation-87c5c1d1.jpg)

* Group assignment: List the basic characteristic of the machine you are working with (name, dimension, power, etc..). Add link to software and machine webpage. Also to the manual. If you don't find the webpage you can link to the [Fablabbcn wiki](https://wiki.fablabbcn.org/RepRapBCN)
* Group assignment: How did you send the GCODE to the printer? Did you send by serial, SD card, or wifi? If it's wifi, which software did you use to communicate with the 3d printers? Add a link [Octoprint](https://octoprint.org/)
* Group assignment: Explain the advantages and limitations of 3D printing.
* Group assignment: List your group team, and add the links to their webpage.
* Design files are missing
* Scanning process is missing
* 3D printing documentation is missing ( just screenshots )
* Show how you designed and made your object and explained why it could not be made subtractively is missing.
* Explain the advantages and limitations of 3D printing.
* For your individual assignment: nice exercise but for this assignment you should design your 3d printing object not one from thingiverse.
* Explain why your 3d object could not be made subtractively .
* Upload your design files to the gitlab repository.
* Add some screenshots of the CURA process.
* Explain the advantages and limitations of the scanning technology.
* Did you use Blender ( or other software like Meshmixer ) for work on the mesh after the scanning process ?
* We already commented but I really love your design. Congrats! Also, the light is an extra point!! ;)
* Show how you designed and made your object (screenshots of fusion) and explain why it could not be made subtractively is missing.
* Upload your design files to the gitlab repository.
* Add some screenshots of the CURA process and explain the basic settings: Infill, raft, supports, Layer hight, etc... (You just explain layer height and speed)
* Explain basic configuration and workflow with skanect. How did you post process your mesh?
* Explain the basic CURA settings configuration you use to print your miniyou.
* explains the basic configuration you used for printing your object (layer height, speed, infill, structural support, etc …
* Extra credit: Explain how it works Octroprint
* Upload your design files to the gitlab repository, in the original editable format (antimony, solidworks, eagle, CAD, etc) and readable forms such as DXFs, STLs, etc.


### Electronic Design

![](../../assets/documentation-9a05b29e.jpg)

* Group assignment: explain any of the test equipment of the Lab, for example the multimeter.
* Did you try Kicad ? Add links to Eagle and Kicad.
* Explain the workflow after design your pcb in Eagle with words and screenshots: .png (traces, outlines), milling and soldering.
* Add a screenshot of your pcb.
*  Could you add some screenshots of the routing in Kicad ?
* Could you add some screenshots of the milling process you used with the Modela MDX-20 ?
* Did you test it ? Explain how you did it.
* Upload your files and add a link to them.
* Missing Eagle files and png files.
* Even if you have explained in the electronic production week it would be nice to remember how you generated the cam files for the SRM-20. Also, include those files in your archive.
* No problems soldering? No comments on that part?
* Explain how did you calculate the resistor for the LED.
* I really like the conclusion part! Nice.
* Did you test it ? Explain how you did it.
* Upload your files and add a link to them.
* Explain basic workflow of Eagle.
* Explained how you worked with design rules for milling (DRC in EagleCad and KiCad)
* 16mil is not only to be sure, it is also because it corresponds to the tool diameter 1/16 and thus we guarantee that we will be able to pass everywhere when we do the traces
* I love the Frankenstein of your first Hello board intent! congrats
* The download link seems to be broken, or you moved the files to another side.
* Explain how did you add the fab library in eagle

### Computer-controlled machining

![](../../assets/documentation-51fbe019.gif)

* The editable files should be in the corresponding week, and if you have in another week, you should add a link to that week and explain it.
* Explain how did you calculate the chipload
* Very nice step by step documentation, but you should also write the general setting for cnc (feeds and speeds, depth…..)
* Explain a bit more how to set up the milling machine. (xy zero, Z zero, etc..)
* List the basic characteristic of the machine you are working with (name, dimension, Working area, etc..). Add link to software and machine webpage. Also to the manual.
* I love the summary of the most important points of the milling process
* Nice rule: Check -> doublecheck -> recheck!
* Amazing bonus the documentation of Good Enough CNC and Fusion CAM process !! Thanks!
* Could you include the generically readable forms such as DXFs for the Rhino files?
* Explain how you calculated the Chipload calculation. [Take_a_look] (https://pub.pages.cba.mit.edu/feed_speeds/)
* What tolerance/margin did you use?
* I did not see any errors, it’s hard to believe …😉 I encourage you to document the errors since you can get them as much as the hits. Especially if you explain how you solved it.
* Do the Gcode files generated by fusion work well without any change? If not, can you explain what you changed?
* Missing files
* I really like your introductions on the key concepts that are going to be treated in the assignment! Congrats!! Maybe sometimes I miss more images that support the text ;)
* Explain how you designed your object in fusion 360, with images and text;)
* Did you do a test to check the tolerances in the press fit solution? What conclusion did you get?
* I would like to see more information about milling strategies in Fusion 360.
* Design and Fabrication design files should be in gitlab repository.
* Very nice step by step documentation! Also the Chipload calculation! well done
* Add link to software and machine webpage you are mentioning.
* I did not see any errors, it’s hard to believe …😉 I encourage you to document the errors since you can get them as much as the hits. Especially if you explain how you solved it.
* - Missing readable forms files such as DXFs, STLs...think that there are a lot of people who do not have access to Rhino, and maybe they want to replicate your project;)
* I would like to see more information about design process.
* Congrats! Amazing week!


### Embedded Programming

![](../../assets/documentation-53ed61f9.jpg)

* Nice debugging process with the resistor led
* Described the programming process you used.
* Explain the key point of your code. How is it structured (loops, variables, etc). Why are you using softserial library?A nice workflow is to add comments to the code in order to be easy to read for the non-expert people.
* Describe how did you install the Arduino library for Attiny, or add the link to some tutorial.
* Identify relevant information in a microcontroller datasheet add link and explain what it was useful for you in order to program the board
* What questions do you have? What would you like to learn more about?
* The download link seems to be broken, or you moved the files to another side.
* Recommendation: Instead of adding screen captures of the Arduino codes, it is much more optimal if you insert them directly into the HTML webpage as text. In this way it is easier to copy/paste, for people who want to replicate your work.

### Molding and Casting

![](../../assets/documentation-dcdcca4a.jpg)
![](../../assets/documentation-83ce3a0a.jpg)

* linked to the material safety data sheet (MSDS) and technical data sheet (TDS) for the resins that you’re using and explain the key futures of this materials? Curating time, melding temperature…
* [Bismuth](https://www.rotometals.com/lead-free-bullet-casting-alloy-88-bismuth-12-tin/)
* Can you add the link to FormSil 25 webpage and the link to the MSDS and TDS, and explain the key futures of this materials? Curating time, melding temperature…
* Which resin did you use? Can you add also the technical information?
* linked to the material safety data sheet (MSDS) and technical data sheet (TDS) for the resins that you're using
* Add a link to the wax model you used for the mold, and explain the basic characteristics of the material (dimensions, hardness, melting point ...) [Ferris wax] (http://www.freemanwax.com/ferris-file-a-wax-carving-milling-waxes.html)
* Very good idea to use pine resin as glue!
* Explain better how you generated the 3d milling strategies in Rhinocam. What Feeds and Speeds did you use, etc ...
* What cement do you use? Could you indicate basic features, mixing ratio, and the link to the TDS?
* Explain how you did the design of the mold in Blender. Basic tools that you used for modeling.
* What design criteria did you apply? (Minimum dimensions, tolerances, wall angles, etc.)
* Add link to the "Modela player 4" program
* It would be nice if you can add screenshots of the process of creating 3d strategies how to establish origin.
* It would be good to mention that the process of establishing the origin XYZ is the same as when we make the PCBs, manually and dropping the tool.
* Can you add an image of the vacuum process, to extract the air bubbles?
* Did you have time to cast in metal?
* Would be nice to indicate that during WIldcar week you cast with bioplastic, and add a link to the webpage.
* Recommendation: In the files, it seems that there is more than one version of the same design. Could you leave the final version alone?
* I am fascinated with the final shape that creates soy sauce (a heart!) First I thought that it was designed on purpose, but reading the documentation I have seen that it has been accidental! Amazing! Brutal!
* Add a link to software and machine webpage you are mentioning.
* Add a link to the wax model you used for the mould, and explain the basic characteristics of the material (dimensions, hardness, melting point ...) [Ferris wax] (http://www.freemanwax.com/ferris-file-a-wax-carving-milling-waxes.html)
* Unfortunately, in order to pass the assignments, it's mandatory to go through the 3 step process (design, 3D milling, creating mould (Soft part), casting (hard part)). It does have to be a complex design or being milled in wax you can use other materials as Soup or hight density foam...

### Input Devices

![](../../assets/documentation-75b387cf.jpg)

* Described your design and fabrication process using words/images/screenshots. (eagle, milling, soldering)
* It would be nice to see your comments about neil’s code. How do you understand it, or if you modified it.
* You explained some changes you did in other to adapt to the new python version and your usb port, but you need to explain deeper how neil’s code works. How is structured and how do you understand the logics of the code.
* If you didn't change the Neils code you need to explain deeper how it works. How is structured and how do you understand the logic of the code.
* The download link seems to be broken.
* I really like the idea of potentiometer as and input. Would be nice to measure the range resistance it has. Normally is 10K ohms but you should double check with datasheet or voltmeter.
* Explain what are the multiple tests you did with Arduino? What code did you use? What conclusions did you get?
* Described your design and fabrication process using words / images / screenshots. (eagle, milling, soldering)
* Explain the code with which you programmed your pcb. Did you use the neil code?
* Explains the programming process. Did you use Arduino IDE or Command line?
* Missing eagle and code files
* Super nice explanation about fuses.
* Add Guillem fabacademy webpage link.
* I didn't know about Platformio maybe would be nice to explain a bit more about how the program works, and how do you use it for the assignment.
* Maybe you show in networking week but will be nice to have a short video that shows how you read the sensor.
* I know maybe it’s obvious but would be nice to include some pictures of programming process

### Output Devices

* Missing eagle files
* If you didn’t change the Neils code you need to explain deeper how it works. How is structured and how do you understand the logic of the code. It's not enough to replicate the others work. You should add something.
* I know you work in the LCD screen output, can you document, please?
* Same comment as before. Direct the download links to the specific folders, please!
* Explain the key point of your code. If you used Neil’s code, explain what did you change or how did you understand the code.
* Do not repeat information (no sense). You already add the information on Attiny 44 in the Embedded Programming week , it will be enough to put the link to that week.
* It would be good if you specified what information was relevant to you to program the motor Stepper.
* Show some pictures of how did you design it(eagle).
* Explain a bit more how did you program the board. Seems you did by terminal (Neil’s code) and another with arduino? Did you use Fabisp or AvrmkII? Also what did you learn from Neil’s code?
* Did you have problems?
* I am sorry if I made your task difficult during the assignment, due to the lack of mosfets N. ;(
* Only as a detail, it seems that the link of the last photo is broken. Check it out please.

### Wildcard

* Explain how did you generate the milling strategies in Rhinocam for this material and project, Feeds and Speeds, tolerances…
* Includes the name and resin link you have used. What characteristics does the material have? What percentage of the mixture is there between the parts?
* Explain a little more about how the vacuum bag works. What components / materials are needed? How do they connect?
* Nice to see you did a lot of test with materials and techniques!
* It’s missing a crucial part of the assignment, we should incorporating computer-aided design in the digital fabrication process. To count as digital fabrication the workflow should pass through design in a computer and some kind of automated output process. If you can replicate one of the process with an object you computer design and fabricate will be done. Also remember to upload the fabrication files.
* Includes the name and resin link you have used. What characteristics does the material have? What percentage of the mixture is there between the parts?
* Explain a little more about how the vacuum bag process. What layers / materials are needed? the order of them? Did you use a breather material?
* The link to the "computer controlled machining week" it's broken
* Nice composite work! And I love the idea to use the wood lamp as a mold for the composite.
* Bonus!! Will be super nice if you can add a video where you hit the composite lamp to hear the noize as we usually do in Neil's review ;)


### Mechanical Design and Machine design

**Group page**

* Not clear how your team planned and executed the project. Maybe you should explain better how you divide the task and plan the week.
* Super nice the Lesson learned section! Congrats!
* Bonus: Add name of team and lab in the slider.
* Not clear how your team planned and executed the project. Maybe you should explain better how you divide the task and plan the weeks.
* Missing Silde and Video
* Seem you have not updated this page with the latest changes you have made. Please do it because the machine is super nice and it is a pity that we do not see all the work that you have done during the last two weeks to make it work!
* Individual page
* Add a link to the Project page documentation.
* Here you should explain just your individual contribution to the project.
* Missing Slide
* Missing fabrication files
* Recognise opportunities for improvements in the design
* Listed future development opportunities for this project

**Individual page**

- Would be nice to explain basic settings for the 3d printed pieces.

### Embedded Networking and Communications

- you must use some form of addressing between boards.

### Invention, Intellectual Property and Business Models

- Very nice Licence analysis.
- Very interesting the “notes on patents, design register and licences”
- It would be nice to summarize some of the different licenses available and explain why did you choose that.

### Project development

- You should add this week to the list of weeks and answer the questions.
- Maybe you can switch some content from final project page in project development.
- I will document just the final version in Final project and pass the rest to project development
- Also check the Assignments and Assessment book and answer the questions. http://docs.fabacademy.org/assessment/project_development.html

### Final project

- It is difficult for me to evaluate the final project, because it seems that a lot of documentation is missing.
- On this page there is only information about what seems to be a preliminary version of the pcb design and the process of welding the wheels to the chair. - But there is no detailed information on the rest of the elements needed to complete the project. 3D design, programming, digital fabrication....
- Based on whats in the video you did more than it's documented.
- Please read well the assessment guide and answer all the questions.
 http://fabacademy.org/2020/docs/fabacademy-assessment/project_requirements.html
- I might be easier for you local instructor to know what you did because he was physical with you in the lab, but to any global instructor just following your poor documentation it almost impossible.
- We don´t even know how you fabricate your project as is not explained. It is just welding? has other techniques?( is a requirement to have at least a subtractive and additive or 2 techinques)
- BRAVO! You are close to finishing your fab academy! Congrats for your documentation. You did a great job!

## Documentation examples

**SUPER**

- [krisjanis-rijnieks](http://fab.academany.org/2018/labs/barcelona/students/krisjanis-rijnieks/)
- [joris-navarro](http://fab.academany.org/2018/labs/barcelona/students/joris-navarro/)
- [matthes.ben](http://fabacademy.org/archives/2015/as/students/matthes.ben/project_proposal.html)

**GOOD**

- [student](http://archive.fabacademy.org/fabacademy2017/fablabbcn/students/141/index.html)
- [javier-alboguijarro](http://fab.academany.org/2018/labs/barcelona/students/javier-alboguijarro/)

**NOT**

- [santifu](http://fabacademy.org/archives/2012/students/fuentemilla.santiago/week2.html)
