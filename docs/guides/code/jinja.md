## Intro

From March 24th, if you are using Mkdocs, you may have noticed your [latest CI pipelines]({$project['web_url']}/-/jobs) are failing, with the following errors:

```
AttributeError: module 'jinja2' has no attribute 'contextfilter'
```

If you **have not** made any commits to your project since then, you may not have yet triggered the error. The next time you make a commit, depending on the Mkdocs version or theme, this error might appears.

This is estimated to only affect continuing students from 2021, 2020, those using v7 or less of the Material theme, as v8 also includes the fix).

If you already solved this issue, or do not get this error in your pipelines, you can safely ignore this.

## What to do now

1. Edit the `requirements.txt` fix in your project root.
2. Replace the "mkdocs" version line to the follow:

**From:**
```
mkdocs >=1.1.2, ==1.1.*
```
**To:**
```
mkdocs >= 1.2.2
```

Hopefully you pipeline works now.

---

There are other way to solve, for example by upgrading to the Material Theme version 8. Which you can follow using the Material [upgrade guide here](https://squidfunk.github.io/mkdocs-material/upgrade/), keeping in mind which version you are upgrading from in your `requirements.txt` file.

## Why

This is related to [a new release of Jinja](https://jinja.palletsprojects.com/en/3.0.x/changes/#version-3-0-0) in a GitHub package, used by Mkdocs. The version range specified in our default setup, does not install this new release.

This merge request with the commit will update the version of MkDocs, to include [release v1.2.4](https://github.com/mkdocs/mkdocs/releases/tag/1.2.4), used in your project with the  file. See the "Changes" tab to preview see the file differences.

MkDocs [updated the fix here](https://github.com/mkdocs/mkdocs/pull/2795) which is in the new release.
