### Where can I find the classes?

- [Class & Recitations](https://vimeo.com/academany/)

### Where can I find Neil's screen?

- [Screenshare Neil's screen](http://screen.academany.org)

### How do I get feedback?

* [Evaluation platform](http://nueval.fabacademy.org/)

### How will I be evaluated?

* [Assignment assessment guide](http://fabacademy.org/2022/docs/fabacademy-assessment/)

### Where should I document?

* [GITLAB Repo](https://gitlab.fabcloud.org) (Students must be registered - username is the same from the program registration, normally fablabs.io account)

### Where do I get information?

* [Student assesment evaluation guidelines](https://fabacademy.org/2022/nueval/)
* [Fab Academy Tutorials](http://pub.fabcloud.io/tutorials/)
* [Local Docs](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs)
* [Global Docs](https://fabacademy.org/)

### I am a continuing student in FablabBCN, what should I know?

Fab Academy local costs in Fab Lab Barcelona are planned to cover one cycle (from January to July) each year. Continuing students that want to finish their Diploma must be aware of the extra costs:

!!! warning ""
    - **150€/week > Instructor’s support + Fabrication**
    - This only includes access to fabrication and the support of the Instructor and Fab Lab staff to update the assignments and documentation. It does not include Materials*.
    - Either you use 1 day of the week or the whole week, the price is fixed. We are flexible to allocate specific cases, but in general, the costs are not subject to discounts.

!!! Note ""
    - **100€ > Local Assessment**
    - Local Assessment is the process of evaluating the student’s work from their website and via the Nueval App. It is essential that the gross of the work is complete, so the evaluation is done at once. It only covers 1 evaluation cycle.
    - If a student needs to re-do an assignment, and the assignment requires new material, the cost of the material must be covered aside.
    - If a Student disengages for more than 1 year (this means no activity at all via Gitlab or emails, for 1 year after the cycle he/she was enrolled), these prices may vary. The reason for this is that the course changes every year, which means there are always new features to incorporate.

### How do I complain?

- If it's related to **content, local or global**, please address to Luciana <luciana.asinari@iaac.net>.
- If it's related to the **FabLab/IAAC infrastructure (space, machines, tools)**, to the Infrastructure Department <infrastructure@iaac.net> and CC Luciana.
