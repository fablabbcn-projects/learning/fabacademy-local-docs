# About Fabacademy

![](../../assets/fab_academy.png)

Fab Academy is an intensive ﬁve-month program that teaches students to envision, design and prototype projects using digital fabrication tools and machines. It is a multi-disciplinary and hands-on learning experience that empowers students to learn-by-doing and inspires them to make stuff locally to become active participants in sustainable cities and communities.

At the Fab Academy, you will learn how to envision, prototype and document your ideas through many hours of hands-on experience with digital fabrication tools. We take a variety of code formats and turn them into physical objects. The Fab Diploma is the result of the sum of Fab Academy Certificates. Progress towards the diploma is evaluated by a student’s acquired skills rather than time or credits.

The Fab Academy is a fast paced, hands-on learning experience where students plan and execute a new project each week! Each individual documents their progress for each project, resulting in a personal portfolio of technical accomplishments.

***What it is***

- Distributed education
- Personal Research
- No roads

***What it is not***

- A traditional education progamme

## How it works

- 1 week cycle - every week, new topic, new assignment, new documentation to do
- 19 to 20 weeks with 1 week break
- Global classes happen on Wednesdays 9am in Boston. Global Assignment reviews and Lecture (small bits of information)
- Local classes happen during the week
- There is one Final Project, which doesn't have to be at the end only

### The weekly assignments

Document what you do every day:

- Don´t expect to remember everything after 1 or 2 days
- If you document as long as you move on it will not be that annoying/time consuming

### The final project

- It is a 1 week project (production time)
- Your research may take the whole semester
- Remember: FabAcademy will not convert you into a (you name it)
- Think about you. Your hobbies and interests
- Propose what you want to do. Forget how to do it

**The three types of final projects**

![](https://gloobus.it/wp-content/uploads/2017/03/AAEAAQAAAAAAAAtTAAAAJDUyNTI0OGJiLTlmYjEtNGZjYS04NjQ0LTljODA2YTE5ZDhiYw.jpg)

- The good. Simple, well crafted
- The bad. Overcomplicated, does not solve any problem
- The ugly. Bad documented, horrible aspect

!!! Note "One last thing"
    - Design for long lasting
    http://www.core77.com/posts/24649/WhenWeBuiltThingsSolidly
    - Use digital fabrication to fix things and give trash a new life
    - Find the treasures in electronic waste: bearings, motors, shafts, encoders
    - Please, stop 3D printing, there is world way beyond that technique

### Get ready!

![](https://media.giphy.com/media/xUA7baWfTjfHGLZc3e/giphy.gif)

!!! tip "Some free tips"
    - Free your mind (forget what you already know)
    - Be brave, take risks (not physical risks)
    - Stress control - _can you dig it?_
    - Onus is on the student, not on the instructor
    - Use this as an opportunity to practice on the machines
    - Manage your resources and time appropriately
    - Iterate as many times as possible
    - Progress gradually in the resources you use. Prototype smartly
    - Questions are always ok!
    - Not knowing how to do something doesn’t mean you can’t do it. The key is to know how to find out
    - Something personal is always a better starting point as you already know a lot about yourself