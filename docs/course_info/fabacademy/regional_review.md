# Regional Review

[Attendance sheet](https://docs.google.com/spreadsheets/d/133z4wsmHGYKk90Loir30xvYG4kq8FPKSJ6k8xOjwGYw/edit?usp=sharing)

[Docs](https://docs.google.com/document/d/1u-Ff8A2TYpFUQ9ChMLqnfc3fuIZQf25Gv9EQ6G0fuQo/edit?usp=sharing)

!!! warning ""
    **Nota:**As a general rule, each student is assigned an instructor and must follow the guidelines dictated by him. (In case of doubt you can always ask the supervisor in the Regional Reviews)


    **Every Tuesday from 13h to 14h hours**
    - Zoom Link : [https://us02web.zoom.us/j/87451768492?pwd=bDFwb3h1YUNjdEg1dW9zQWljT21UQT09]( https://us02web.zoom.us/j/87451768492?pwd=bDFwb3h1YUNjdEg1dW9zQWljT21UQT09)
    - Pass: 4321
    Metting ID: 874 5176 8492

    https://us02web.zoom.us/j/87451768492?pwd=bDFwb3h1YUNjdEg1dW9zQWljT21UQT09
    
### Instructors

**Who is in Fab Lab León?**

- Nuria Robles - [mail](mailto:nuriafablab@gmail.com)
- Pablo Núñez - [mail](mailto:eltercerlugar@gmail.com)

**Who is in Barcelona:**

- Santi Fuentemilla - [mail](mailto:santi@fablabbcn.org)
- Josep Marti - [mail](mailto:josep@fablabbcn.org)
- Eduardo Chamorro - [mail](mailto:eduardo.chamorro@iaac.net)
- Óscar González Fernández - [mail](mailto:oscar@fablabbcn.org)
