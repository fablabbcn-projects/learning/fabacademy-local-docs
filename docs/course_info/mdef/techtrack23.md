## Welcome

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTAWERYbLq87f0VXmUY-lLMubJnBlmA_cebf7W6MuDmYMzeBYXvcFpKuU12fX_D6nvKKJjsMYKjIX9B/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Fabacademy VS MDEF Techtrack

As you know, the Fab Academy block inside the Tech Track for 2nd and 3rd terms was designed based on the content of the Fab Academy Diploma Program, as a version of it.
Fab Academy processes and dynamics are so intensive that, during the first trials, they proved to be incompatible with MDEF's load of work. Therefore, we adapted them so you can have access to the content, tools, and approach, but in a way that fits better into your schedule and makes more sense for your personal research projects

### Content
Fab Academy is comprised of 20 technical assignments that are weekly tested, developed and documented in small assignments, and evaluated accordingly.  MDEF students experience tasks in packages, followed by Challenges that integrate the knowledge into week-long projects.
The content is the same, the way of developing and documenting the assignments differs.

The program applies Fab Academy mindset without modifying the global schedule, but applying new methodologies such as "rotational task’s stations", redistributing the impact of weekly hours, and adding new assessment criteria.

The instructional design of the course has two fundamental assumptions, individual reflection tasks for each weekly topic, and monthly intensive maker-sprint in the form of “micro-challenges”. Students work in small groups to develop week-long projects applying knowledge and skills from the previous Fab Academy topics with concepts related to MDEF and their research projects, aimed to bridge the gap that has existed between these two courses and demonstrating the competencies acquired.

### Global classes
Because you won't be following the assignments weekly in a literal sense, you can attend Neil's global classes to get the lecture firsthand and experience the immersion of the global community, but you won't be called by him to review your work each week. You are not listed in the Students list because you are not taking Fab Academy Diploma, but the MDEF version of it.


### Evaluation standards & process
The way we evaluate MDEF's version also differs.
Fab Academy evaluation standards are designed to assess a deep immersion in each assignment. MDEF only considers these standards partially to evaluate your work, in order to adapt it to your level of exposure to each task.
The process of evaluating Fab Academy is based on a 2 layer system: Instructors evaluate their students locally and Global Evaluators (picked randomly from the Global team) re-assess that same documentation to make sure the Evaluation Standards were met.
In your case, your evaluation happens in a local instance, in accordance with the evaluation system of IAAC's Master Programs.


### Repository and Documentation
Because you are not Fab Academy Students in the strict sense, you don't document in the Fab Academy GitLab Repository: https://gitlab.fabcloud.org/
Your documentation happens in MDEF's own repository (GitHub), this is why you don't receive access to the global repository, not any of the issue trackers Neil and the team create there.
But don't worry...your Instructors will give you access to all the information needed to fulfill MDEF's requirements.



## MDEF Precourse

Find the precourse documentation [here](https://fablabbcn-projects.gitlab.io/learning/precourse/)
