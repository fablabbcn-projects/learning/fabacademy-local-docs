# Evaluation criteria
## Grade System

![](assets/assessment-e5677c52.png)

## ASSESSMENT

### TASKS

**In case the student fail to submit on time, the tasks/challenges documentation and repositories. A final *-1* will be substracted to the final course grade.**

#### Individual post

- Write a post out your weekly experience
- Reflect your learning goals
- (Challenge week) Add link to the challenge section and repo

### CHALLENGES

#### Academic level
**Level of the project (quality and complexity of the designed prototype/code/artifacts)**

- Add names and links to your individual pages
- Linked to your individual pages
- Initial idea / Concept of the Project ( aligned to research areas)
- Propose (What is supposed to do or not to do)
- Shown how your team planned and executed the project. (Explain personal contributions)
- System diagram (illustration explaining function, parts, and relations)
- Integrated Design (How you designed it - relation between elements)
- Honest Design (use of technology in a meaningful way, in relation to the topics)
- Be creative (find solutions with materials and technology you have)
- Explore design boundaries (based on your expertise)
- Listed future development opportunity for this project

#### Replicability
**Level of clarity and detail of the documentation material (photos, video, text, etc)**

- How did you fabricate it (fabrication processes and materials)
- Design & Fabrication files (open source or open format)
- BOM (Build of Materials)
- Iteration Process (spiral development)
- Described problems and how the team solved them
- Photographies of the end artifacts

#### Attitude
**Involvement, Motivation level, proactive behaviors**

- Attendance to classes
- Proactive behaviours to find answers during the challenge
- Help others student’s projects
- Participation in feedbacks sessions
- Dealing with uncertainty
- Don't be afraid to make mistakes ( going out of your confort zone)

#### Explosion
- Bonus
