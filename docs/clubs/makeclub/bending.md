# Bending notes

Presentation for bending materials.

![](https://teenageengineering.com/_img/5c3373bffe5ac600048a8a3c_4096.png)
_Image Source: Teenage Engineering_

## How you display things really matters

This is a mess:

![](https://i.imgur.com/ubkGJ5K.png)

This is quite nice:

![](https://i.imgur.com/Z1qOlAh.png)

## Acrylic hot bending techniques

Acrylic is an easy to bend material with reasonably low amounts of heat. Two main techniques can be used: **hot gun** and **hot wire**.

- Hot gun:
![](https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.wikihow.com%2Fimages%2Fthumb%2F8%2F80%2FBend-Acrylic-Step-4-Version-2.jpg%2Faid4719217-v4-728px-Bend-Acrylic-Step-4-Version-2.jpg&f=1)

- Hot wire:
![](https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fshannon.nl%2Fimg%2Faccesoires%2F3-wire%2520reflector.JPG&f=1)

To fix components in place, an equipment like this is useful:

![](https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fs-media-cache-ak0.pinimg.com%2F736x%2Fde%2Fea%2F97%2Fdeea9716e587fad3c3024bc446e50eca.jpg&f=1)

## Metal bending techniques

Metals can also be bent, although in this case other techniques should be used. A summary of these is done in this fantastic post in [hackaday](https://hackaday.com/2016/05/18/the-art-and-science-of-bending-sheet-metal/).

- Slotting: in particular, slotting is an interesting technique to do so, because the material can be later on be bent by hand:

![](https://hackadaycom.files.wordpress.com/2016/05/self-bend.jpg)

An example of a final product:

![](https://www.teenageengineering.com/_img/5c41c7f0112491000438e5a7_4096.png)

## Doing this in fusion360

Fusion360 comes with a nice module for sheet bending. It's called sheet metal. You can find a tutorial [here](https://www.youtube.com/watch?v=nEjFMYNGY4g).