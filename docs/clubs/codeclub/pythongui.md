## GRAPHICAL USER INTERFACE (GUI) WITH PYTHON

Python provides various options for developing graphical user interfaces (GUIs). Most important are listed below.

Tkinter − Tkinter is the Python interface to the Tk GUI toolkit shipped with Python.

wxPython − This is an open-source Python interface for wxWindows http://wxpython.org.

JPython − JPython is a Python port for Java which gives Python scripts seamless access to Java class libraries on the local machine http://www.jython.org.


### FIRST GUI APPLICATION

Let's import Tkinter package and create a window and set its title:

```
from tkinter import *

window = Tk()

window.title("My first GUI app")
""" infinite loop """
window.mainloop()
```

You should see something like:

![](https://i.imgur.com/TgypkQF.png)

**mainloop()** is used when you are ready for the application to run. mainloop() is an infinite loop used to run the application, wait for an event to occur and process the event till the window is not closed.

You can change the **windows size**:
```
windows.geometry('350x200')
```
or fullscreen ( don't forget to add an exit button):

```
windows.attributes('-fullscreen', True)

# Create a button that will destroy the main window when clicked
exit_button = Button(window, text='Exit Program', command=window.destroy)
exit_button.grid(row=2)
```

**Change the background color**:
```
window.configure(background='red')
```
```
window.configure(bg='#000000')
```

### Label Widget

This widget implements a display box where you can place text or images. The text displayed by this widget can be updated at any time you want. It is also possible to underline part of the text (like to identify a keyboard shortcut) and span the text across multiple lines.

[Label widget reference.](http://effbot.org/tkinterbook/label.htm)
```
from tkinter import *

window = Tk()
window.title("First GUI APP")
# To define a label widget
lbl = Label(window, text="Hello")
lbl.grid(column=0, row=0)
# infinite loop
window.mainloop()
```
![](https://i.imgur.com/IYbuZWi.png)

The Grid geometry manager puts the widgets in a 2-dimensional table. The master widget is split into a number of rows and columns, and each “cell” in the resulting table can hold a widget.

The grid manager is especially convenient to use when designing dialog boxes.


### Entry Widget

The entry widget is used to enter text strings. This widget allows the user to enter one line of text, in a single font.

[Entry widget reference.](http://effbot.org/tkinterbook/entry.htm)

Add these two lines of code:

```
entryField = Entry(window)
entryField.grid(row=0, column=1)
```

```
from tkinter import *

window = Tk()
window.title("First GUI APP")
# To define a label widget
lbl = Label(window, text="What's your name?")
lbl.grid(column=0, row=0)
# Entry field
entryField = Entry(window)
entryField.grid(row=0, column=1)
# infinite loop
window.mainloop()
```


![](https://i.imgur.com/JrvHNj9.png)


### Button Widget ( and we'll get the Challenge !)

The **Button widget** is a standard Tkinter widget used to implement various kinds of buttons. ... When the button is pressed, Tkinter automatically calls that function or method. The button can only display text in a single font, but the text may span more than one line.

[Reference for button widget](http://effbot.org/tkinterbook/button.htm)

Add this line:

`b = Button(window, text="Get", width=10, command=callback)`

To fetch the current entry text, use the get method:

`entryField.get()`

The code for the challenge:


```
from tkinter import *

window = Tk()
window.title("First GUI APP")
# To define a label widget
lbl = Label(window, text="What's your name?")
lbl.grid(column=0, row=0)
# Entry field set the entry text in a variable
v = StringVar()

entryField = Entry(window,textvariable=v,width=30)
entryField.grid(row=0, column=1)

def callback():
    print ('Hola,' + entryField.get())

b = Button(window, text="Get", width=10, command=callback)
b.grid(row=1,column=0)

# infinite loop
window.mainloop()
```
![](https://i.imgur.com/TL79zGv.png)


### Pack Geometry Manager

The Pack geometry manager packs widgets in rows or columns. You can use options like fill, expand, and side to control this geometry manager.

Compared to the grid manager, the pack manager is somewhat limited, but it’s much easier to use in a few, but quite common situations:

1. Put a widget inside a frame (or any other container widget), and have it fill the entire frame.
2. Place a number of widgets on top of each other.
3. Place a number of widgets side by side

Try to use Pack manager insted of grid in our code:

```
from tkinter import *

window = Tk()
window.title("First GUI APP")
window.geometry('600x300')
window.configure(background='red')
window.configure(bg='#000000')
#window.attributes('-fullscreen', True)
# To define a label widget
lbl = Label(window, text="What's your name?",font=("Arial Bold", 20))
lbl.pack()
# Entry field set the entry text in a variable
v = StringVar()

entryField = Entry(window,textvariable=v,width=30,font=("Arial Bold",20))
entryField.pack()

def callback():
    print ('Hola,' + entryField.get())

b = Button(window, text="Get", width=10, command=callback)
b.pack()

# Create a button that will destroy the main window when clicked
exit_button = Button(window, text='Exit Program', command=window.destroy)
exit_button.pack()
# infinite loop
window.mainloop()
```
![](https://i.imgur.com/0ufUS58.png)

### Canvas Widget

The Canvas widget provides structured graphics facilities for Tkinter. This is a highly versatile widget which can be used to draw graphs and plots, create graphics editors, and implement various kinds of custom widgets.

[Canvas widget reference.](http://effbot.org/tkinterbook/canvas.htm)

Example of use:

```
from tkinter import *

window = Tk()
window.title("First GUI APP")
window.geometry('600x300')
window.configure(background='red')

#window.attributes('-fullscreen', True)

w = Canvas(window, width=200, height=100)
w.pack()

w.create_line(0, 0, 200, 100)
w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))

w.create_rectangle(50, 25, 150, 75, fill="blue")
# Create a button that will destroy the main window when clicked
exit_button = Button(window, text='Exit Program', command=window.destroy)
exit_button.pack()

# infinite loop
window.mainloop()
```
![](https://i.imgur.com/NYw2KAl.png)


### Image background

```
from tkinter import *
from PIL import ImageTk, Image

window = Tk()
window.title("First GUI APP")
window.geometry('1200x833')

img = Image.open("image_bg.jpg")
img_bg = ImageTk.PhotoImage(img)

window.configure(background='red')

# To define a label widget
lbl = Label(window, text="What's your name?",font=("Arial Bold", 20))
lbl.pack()
# Entry field set the entry text in a variable
v = StringVar()

entryField = Entry(window,textvariable=v,width=30,font=("Arial Bold",20))
entryField.pack()

def callback():
    print ('Hola,' + entryField.get())

#window.attributes('-fullscreen', True)
background_label = Label(window, image=img_bg)
background_label.place(x=0, y=0, relwidth=1, relheight=1)

# Create a button that will destroy the main window when clicked
exit_button = Button(window, text='Exit Program', command=window.destroy)
exit_button.pack()

# infinite loop
window.mainloop()
```
![](https://i.imgur.com/v1vBcmr.jpg)

### Frame Widget

A frame is rectangular region on the screen. The frame widget is mainly used as a geometry master for other widgets, or to provide padding between other widgets.

Frame widgets are used to group other widgets into complex layouts. They are also used for padding, and as a base class when implementing compound widgets.

Example:

```
from tkinter import *
from PIL import ImageTk, Image

window = Tk()
window.title("First GUI APP")

window.configure(background='red')

frame = Frame(window)
frame.pack()

bottomframe = Frame(window)
bottomframe.pack( side = BOTTOM )

redbutton = Button(frame, text="Red", fg="red")
redbutton.pack( side = LEFT)

greenbutton = Button(frame, text="Brown", fg="brown")
greenbutton.pack( side = LEFT )

bluebutton = Button(frame, text="Blue", fg="blue")
bluebutton.pack( side = LEFT )

blackbutton = Button(bottomframe, text="Black", fg="black")
blackbutton.pack( side = BOTTOM)

# infinite loop
window.mainloop()
```

![](https://i.imgur.com/koeL2Ro.png)
