Originally from:
https://hackmd.io/Z7uBEzZ3TxaxjChZ8T5TfA#

---
tags: fabacademy
title: Code Club
---

## General Overview

| Session | Hours | Python Basics Covered|
|:-:|:-:|:-:|
| Basics | 2 | Executable, functions, conditionals and loops |
| GUI | 2 | imports, application concept, interaction with filesystem |
| Internet | 4 | lists, dicts, json, http request, apis | 
| Real world (Serial, I2C, Camera) | 4 | serial, i2c, flush |
| Communication | 2 | ? | 
| Interface | 2 | ? | 
| Project | 4 | Overal project definition |

## Program

### Python Basics

**Target**:
Install python _properly_ and learn application basics.

**Content**:
* Cross-platform installation
* Package managers: pip, anaconda
* Virtual environment: venv, conda
* Executable python applications, _chmod_, _shebang_, _which_ commands
 
**Example**: 
Basic hello world in different flavours
 
**Challenge**:
Make executable with argparsing that yields:
```python=
$ ./hello --name "Andres"
Hello, my name is Andres
```

### GUI

**Target**:
User interface with Python

**Content**
* How to import modules into Python programs
* How to interact with the filesystem, open a file, close it, with different rights
* Structure of an application with __main__

**Example**:
Basic application with button and text field that prints out on callback

**Challenge**:
Make an application that interacts with the user in at least three ways: button, text field and another

### Python vs The internet

**Target**:
Learn how Python can interact with the internet (basic)
 
**Content**
* Web Scrapping
* HTTP Requests
* Storing data in the application - list, dicts, json file format
* Asking APIs

**Example**
* Download a website with BS and read it. Show headlines of BBC News in a Tkinter interface
* Make a request to OMDB API with a string and return a GIF with the posters of films that contain that string in the title

**Challenge**
* Visualize in some way how many actors named _Peter_ have appeared in films since 1950s

### Python vs Real Life

**Target**:
Learn how Python can interact with objects via cables
 
**Content**
* Serial communication: understand how data is being sent from an arduino via serial port
* Learn how to open ports, write and read from them
* Pi I2C, reading GPIOs
* Camera opening, taking pictures
* Making tasks to run periodically

**Example**
* Read an analog sensor with an Arduino and log the data into a log.dat file

**Challenge**
* Read a sensor with an arduino and make an alarm go off when it reaches a certain threshold

## Python communication

**Target**:
Learn how Python can interact with other things without cables, and without the internet

**Content**:
* Understand the different protocols: HTTP, UDP, RTSP, OSC, MIDI...
* Understand ports addresses
* How to send packages reliably

**Example**:
Control something remotely, with a NodeMCU or a raspberry Pi. Maybe use a MIDI controller

**Challenge**:
Make a remote control for a motor

## Final project sessions

Make a _Polling_ Python Application for conferences based on image recognition.

**Ideas**:
- Use camera to recognise the audience's mood, via red/green rackets

## Other Possible sessions

1. Python for ML/AI (TBD)

2. Python for Interface

**Task**:
Web Applications for data visualisation

**Content**:
* Serve an application with python script:
    * Apache
    * nginx
    * flask
* Dash

**Example**
Read an arduino sensor an plot it in a server

**Challenge**
Blink the arduino LED with a click on your browser application


