# The resurrected website

This year, we'll work a bit more in a project-fashion, rather than lectures. For this we'll build our own _raspberry-pi hosted, solar-powered website_. We'll do so step by step, and we won't assume any previous knowledge for making this happen. 

!!! danger "Take nothing for granted"
    We will try to make each and every step of the process in a concious and informed way. We will test which solar panel is best, which options do we have for batteries, servers, website availability...

## Hardware

![](assets/hardware-resurrected-web.png)

We'll use the following hardware:

- a solar panel
- a raspberry pi zero
- a LiPo battery
- an MPTT

For each of these:

- The raspberry pi zero is the lowest consumption pi out there, so not much to discuss here
- MPTT, we have some spare [DF-Robot MPTTs](https://wiki.dfrobot.com/Solar_Power_Manager_5V_SKU__DFR0559) in the lab, so for the sake of simplicity, they are a good choice
- For the solar panel, we have to decide (some options to test)
- For the LiPo, we have to decide as well, we will use a 3.7V battery, but we have to test which capacity to use together (2Ah, 6Ah, 8Ah...)

!!! warning "Why so much testing?"
    Because every decission we make on a project can affect many other outcomes, not only performance. For instance, we might accept lower website availability for saving some Lithium to the planet, same for the solar panel.

## Workflow

On the hardware part, we'll initially build and test various options of solar panel and battery combinations and we'll check how long where they alive for. Based on that, we'll pick one and build the enclosure and internal fittings. Once that's ready, we won't touch them unless we need to debug.

On the software part, we'll use **gitlab** to collaborate, although it won't host the website itself (obviously). The main repository is here: https://gitlab.com/fablabbcn-projects/learning/the-resurrected-web. The instructions to reproduce this project will be in the raspberry-pi itself, or in _raw_ in the gitlab repository. We'll balance online setup in the raspberry pi via SSH, or with periodic pulls using crontab.

![](assets/workflow-diagram.png)

## Learning outcomes

Expected, many, but hopefully we can learn how to:

- Set up a raspberry pi and have remote access to it
- Power it with a solar panel setup we design
- Make it serve a static site
- Make it serve it on boot, without our intervention
- Make it run jobs periodically
- Make it visible to the world
- Assess consumption, possible energy saving setups

Also, we will discuss about:

- Technology and implications
- Energy consumption of the ubiquous "cloud"
- Hardware design implications in the environment
- Is high tech always better? Is availability always better?

## Inspiration

This project is heavily inspired by [high-tech low-tech](http://highlowtech.org/) project on a [solar powered site](https://solar.lowtechmagazine.com/).

![](assets/solar-lowtech.png)

## Sessions

### Session 1: testing our setup

Today we will test which solar panel size and battery combination suits better our needs. For this we will set up several raspberry pi zero for only a week with different solar panels and batteries. 

**Instructions:**

1. [Set up the raspberry pi](../../../guides/code/raspberrypi/) and connect it to the WiFi network
2. Pick a battery and solar panel
3. Assemble it in an IP box
4. Tag it and put the same [local hostname](https://raspberrytips.com/set-new-hostname-raspberry-pi/)
5. Take a picture of the set up and put it in gitlab with a README in the [test folder](https://gitlab.com/fablabbcn-projects/learning/the-resurrected-web/-/tree/master/test), include the hostname

### Session 2: analysing data

Today we'll have a look at the data we gathered. If something didn't work, we'll fix it and leave it running again.

**Instructions:**

1. What did you test?
2. What were the problems you faced?
3. What conclusions did you get?
4. Want to share them with the rest? Put it in the repository in the [test report](https://gitlab.com/fablabbcn-projects/learning/the-resurrected-web/-/tree/master/test/report.md)
5. Let's decide about our setup!

### Session 3: making a website and serving it

**Instructions:**

1. Divide in groups
2. Each group one page of:
    - about.html
    - index.html
    - ...
3. Let's decide how to make it light! Let's put it in https://gitlab.com/fablabbcn-projects/learning/the-resurrected-web/-/tree/master/site

This is a traditional setup for serving static content or a web application:

![](assets/example-network-diagram.png)

We'll be serving content using NGINX, a very light weight and versatile web server. 

```
sudo apt-get update
sudo apt-get install nginx
```

After this, the nginx is already running, and anything we put in `/var/www/html` will be served by `nginx`. Now, we just need to put our website there!

### Session 4: making it public

In the previous session, we used NGINX to publish our website. In order to see it from the outer world we'll use a single raspberry pi available in Fablab for these kind of experiments:

![](assets/network-diagram.png)


