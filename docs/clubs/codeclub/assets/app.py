import serial # For the Serial interface
# Plot stuff
import plotly
import plotly.graph_objs as go
# For the data
import pandas as pd
# For the time
import datetime
import time
# For the interface
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, Event
# For interfacing with the file system
from os.path import dirname, join
import csv
import os.path
# For doing things nicely
from multiprocessing import Process, Queue
import multiprocessing

# Define queues
input_queue = Queue()
output_queue = Queue()

# Create app
app = dash.Dash()
server = app.server

# Define port names
PORT = '/dev/cu.SLAB_USBtoUART'
BAUDRATE = 115200

# Reading interval
reading_interval = 1

# Test file
test_file = ("LogTemp.csv")
file_path = dirname(__file__)

# Init some stuff
index = -1
Time=[]
Temp=[]

class testSequencer(multiprocessing.Process):

	def __init__(self, input_queue, output_queue, port='/dev/ttyAMA0', baud=115200):

		multiprocessing.Process.__init__(self)
		self.PORT = port
		self.BAUDRATE = baud
		self.input_queue = input_queue
		self.output_queue = output_queue
		self.serial_open = False
		self.serial = []
		self.df = pd.DataFrame(
			{'Time':[],
			'Temp': []
			})

	def close_serial(self):
		if self.serial_open == True:
			self.serial_open = False
			self.serial.close()

	def writeSerial(self, data):
		self.serial.write(data)
		
	def readSerial(self, lines):
		if lines == 1:
			return self.serial.readline().replace("\r", "")
		else:
			for i in range(lines):
				self.serial.readline().replace("\r", "")
			return ''

	def flush(self):
		self.serial.flushInput()
		self.serial.flushOutput()

	def perform_test(self, file):
		with open(file, "w") as csv_File:
			wr = csv.writer(csv_File)
			wr.writerow(['Time', 'Temperature'])

		time_last = 0
		while self.input_queue.empty():

			buffer = self.serial.inWaiting()

			if buffer != '':
				try:
					_data = int(self.readSerial(1))
				except:
					_data = 0

				_data_list =  [datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), _data]
							
				with open(file, "a") as csv_File:
						if (time.time() - time_last > reading_interval):
							time_last = time.time()
							wr = csv.writer(csv_File)
							wr.writerow(_data_list)

				# except:
				# 	if self.serial_open == False: break
				# 	pass

	def run(self):

		while True:

			if not self.input_queue.empty():
		   		task = self.input_queue.get()
		   		print 'TASK RECEIVED: ' + task
				if task == 'START':
					self.serial = serial.Serial(self.PORT, self.BAUDRATE, timeout = 1)
					self.serial_open = True

					self.flush()
					print '------------------------------'
					print ' STARTING APP'
					print '------------------------------'

					print 'Saving data to:', test_file
					self.perform_test(test_file)

				elif task == 'STOP':
					try:
						self.flush()
						print '------------------------------'
						print ' STOPING APP'
						print '------------------------------'
						self.close_serial()
					except:
						print 'Probably there is nothing to stop yet'

app.layout = html.Div(children=[
	html.Div([
		dcc.Markdown(children='## Example Interface'),	
		html.Button(id='start', type='submit', children='START'),
		html.Div(id='start-result'),
		html.Button(id='stop', type='submit', children='STOP'),
		html.Div(id='stop-result'),
		], className='row'),

	html.Div([
		dcc.Graph(id='Graph', style={'height': 400}, animate = False),
	]),
		
	## Interval from graph Update
	dcc.Interval(
		id='interval-component',
		interval=1*1000,
		n_intervals=0
	),
])

@app.callback(Output('Graph','figure'),
  [Input('interval-component','n_intervals')])
def update_graph_live(n):
	traces = list()

	layout = go.Layout(
			 showlegend=True,
			 legend=go.layout.Legend(
				 x=0,
				 y=1.5
			 ),
		)
	
	try:

		dataVal = pd.read_csv(join(file_path, test_file), delimiter = ',', encoding="utf-8-sig").set_index('Time')
		dataVal = dataVal.tail(1000)
	
		Temperature = dataVal.loc[:,'Temperature']
	
		
		traces.append(go.Scatter(
			x=dataVal.index,
			y=Temperature,
			name='Temperature'
		))
	
	except:
		pass

	return {'data': traces, 'layout': layout}


@app.callback(Output('start-result', 'children'), 
	[Input('start', 'n_clicks')])
def onclick(n):
	print 'DEBUG: START'
	if n>1:
		input_queue.put('START')


@app.callback(Output('stop-result', 'children'), 
	[Input('stop', 'n_clicks')])
def onclick(n):
	print 'DEBUG: STOP'
	if n>0:
		input_queue.put('STOP')

# CSS	
app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"})
app.scripts.config.serve_locally = True

# Port
if __name__ == '__main__':

	## Port 1 for 
	worker = testSequencer(input_queue, output_queue, PORT, BAUDRATE)
	worker.daemon = True
	worker.start()

	## Dash over Flask
	app.run_server(debug=True, host='0.0.0.0', port=8000, processes = 4)