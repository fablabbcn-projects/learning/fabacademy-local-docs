# Seeing the world!

<iframe src="https://giphy.com/embed/6865p3Xeu4a6A" width="480" height="300" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/cat-magic-future-6865p3Xeu4a6A">via GIPHY</a></p>

!!! Note
    Find today's code in the [gitlab repo](https://gitlab.fabcloud.org/barcelonaworkshops/code-club/tree/2020/04_image_processing)

!!! warning 
    Many of the examples from today, and many many more can be found at the [official docs](https://github.com/opencv/opencv/tree/3.4/samples/python/tutorial_code)

## Open CV

![](https://raw.githubusercontent.com/wiki/opencv/opencv/images/OpenCV_Logo_with_text.png)

> OpenCV (Open Source Computer Vision Library: [http://opencv.org](https://opencv.org/)) is an open-source BSD-licensed library that includes several hundreds of computer vision algorithms.

!!! Note 
    [opencv](https://opencv.org/) and in [github](https://github.com/opencv/opencv)

### Installation

You need to compile OpenCV from source form the [master branch on github](https://github.com/opencv/opencv) in order to get the python bindings. 

!!! Note "Building from source"

    - Official: https://docs.opencv.org/4.3.0/df/d65/tutorial_table_of_content_introduction.html
    - Guide for MAC: https://www.learnopencv.com/install-opencv-4-on-macos/
    - Guide for Ubuntu: https://www.pyimagesearch.com/2017/09/25/configuring-ubuntu-for-deep-learning-with-python/
    - General for Linuxes (including Pi): https://github.com/jayrambhia/Install-OpenCV

### OpenCV-Python

![](https://3.bp.blogspot.com/-Q0XriPVi_KQ/VG9LQyFIy3I/AAAAAAAADVI/wuWtsgrAKUQ/s1600/opencv-python.png)

> OpenCV-Python is a library of Python bindings designed to solve computer vision problems.

> Compared to languages like C/C++, Python is slower. That said, Python can be easily extended with C/C++, which allows us to write computationally intensive code in C/C++ and create Python wrappers that can be used as Python modules. This gives us two advantages: first, the code is as fast as the original C/C++ code (since it is the actual C++ code working in background) and second, it easier to code in Python than C/C++. OpenCV-Python is a Python wrapper for the original OpenCV C++ implementation.

> OpenCV-Python makes use of Numpy, which is a highly optimized library for numerical operations with a MATLAB-style syntax. All the OpenCV array structures are converted to and from Numpy arrays. This also makes it easier to integrate with other libraries that use Numpy such as SciPy and Matplotlib.

!!! warning "Overwhelmed?"

    Choose how you want to use openCV. There are various frameworks (not only with python):

    - There is an **unofficial Python package** using:

    `pip install opencv-python`

    But this is not maintained officially by OpenCV.org. See [this post](https://stackoverflow.com/questions/19876079/cannot-find-module-cv2-when-using-opencv) if you are running into problems.

    You can also use:

    - [Processing](http://processing.org) (Java): https://github.com/atduskgreg/opencv-processing or [Processing.py](https://py.processing.org/) although it is not yet fully supported
    - [openFrameworks](https://openframeworks.cc/) (C++): https://openframeworks.cc/documentation/ofxOpenCv/

## Basics of image processing

![](https://www.datamation.com/imagesvr_ce/6540/AIvsMLvsDeepLearning.png)

- Image alignment: align images in pictures
- Object detection: detect object in images (where something is, knowing what we are looking for) - solvable with DL, but ML works too
- Identification: identify if one person matches another

### Descriptors

When dealing with image processing problems, a common approach is to divide the image in subimages or patches. Each of the patches is a small portion of the original image which can then be used and compared to other patches of other images and extract conclusions. Are the objects the same? Are the objects of the same type? Ideally, we would need of a method to determine if a patch is the same to another one, and that this method is independent of image changes: rotation, lighting, noise... Welcome **descriptors**.

> A descriptor is some function that is applied on the patch to describe it in a way that is invariant to all the image changes that are suitable to our application (e.g. rotation, illumination, noise etc.). A descriptor is “built-in” with a distance function to determine the similarity, or distance, of two computed descriptors. So to compare two image patches, we’ll compute their descriptors and measure their similarity by measuring the descriptor similarity, which in turn is done by computing their descriptor distance.
> 
![](https://gilscvblog.files.wordpress.com/2013/08/figure2.jpg?w=600&h=400)
_Source: [GilCV Blog](https://gilscvblog.com)_

!!! Note "More in wikipedia"
    https://en.wikipedia.org/wiki/Visual_descriptor

** Some types**

- [HOG](https://en.wikipedia.org/wiki/Histogram_of_oriented_gradients): Histogram of Oriented Gradients. Nice wrap-up [here](https://www.learnopencv.com/histogram-of-oriented-gradients/)
    + SIFT (non free)
    + SURF (non free)
- [Binary descriptors](https://gilscvblog.com/2013/08/26/tutorial-on-binary-descriptors-part-1/): Are a way of representing a patch as a binary string, using only comparison of intensity (in separated channels if necessary). 
    + BRIEF
    + ORB
    + BRISK
    + FREAK
    + ...

    They are based on the following workflow.
    1. Sampling pattern definition: where to sample points in the region around the descriptor.
    2. Orientation compensation: some mechanism to measure the orientation of the keypoint and rotate it to compensate for rotation changes.
    3. Sampling pairs: the pairs to compare when building the final descriptor.

!!! Note "Tutorial"
    [Finding features](https://docs.opencv.org/3.4/db/d27/tutorial_py_table_of_contents_feature2d.html)

### Homography

> Homography is a transformation (a matrix) that maps the points in one image to the corresponding points in the other image.
> 
![](https://www.learnopencv.com/wp-content/uploads/2016/01/homography-example-768x511.jpg)

![](https://user-images.githubusercontent.com/57528373/76420885-20d6f980-63a3-11ea-9716-a7b2925b6a6e.gif)

!!! Note "Read about it in much nicer way"

    - [Homography](https://www.learnopencv.com/homography-examples-using-opencv-python-c/)
    - [Image alignment](https://www.learnopencv.com/image-alignment-feature-based-using-opencv-c-python/)

### Video

![](https://docs.opencv.org/3.4/videoFileStructure.png)

> For start, you should have an idea of just how a video file looks. Every video file in itself is a container. The type of the container is expressed in the files extension (for example avi, mov or mkv). This contains multiple elements like: video feeds, audio feeds or other tracks (like for example subtitles). How these feeds are stored is determined by the codec used for each one of them. In case of the audio tracks commonly used codecs are mp3 or aac. For the video files the list is somehow longer and includes names such as XVID, DIVX, H264 or LAGS (Lagarith Lossless Codec). The full list of codecs you may use on a system depends on just what one you have installed.

!!! Note
    [https://docs.opencv.org/3.4/d7/d9e/tutorial_video_write.html](https://docs.opencv.org/3.4/d7/d9e/tutorial_video_write.html)

## Tracking movement

<div style="width:100%;height:0;padding-bottom:67%;position:relative;"><iframe src="https://giphy.com/embed/tHpYMu8ClzTry" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/computer-vision-tHpYMu8ClzTry">via GIPHY</a></p>

Ultimately, we can try to detect things in a video, and identify them as different classes. We can find objects in a video by tracking color, pixel changes, or trying to build more complex classifiers, all the way up to deep learning algorithms.

### Color tracking

We pick a color and track it's centroid. 

!!! Note "Example"
    [Color tracking](https://gitlab.fabcloud.org/barcelonaworkshops/code-club/blob/2020/04_computer_vision/06_color_tracking.py)

### Optical Flow

**Optical flow or optic flow** is the pattern of apparent motion of objects, surfaces, and edges in a visual scene caused by the relative motion between an observer and a scene.

!!! Note "Tutorial"
    [Optical flow](https://docs.opencv.org/4.2.0/d4/dee/tutorial_optical_flow.html)

## Identifying things

### Haar Cascade Classifiers

<iframe src="https://player.vimeo.com/video/12774628" width="640" height="610" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/12774628">OpenCV Face Detection: Visualized</a> from <a href="https://vimeo.com/adamhrv">Adam Harvey</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/uEJ71VlUmMQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

!!! Note "Source"
    [Cascade classifier](https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html)

### Welcome to YOLO

YOLO performs object detection by creating a rectangular grid throughout the entire image. Then creates bounding boxes based on (x,y) coordinates. Class probability gets mapped by using random color assignment. To filter out weak detections, a 50% confidence is being used (this can change) which helps eliminate unnecessary boxes.

!!! Note
    The paper: [https://arxiv.org/abs/1804.02767](https://arxiv.org/abs/1804.02767)
    The source: [https://pjreddie.com/darknet/yolo/](https://pjreddie.com/darknet/yolo/)

**Installing**

Options:
A) Install Darknet: https://pjreddie.com/darknet/install/
A.1) For weird image formats, use it with OPENCV (optional). For this, modify Makefile.

B) If using opencv and opencv-python, it comes already with darknet accessible:

**Weights and cfg**

Get the **weights** from [here](https://pjreddie.com/media/files/yolov3.weights):

```
wget https://pjreddie.com/media/files/yolov3.weights
```

Get the yolov3.cfg from [here](https://github.com/pjreddie/darknet/blob/master/cfg/yolov3.cfg):

```
wget https://github.com/pjreddie/darknet/blob/master/cfg/yolov3.cfg
```

Check in the [Darknet github](https://github.com/pjreddie/darknet/tree/master/cfg) for more cfgs.

**COCO dataset**

COCO dataset is trained on 200,000 images of 80 different categories (person, suitcase, umbrella, horse etc…). It contains images, bounding boxes and 80 labels. COCO can be used for both object detection, and segmentation, we are using it for detecting people. 

!!! Note
    We will be using the COCO dataset. You will find `coco.names` in the repository

## References

- [COCO - Common Objects in Context](https://github.com/cocodataset/cocoapi)
- [Learning Computer Vision Basics in Excel](https://github.com/amzn/computer-vision-basics-in-microsoft-excel)
- [Corner Detector](https://en.wikipedia.org/wiki/Harris_Corner_Detector)
- [Real time person removal](https://github.com/jasonmayes/Real-Time-Person-Removal)
- [Introduction to descriptors](https://gilscvblog.com/2013/08/18/a-short-introduction-to-descriptors/) and [Tutorial on descriptors](https://gilscvblog.com/2013/08/26/tutorial-on-binary-descriptors-part-1/) 
- [GilCV Code Examples](https://gilscvblog.com/code/)
- [Viola-Jones face detection and tracking explained](https://www.youtube.com/watch?v=WfdYYNamHZ8)
- [Using non-free detectors](https://github.com/godloveliang/YOLOv3-Object-Detection-with-OpenCV)

### Model Zoos

- [Caffe Model Zoo](https://github.com/BVLC/caffe/wiki/Model-Zoo)
- [Open VINO](https://github.com/opencv/open_model_zoo/)
- [Open VINO docs](https://docs.openvinotoolkit.org/latest/index.html)

### PIL/Pillow

These are libraries for managing images in python (you can still use opencv) and achieve similar functions.

- [Python Imaging Library](https://www.pythonware.com/products/pil/)
- [Pillow (Fork - recommended)](https://pillow.readthedocs.io/en/stable/)

!!! Note
    Installation instructions [here](https://pillow.readthedocs.io/en/stable/installation.html)

### Working on a Pi

- [Rpi camera do's and dont's](https://www.pyimagesearch.com/2016/08/29/common-errors-using-the-raspberry-pi-camera-module/)
- [Optimizing opencv for the pi](https://www.pyimagesearch.com/2017/10/09/optimizing-opencv-on-the-raspberry-pi/)
- [Face Recognition](https://www.pyimagesearch.com/2018/06/25/raspberry-pi-face-recognition/)
- [Object Detection with DL](https://www.pyimagesearch.com/2017/10/16/raspberry-pi-deep-learning-object-detection-with-opencv/)
- [Facial landmarks](https://www.pyimagesearch.com/2017/10/23/raspberry-pi-facial-landmarks-drowsiness-detection-with-opencv-and-dlib/)
