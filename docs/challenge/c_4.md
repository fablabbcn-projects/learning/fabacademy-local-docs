# Micro Challenge IV

## Intro presentation

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSOUQIFYEiIxNBgoaC5a2kqiZ8XihyqF-mYbrPPcauXHfoEqElzl887DmwS9p-pO6fFqkbM8esOYBtM/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="430" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


## Goal
**"Integrated prototype" that helps your MDEF Project /Fest and integrates a range of units covered. (Minimum three )**

### Must
Long term dissemination plan, need to explain strategy you will use to share with future users your project (licences, tools, communication, etc)

### What does "dissemination plan" mean?
It means how will you raise awareness of your project amongst the target group. The dissemination plan might answer questions such as: who is your project for, how is it funded, is there a license you chose or something else to protect intellectual properties of your project, is there a business plan, how would you fund scaling up, etc.


### Impact
- Preparation: 4h
- Dedication time: 24h
- By Pairs
- Outcomes: Individual reflection (post) & Project Repository


### Weekly plan
![](assets/c_3-6fbb97e4.png)


## Documents

![](assets/c_1-7dcc426c.png)

- [Miro](https://miro.com/app/board/uXjVOwtpzIE=/?share_link_id=3582000539892)

![](assets/c_mt_feedback-5cd64af2.png)

- [Link to Spreadsheet](https://docs.google.com/spreadsheets/d/1MkyGEFZoNPbxHTipImWIy3Hs9n1a_4EADSqdEmrq6gs/edit?usp=sharing)
