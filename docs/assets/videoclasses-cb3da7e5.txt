00:23:17	FABACADEMY_BCN ZOOM:	https://randomnerdtutorials.com/esp32-esp8266-publish-sensor-readings-to-google-sheets/esp32 to google sheets by ifttt
00:24:16	Benjamin S:	Can somebody send the link to this HackMD page?
00:32:18	xavi:	yes
00:32:33	xavi:	https://hackmd.io/ndksk2bfQa2JtiwMQytPtQ?view
00:35:45	Georgia Restou:	I don't have it 
00:35:57	Pablo Zuloaga:	I don´t
00:35:58	Naty Baránková:	me neither :(
00:35:59	Georgia Restou:	i think no one from MDEF got an email
00:36:02	Caroline Rudd:	Me neither
00:36:54	daphne:	But didn’t Hala or morale sent us the email?
00:37:04	daphne:	Mitale *
00:39:05	Oscar Gonzalez:	http://netfabbcn.hopto.org:9393
00:39:37	Oscar Gonzalez:	https://gitlab.com/fablabbcn-projects/learning/multinetworking/-/tree/master/remote
00:41:23	Oscar Gonzalez:	http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/extras/week14/diynetworks
00:44:32	FABACADEMY_BCN ZOOM:	#include <WiFi.h>
00:48:09	Marco Cataffo:	oscar can you zoom in the web page please?
00:48:37	Benjamin S:	Marco and his 20pixel computer
00:48:50	David Prieto:	Marco is using a smartwatch from Casio
00:48:59	David Prieto:	unbreakable, btw
00:55:33	Mitalee Parikh:	the diy network class link on local page isn’t working for me
00:55:34	Naty Baránková:	mqtt_server is mqtt_broker?
00:56:07	FABACADEMY_BCN ZOOM:	yes
00:56:15	David Prieto:	http://netfabbcn.hopto.org:9393/ui/
00:56:25	Laura Freixas Conde:	thank you!
00:58:39	Caroline Rudd:	can I use my mega2560 board for this?
01:01:11	Naty Baránková:	#include <ESP8266WiFi.h> this one doesn’t show for me
01:01:23	Naty Baránková:	Which library should I install and include for wifi pleas?
01:03:34	FABACADEMY_BCN ZOOM:	THESE ARE ALL THE WIFI LIBRARIES
01:03:36	FABACADEMY_BCN ZOOM:	https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.jsonhttp://arduino.esp8266.com/stable/package_esp8266com_index.jsonhttps://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.jsonhttps://adafruit.github.io/arduino-board-index/package_adafruit_index.jsonhttp://drazzy.com/package_drazzy.com_index.jsonhttps://www.mattairtech.com/software/arduino/beta/package_MattairTech_index.jsonhttps://dl.espressif.com/dl/package_esp32_index.jsonhttps://mcudude.github.io/MiniCore/package_MCUdude_MiniCore_index.jsonhttp://arduino.esp8266.com/stable/package_esp8266com_index.json
01:09:20	Naty Baránková:	for me. Its connecting to wifi for really long time
01:11:21	Caroline Rudd:	a mega2560 board won’t work will it?
01:11:41	Benjamin S:	Naty make sure you use you 2.4GHz wifi
01:11:50	Benjamin S:	Doesn’t work on 5GHz
01:12:47	Naty Baránková:	how? :D
01:13:38	Marco Cataffo:	ignored. wasted
01:14:00	David Prieto:	what happened Marco?
01:14:46	Pablo Zuloaga:	supposedly I´m connected
01:14:51	Benjamin S:	A lot of people have two WiFi’s names like Orange-4353 and Orange5G-4353, with 5G second name
01:19:10	Andrea Bertran:	"Attempting MQTT connection...failed, rc=5 try again in 5 seconds" ?
01:19:43	David Prieto:	@Beny I recieve your data. What is the range of it?
01:20:58	Hala Alzawaydeh:	https://github.com/esp8266/Arduino
01:24:35	Naty Baránková:	My code is not recognising espClient and is asking if I meant DSN client
01:25:00	Hala Alzawaydeh:	I'm getting an error message about my wifi "'\stray242' in program" anyone know what that means?
01:26:44	Georgia Restou:	I am so lost
01:28:36	David Prieto:	the stray 242 you recieved by the serial? or while compiling?, Hala?
01:29:11	Hala Alzawaydeh:	While trying to compile
01:29:30	Hala Alzawaydeh:	its highlighting the ssid line
01:31:37	Juanita Pardo García:	Can someone please tell me the name of the ESP WROOM 32 library? I already copied the links but can’t seem to find it
01:33:42	Benjamin S:	Hala, online somebody says “If the error message referred to a stray \342 in the code it is most likely to have been caused by copying the code from a Web page which has Unicode characters in it.”
01:34:03	Benjamin S:	did you copy and paste your WiFi name from somewhere? Is there any weird characters?
01:34:08	Pablo Zuloaga:	Yes, same happened to me…what Benjy says
01:34:20	Hala Alzawaydeh:	Yess I had a weird looking "" sign 
01:34:22	Hala Alzawaydeh:	thanks!
01:34:41	arman:	https://github.com/256dpi/processing-mqtt
01:35:14	FABACADEMY_BCN ZOOM:	https://github.com/256dpi/grasshopper-mqtt
01:35:15	David Prieto:	ah, yeeh, the "" it's tricky to paste them
01:35:28	Benjamin S:	Juanita, did you add the links to the Board Managers Library and then installed the ESP32 in Tools > Board > Boards Manager?
01:35:37	FABACADEMY_BCN ZOOM:	Benjamin, I got the same problem yesterday
01:35:51	FABACADEMY_BCN ZOOM:	I just erased it and write it again in Arduino interface and it worked
01:35:58	FABACADEMY_BCN ZOOM:	and restart it
01:36:05	Benjamin S:	Yeah it’s better to type it straight in
01:36:18	Benjamin S:	So you’re sure to have UTF-8 characters
01:36:42	David Prieto:	Benjamin, I'm recieving your data I'm puttingn now the output, can you tell me what are the thresholds?
01:36:49	Benjamin S:	ah sorry
01:36:56	David Prieto:	don't worry = )
01:37:05	Benjamin S:	Well, the min and max value of ESP32 are 0 to 4095
01:37:06	FABACADEMY_BCN ZOOM:	@ andrea you need to type correctly the mqtt server-user-pass on the header of your Arduino file
01:37:16	FABACADEMY_BCN ZOOM:	the correct lines are in the email
01:37:21	Benjamin S:	but at the moment I’m oscillating around 1000 to 2000
01:37:22	Andrea Bertran:	ya ya lo hice 
01:37:31	Benjamin S:	But I can move the potentiometer and make it lower and higher if you want
01:37:32	David Prieto:	ok, is it random or something that you can actívate if I tell you "actívate"?
01:37:41	David Prieto:	ah, ok
01:37:58	Zoe:	Edu how do we install the link of the libraries that you sent? If I open it, it is code right?
01:38:09	David Prieto:	keep it like it is and when I tell you put the power over 3000
01:53:13	Benjamin S:	https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json
http://arduino.esp8266.com/stable/package_esp8266com_index.json
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
https://adafruit.github.io/arduino-board-index/package_adafruit_index.json
http://drazzy.com/package_drazzy.com_index.json
https://www.mattairtech.com/software/arduino/beta/package_MattairTech_index.json
https://dl.espressif.com/dl/package_esp32_index.json
https://mcudude.github.io/MiniCore/package_MCUdude_MiniCore_index.json
https://dl.espressif.com/dl/package_esp32_index.json
01:54:57	Benjamin S:	Try and installhttps://dl.espressif.com/dl/package_esp32_index.json
01:54:59	Benjamin S:	oops
01:55:15	Benjamin S:	If anybody has a problem try and delete all the lines and only install
01:55:16	Benjamin S:	https://dl.espressif.com/dl/package_esp32_index.json
02:04:47	Caroline Rudd:	can I get help next? Lol
02:06:02	Oscar Gonzalez:	pip install paho-mqtt
02:13:17	Naty Baránková:	Can you send a link to dashboard please
02:13:56	Benjamin S:	http://netfabbcn.hopto.org:9393/ui/
02:13:58	Oscar Gonzalez:	http://netfabbcn.hopto.org:9393/ui
02:15:39	Caroline Rudd:	Go ahead daphne
02:17:56	Naty Baránková:	IndentationError: unindent does not match any outer indentation level
02:17:59	Naty Baránková:	What does this mean ?
02:18:37	Hala Alzawaydeh:	i th9ink open and closing indention don't match (?)
02:21:23	arman:	https://platformio.org/lib/show/617/MQTTPlatformio examples if anyone intrested
02:22:22	Oscar Gonzalez:	pip3 install pyserial
02:23:06	Andrea Bertran:	Yes, potentiometer
02:25:42	Andrea Bertran:	Edu, 4000?
02:26:50	Zoe:	Arduino: 1.8.12 (Windows Store 1.8.33.0) (Windows 10), Board: "NodeMCU 1.0 (ESP-12E Module), 80 MHz, Flash, Legacy (new can return nullptr), All SSL ciphers (most compatible), 4MB (FS:2MB OTA:~1019KB), 2, v2 Lower Memory, Disabled, None, Only Sketch, 115200"mqtt:9:1: error: stray '\342' in program const char* mqtt_broker= “netfabbcn.hopto.org”; ^mqtt:9:1: error: stray '\200' in programmqtt:9:1: error: stray '\234' in programmqtt:9:1: error: stray '\342' in programmqtt:9:1: error: stray '\200' in programmqtt:9:1: error: stray '\235' in programmqtt:10:1: error: stray '\342' in program const char* mqtt_user = “students”; ^mqtt:10:1: error: stray '\200' in programmqtt:10:1: error: stray '\234' in programmqtt:10:1: error: stray '\342' in programmqtt:10:1: error: stray '\200' in programmqtt:10:1: error: stray '\235' in programmqtt:11:1: error: stray '\342' in program const char* mqtt_pass = “fablabbcnisnice”; ^mqtt:11:1: error: stray '\200' in programmqtt:11:1: error: stray
02:30:54	Oscar Gonzalez:	“input/naty/“ + str(sensor_reading)
02:33:46	Caroline Rudd:	how do we declare the callback again?
02:35:01	Oscar Gonzalez:	is the function in the example, you can put it before the setup()
02:41:10	Pablo Zuloaga:	Why mine doesn’t´t appears in the “speedometer” kind bars?
02:42:03	Benjamin S:	Probably because Edu didn’t add it
02:42:08	Oscar Gonzalez:	http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/extras/week14/diynetworks/#code-example_1
02:42:10	Benjamin S:	he has to do it himself, it’s not automatic
02:42:20	Oscar Gonzalez:	I add it myself, one second
02:42:31	Pablo Zuloaga:	thx Benjy
02:42:35	Oscar Gonzalez:	http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/extras/week14/diynetworks/#code-example_1
02:42:42	David Prieto:	If anybody want's to try connect me I'm suscribed to "output/david" and play it's over 3000 and stop it's let's that 3000
02:42:53	David Prieto:	right now I'm going to document all this that it's fresh
02:45:24	Zoe:	guys I have an error "setup_wifi" was not declared in the scope
02:46:40	David Prieto:	that's probable an extra "}" somewhere
02:47:07	David Prieto:	because you closed too much or too less so it doesn't read that function
02:47:11	David Prieto:	(it happened to me)
02:49:46	Zoe:	do I have to write something in the parenthesis of setup_wifi(); ?
02:50:40	Caroline Rudd:	What is “client does not name a type” error?
02:52:17	David Prieto:	@Zoe, no you don't need to because that function (That it's the last one) doesn't need any arguments to work
02:53:01	FABACADEMY_BCN ZOOM:	no zoe you don´t
02:55:21	Hala Alzawaydeh:	Attempting MQTT connection...connectedAttempting MQTT connection...failed, rc=-2 try again in 5 seconds
02:55:33	FABACADEMY_BCN ZOOM:	hala, could be 
02:55:41	FABACADEMY_BCN ZOOM:	wrong mqtt
02:55:52	FABACADEMY_BCN ZOOM:	server-user or pass
02:56:16	Hala Alzawaydeh:	it was working then i changed the sensor and this happened
02:56:44	FABACADEMY_BCN ZOOM:	maybe you erased a coma or ; somewhere
02:56:50	FABACADEMY_BCN ZOOM:	when you defined the sensor
02:57:42	arman:	what is the different between MQTT/MQTTS/MQTTWS/MQTTWSS?
02:58:18	David Prieto:	Some have more random letters some have less. It's OBVIOUS ; P
02:58:41	arman:	:)))
03:01:02	Benjamin S:	I would guess in order it’s MQTT, then MQTT Secure, MQTT WebSocket and MQTT WebSocket Secure
03:01:05	Benjamin S:	I am guessinh
03:04:01	elsa garduño:	i am getting this: elsamariag@Elsas-MacBook-Pro python % python remote_example.py
  File "remote_example.py", line 18
    print(f"Connected With Result Code: {rc}")
03:04:15	elsa garduño:	does anyone knows what’s happening?
03:04:44	elsa garduño:	does anyone know*
03:06:53	Oscar Gonzalez:	sensor_reading = str(ser.readline().decode('utf-8').replace("\r\n", ""))
03:08:16	daphne:	Oscar are you receiving it?
03:08:52	Pablo Zuloaga:	y = map(x, 1, 50, 50, -100)
03:09:31	Pablo Zuloaga:	y = map(x, 0, 2000, 0, 250)
03:10:13	David Prieto:	lo declaras con amor y cariño, Pablo < 3
03:10:15	David Prieto:	;P
03:10:19	David Prieto:	(sorry I can't resist)
03:10:44	Benjamin S:	Pablo check this: https://www.arduino.cc/en/Tutorial/PWM
03:11:50	FABACADEMY_BCN ZOOM:	brightness = map(strPayload.toInt(), 0, 200, 0, 255);analogWrite(led, brightness);
03:11:54	Pablo Zuloaga:	hahaha, una relación de amor y odio David hahaha
03:11:55	FABACADEMY_BCN ZOOM:	something like this
03:11:56	Pablo Zuloaga:	thanks Benny
03:12:15	Pablo Zuloaga:	Gracias Edu
03:14:16	FABACADEMY_BCN ZOOM:	https://techtutorialsx.com/2017/06/15/esp32-arduino-led-pwm-fading/
03:17:02	daphne:	my internet gets stuck
03:18:14	arman:	did anyone try mqtt with processing??
03:18:53	David Prieto:	you're on a row, Arman xD
03:19:16	David Prieto:	but that.. that seems pretty cool thing to wire xD
03:19:33	arman:	:)) I am trying everything
03:19:51	Naty Baránková:	my sister and her bf just arrived for lunch,…I have to go already sorry …thank you so much for help! Have a good day guys all of you :)
03:30:43	daphne:	Beer is food
03:39:31	Oscar Gonzalez:	https://gitlab.com/fablabbcn-projects/learning/multinetworking/-/tree/master/remote
