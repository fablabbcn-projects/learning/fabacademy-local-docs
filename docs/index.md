# Welcome!

<iframe width="100%" height="400" src="https://www.youtube.com/embed/Z5nOlFDkAAE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

About this documentation:

- **Welcome**: you will find information about the Fabacademy program, for either [Fabacademy students](course_info/fabacademy/what_is_it.md) or the [_Master in Design for Emergent Futures_](course_info/mdef/things_to_know.md) program. Also some [useful links](useful_links) and some information for [foreign students coming to Barcelona](living_barcelona.md)
- **Weekly classes**: what you came for. The documentation of each week of the Fabacademy program, and links to the global sessions
- **Video Classes**: Video recordings of the classes
- **Guides**: Step-by-step guides (WIP)

<!--

<p>Whenever you are panicking, <strong>use this button:</strong></p>

<a id="bookmarklet" href="javascript:(function()%7Bjavascript:var%20s%3Ddocument.createElement(%27script%27)%3Bs.setAttribute(%27src%27,%27https://nthitz.github.io/turndownforwhatjs/tdfw.js%27)%3Bdocument.body.appendChild(s)%3B%7D)()%3B">
    <div style="overflow: hidden; width=100%">
        <img src="https://minddetoxtunbridgewells.files.wordpress.com/2013/03/panic-button.jpg">
    </div>
</a>-->

<style>

    #bookmarklet {
      display: block;
      font-weight: bold;
      text-tranform: uppercase;
      color: white;
      text-align: left;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      -ms-border-radius: 0px;
      -o-border-radius: 0px;
      border-radius: 0px;
      border-bottom: 0px solid ;
      text-shadow: 0 1px 1px ;
      font-size: 16px;
      overflow: hidden;
      cursor: move;
      padding: 10px;
      -webkit-user-select: none;
      text-decoration: none;
      text-transform: uppercase;
      text-align: center;
    }

    #bookmarklet:hover {
      border-bottom: 2px solid ;
      text-shadow: 0 1px 1px ;
    }

    #bookmarklet:active {
      border-bottom: none;
      border-top: 2px solid ;
      text-shadow: 0 1px 1px ;
    }
</style>
