# 18. Invention, intellectual property, and income

![](../assets/ip.jpg)

### Class Week 18

- [invention, intellectual property, and income](http://academy.cba.mit.edu/classes/invention_IP_business/index.html)


## FABLAB BCN classes

### From All Rights Retained to All Rights Relinquished

- [IP discussion notes](extras/week18/ipnotes.md)

This is a copy of the FLU class notes. You can find the latest version [here](https://hackmd.io/s/HJZ9KGRoE)


### Governance and Licensing in Open Source

- [Debate](https://hackmd.io/Gv6AAZOCTASNP9h04w-uMA?both)
