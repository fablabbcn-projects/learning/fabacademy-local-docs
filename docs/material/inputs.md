# Input Devices

<p><a href="https://commons.wikimedia.org/wiki/File:Hall_sensor_tach.gif#/media/Archivo:Hall_sensor_tach.gif"><img src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Hall_sensor_tach.gif" alt="Hall sensor tach.gif"></a><br>De <a href="https://en.wikipedia.org/wiki/User:IMeowbot" class="extiw" title="wikipedia:User:IMeowbot">IMeowbot</a> de <a href="https://en.wikipedia.org/wiki/" class="extiw" title="wikipedia:">Wikipedia en inglés</a> - Transferido desde <span class="plainlinks"><a class="external text" href="https://en.wikipedia.org">en.wikipedia</a></span> a Commons., <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=477535">Enlace</a></p>

### Local Classes

#### Input Devices

What is a sensor, sensor types, how to choose them. Theory, applications and examples.

- [Input devices basics](../extras/inputs/local)

#### DIY Input Sensors

How to build your own DIY sensors that you can build at home with household materials.

- [DIY Input Devices](../extras/inputs/diy)

#### From signals to events

Make stuff with your signals to detect events:

- [From signals to events](../extras/inputs/dsp)

## References and resources

### Arduino class I

<script async class="speakerdeck-embed" data-id="419501f815f04fd3a568e7197c94eb3b" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

Check the [class slides](https://speakerdeck.com/pral2a/arduino-an-introduction-updated)[Guillem class original location](https://hackmd.io/s/r18A2AOsM)

### Examples Sketches

**Check all the examples in [Github](https://github.com/fablabbcn/arduino-workshops/tree/master/On%20the%20go%20Examples/Arduino%20Basics%20-%20Fab%20Academy%2018).** Each folder contains the [Arduino](https://www.arduino.cc/en/Main/Software) sketch and the required circuit in [Fritzing](http://fritzing.org/download/).

**Soft sensor**
- [Textile-flexible sensor](http://archive.fabacademy.org/archives/2016/fablabbcn2016/students/139/htm/week_11.htm)
- [Wearables sensors](http://www.kobakant.at/DIY/)

**Hall Sensor**
- [Sensors with visualization in grasshopper](http://fabacademy.org/archives/nodes/barcelona/perona.francesca/htm/19_week.html)
- [Magnetic sensor bicycle speedometer](http://archive.fabacademy.org/archives/2017/fablabbcn/students/141/week%2011.html)

**Capacitive Sensing**
- [BIO PLANT SENSING INTERFACE](http://fab.cba.mit.edu/classes/863.13/people/Jonathan_Grinham/index.html)
- [CUSTOM TOUCHPAD](http://archive.fabacademy.org/archives/2017/fablabbcn/students/51/w13.html)
- [Touch SLIDER](http://fab.cba.mit.edu/classes/863.13/people/akashbad/week9/)
- [Flexible copper sensor](http://fabacademy.org/archives/2015/eu/students/vloet.frank/week10.html)
- [Material stress/load sensor](http://archive.fabacademy.org/2017/fablabbcn/students/141/Final%20Project.html)
- [Disney research on touch interactions](https://la.disneyresearch.com/publication/touche-enhancing-touch-interaction-on-humans-screens-liquids-and-everyday-objects/)

### Extra material

**Esp8266 Node MCU Connection PinOut**
![](extras/inputs/assets/esp8266.jpg)

**Esp32 Node MCU Connection PinOut**
![](extras/inputs/assets/ESP32-Pinout.jpg)

**Arduino Uno Connection PinOut**
![](extras/inputs/assets/fn9wzcdn.jpg)

**Tipical Commercial sensors**
![](extras/inputs/assets/sensors.jpg)
