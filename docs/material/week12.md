#12. Interface and application programming

![](https://www.pushing-pixels.org/wp-content/uploads/2013/06/sony-selects.jpg)

### Local Class 2022

- [Documentation for the class](https://hackmd.io/kGwdO2gHTt2h1-ppn5h_fg?both)

### Introduction
- [Overview & Interface tools review](https://hackmd.io/@fablabbcn/HJQHTMJdd)


### MIT APP INVENTOR

- [MIT APP INVENTOR class](../extras/week12/appinventor)

### ESP WEB SERVER

- [EXPRESSIF ESP WEB SERVER CLASS LINK](../extras/week12/espweb)

### Processing

- [Processing class](../extras/week12/processing)

### A-frame

- [A-frame class](../extras/week12/aframe)

### Blender

- [Blender IO class](../extras/week12/blender)

## References and resources

[BLINK ECOSYSTEM](https://blynk.io/)
[BLINK EXAMPLE](https://examples.blynk.cc/?board=ESP32&shield=ESP32%20WiFi&example=Widgets%2FLED%2FLED_Fade)
[ANOTHER BLINK EXAMPLE](https://maker.pro/arduino/projects/how-to-remotely-control-an-arduino-with-the-blynk-app)

### Documentation Student Reference:

[http://fabacademy.org/archives/2015/eu/students/hisatsune.taichi/classes14.html](http://fabacademy.org/archives/2015/eu/students/hisatsune.taichi/classes14.html)

### Tutorials

- [A simple serial stub patch for Pure Data](http://archive.fabacademy.org/archives/2017/doc/puredataSerial.html)
- [A simple serial stub sketch for Processing](http://archive.fabacademy.org/archives/2017/doc/SimpleSerialStub.pde)
- [Introduction to Python](http://archive.fabacademy.org/archives/2017/doc/python.html)
- [Reading serial data with Python](http://archive.fabacademy.org/archives/2017/doc/python-serial.html)
- [Introduction to wxPython, and reading and drawing serial data with wxPython](http://archive.fabacademy.org/archives/2017/doc/wxpython.html)
- [Reading serial data with Kivy (Python)](http://archive.fabacademy.org/archives/2017/doc/python-kivy.html)
- [Python Web-based Serial Console using WebSockets](http://archive.fabacademy.org/archives/2017/doc/WebSocketConsole.html)

### APP INVENTOR

- [http://appinventor.mit.edu/explore/_](http://appinventor.mit.edu/explore/)

**Requirements:**

- Online
- Android Devices
- Google Account

**About**

[_MIT App Inventor_](http://appinventor.mit.edu/) _is a blocks-based programming tool that allows everyone, even novices, to start programming and build fully functional apps for Android devices. These resources provide example projects to start building apps:_

**Learn**

- [App Inventor Tutorials and Book Chapters](https://sites.google.com/site/appinventor/projects)
- [App Inventor Tutorials and Advanced Examples](http://puravidaapps.com/tutorials.php)
- [Tutorials - App Inventor for Android](http://beta.appinventor.mit.edu/learn/tutorials/)
- [App Inventor Tutorials by Imagnity](http://www.appinventorblocks.com/)
- [App Inventor Book, Classic version (PDF)](http://www.appinventor.org/projects)
- [App Inventor Lessons: Racing Game](http://nebomusic.net/appinventorlessons/racinggame/)
- [tAIR - The App Inventor Repository: Creating your First Game](http://www.tair.info/faqs-and-how-tos/how-tos/creating-your-first-game/)
- [Explore MIT App Inventor: Space Invaders](http://appinventor.mit.edu/explore/ai2/space-invaders.html)
- [APP Inventor Cards](http://appinventor.mit.edu/explore/resources/beginner-app-inventor-concept-cards.html)
- [Basic Bluetooth Communication using App Inventor](http://pevest.com/appinventor2/?p=520)

### Processing

- Web processing: [https://processing.org/](https://processing.org/)
- Fab Academy Students references:
    - [http://archive.fabacademy.org/archives/2016/greenfablab/students/357/week16.htm](http://archive.fabacademy.org/archives/2016/greenfablab/students/357/week16.htm)
- **Example arduino with humidity plotting in processing**. Files [here for arduino](../extras/week12/assets/processing/arduino_processing_hummidity.ino) and [here for processing](../extras/week12/assets/processing/processing_hummidity-20200429T103243Z-001.zip)

### UNITY 3D

- [Web](https://unity3d.com)

**About**

- [_cross-platform_](https://en.wikipedia.org/wiki/Cross-platform)
- [_game engine_](https://en.wikipedia.org/wiki/Game_engine)
- [Unity Technologies_](https://en.wikipedia.org/wiki/Unity_Technologies)
- [wiki](https://en.wikipedia.org/wiki/Unity_(game_engine))
- [_video games_](https://en.wikipedia.org/wiki/Video_game)
- [_simulations_](https://en.wikipedia.org/wiki/Computer_simulation)
- [_PC_](https://en.wikipedia.org/wiki/Personal_computer)
- [_consoles_](https://en.wikipedia.org/wiki/Video_game_console)
- [_mobile devices_](https://en.wikipedia.org/wiki/Mobile_device)
- [_websites_](https://en.wikipedia.org/wiki/Website)

**Learn**

- [http://fab.cba.mit.edu/classes/863.15/section.Harvard/people/Hamman/week11.html](http://fab.cba.mit.edu/classes/863.15/section.Harvard/people/Hamman/week11.html)
- [https://unity3d.com/learn](https://unity3d.com/learn)
- [Unity 3D - Code Samples and Complete Projects](https://unity3d.com/learn/resources/downloads)

#### How to integrate Arduino with Unity

- [Tutorial](http://www.alanzucconi.com/2015/10/07/how-to-integrate-arduino-with-unity/)

### JAVASCRIPT

- [https://musiclab.chromeexperiments.com/](https://musiclab.chromeexperiments.com/)
- [Tonejs](https://tonejs.github.io/)
- [Threejs](https://threejs.org/)
- [D3](https://d3js.org/)

## Other year FABLAB BCN classes

[Hackmd](https://hackmd.io/s/HJvhtz0oV)
