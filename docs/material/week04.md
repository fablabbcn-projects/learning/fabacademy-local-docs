# Electronics production

## Classes

### Global Class

- [Electronics production](http://academy.cba.mit.edu/classes/electronics_production/index.html)

![](assets/microcontroller.png)

!!! Note "Assignments and assessment"

    **Fabacademy students**: visit the [assessment](https://fabacademy.org/2022/nueval/electronics_production.html)

    _You need to produce 1 of the three options from below. If you want to have all the options for programmers (to be able to program any chip), you need to make 3 boards and 2 adapters:_

    - **FabIsP Programmer** (any variant but we recommend the recommended fabisp)
    - **FTDI SERIAL** (hello.USB-serial.FT230X) + **adapter UPDI** (hello.serial-UPDI.FT230X)
    - **CMSIS-DAP** (hello.CMSIS-DAP.10.D11C) + adapter 10-4 pins (hello.SWD.10-4)

    [Here is the link to all the boards](http://academy.cba.mit.edu/classes/embedded_programming/index.html#programmers)

### Local Class

#### Electronic production Overview

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTev9_NuHFuMPZfi600Ir06N7tfvATxlJWRbpkWVqylh2uZnjm8zAIzN9XSB2Ha1lemkSSj1pYVhEmb/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


#### Basics of electronics

- [The physics behind electronics](extras/week04/physics.md)

#### Microcontroller basics

- [What is a microcontroller and what is a programmer?](extras/week04/whatisamicrocontroller.md) - or, what am I doing this week appart from _wax-on, wax-off_?

#### Available microcontrollers and worflows

- [Microcontrollers Summary](extras/week04/microcontrollerssummary.md) - or, where should I go next time I forget how to connect my board?

**Using EDBG to Program D11C Boards**

<iframe width="960" height="420" src="https://www.youtube.com/embed/mqkbcEV7ses" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Other years

* [Choose the FABISP libraries](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp_models.html)
* [FabAcademy Resources page-attiny libraries/](https://gitlab.fabcloud.org/pub/projects/tree/master)

<script async class="speakerdeck-embed" data-id="85a73ebd554c45a2808a6768fb1c2914" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

## FAQ

!!! Note "Does the programmer need to work to complete the assignment?"
    Yes, by the end of Embedded Programming week. Because you will program your board from Electronics Design week with it. It is highly recommended that you attempt it this week and explain ALL the debugging that you did to try to find out problems.

!!! Note "Can we use the locally compiled version of Fab modules to mill PCBs and moulds?"
    There are no requirements for what you use for CAD and CAM, and certainly not to use mods. Having said that, the mods algorithms are more efficient than the older fab modules, so something's wrong if they're taking more time for you. And mods is easier to configure, and shows the internals of the algorithms, so Neil recommends getting set up to use them.

## Resources

!!! Note "Curated references"
    - [Beautiful images of common chips](https://siliconpr0n.org/map/)
    -[Recommended fabrication](http://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html#programming)
    - [What is make](https://www.gnu.org/software/make/manual/html_node/Introduction.html#Introduction)
    - [How to self-learn electronics](https://news.ycombinator.com/item?id=16775744)
    - [Circuit simulation](https://hackaday.com/2019/11/30/circuit-simulation-in-python/?mc_cid=5bce5b5fcb&mc_eid=348b67951f)
    - [Ultimate Electronics tutorial](https://ultimateelectronicsbook.com/#dependent-sources)
    - [Guerrilla electronics](http://lcamtuf.coredump.cx/electronics/)
    - [List of videos and books as an introduction to electronics and computers (doc)](https://docs.google.com/document/d/1TS8hGFBqgIwLksClnXev__a6kn2-_dAEW7L0ZNUII5A/edit?usp=sharing)

### Fab Modules/MODS

* [FabModules App](http://fabmodules.org/)
* [How to use FabModules](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)
* [Mods App](http://mods.cba.mit.edu/) *The newer fabmodules*
* [how to use MODS step by step tutorial](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/mods.html)

### Precision milling CNC/VINYL tutorials

- [How to use SRM-20 Roland](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/srm20_windows.html)
- [How to use MDX-20 Roland](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/mdx20.html)
- [How to use Vinyl cutter for PCB´S](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/flexible_pcb_windows_mac.html)

### Soldering

- [Basics Soldering](https://youtu.be/vIT4ra6Mo0s)
- [Soldering Tutorial](https://www.youtube.com/watch?v=b9FC9fAlfQE)
- [Instructables: How to Solder](http://www.instructables.com/id/How-to-solder/)
- [Student experience guide on how to solder](http://archive.fabacademy.org/archives/2016/fablabseoul/students/62/a04.html)

### Books

* [Essential Mathematics For Computational Design](https://drive.google.com/file/d/1j8TYIhsTqDvvL4RVTRxlug0CxeAG-yl7/view?usp=sharing)
* [Grasshopper Primer](https://drive.google.com/drive/u/0/folders/1ZzBjZiY1CvF747UFQPs-I6DZ01iTd26P)
* [Grasshopper_Sung](https://drive.google.com/drive/u/0/folders/1ZzBjZiY1CvF747UFQPs-I6DZ01iTd26P)

## Tutorials

- [FabISP: Electronics Production](http://archive.fabacademy.org/archives/2017/doc/electronics_production_FabISP.html)
- [How to mill boards with Modela](https://www.youtube.com/watch?v=XdamEhs2RIk&list=PL-xEsC0ZUCUM42QNHaOOdoOwYg0j251dU)
- [Milling with Fab Modules and Roland Modela MDX-20](https://www.youtube.com/watch?time_continue=218&v=rzNpwbv7HIU)
- [What are the fuses?](http://archive.fabacademy.org/archives/2017/doc/fuses.html)
- [Resistor SMD Code](http://www.resistorguide.com/resistor-smd-code/)
- [FabISP: Programming WINDOWS users](http://archive.fabacademy.org/archives/2017/fablabverket/students/100/web/assignments/week4/index.html)
