# Computer aided design

![](../assets/cad.jpg)


## Global Class

- [Computer-aided design](http://academy.cba.mit.edu/classes/computer_design/index.html)

## Local Class

**2D MODELLING (CAD)**
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ5csrS5KhJpuSTXZ68necpgurNNrGsChd4a_uMaNnJoeJvJshRyxG1Uho8JtBcvWsxkMZK_noVYme2/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


**3D MODELLING (CAD)**
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTZPs5NWWGEzTZ9dqG25mBe_Z8V-EHMmXejexHXfqj8Ge9kCDA0rIWLT7wRH6ED1uh8j-kdcll5Qsbx/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### 2D Design Tools (Vector vs Raster)

* [Image compression for web size](https://www.imagemagick.org/)
* [Image compression online tool](https://tinyjpg.com/)
* [Online SVG vector drawing online](http://www.drawsvg.org/)
* [Online Raster edition program](https://pixlr.com/)
* [Online Vector edition program](https://vectr.com/)


### 3D Design Tools

**What is it?**

[Computer-aided design](https://en.wikipedia.org/wiki/Computer-aided_design)(CAD)

* [Sketch-based modeling](https://en.wikipedia.org/wiki/Sketch-based_modeling)
* [Freeform surface modelling](https://en.wikipedia.org/wiki/Freeform_surface_modelling)
* [Parametric design](https://en.wikipedia.org/wiki/Parametric_design)

[Computer-aided manufacturing](https://en.wikipedia.org/wiki/Computer-aided_manufacturing)(CAM)

[Computer-aided engineering](https://en.wikipedia.org/wiki/Computer-aided_engineering) (CAE)

[Generative Design](https://www.youtube.com/watch?v=a6bDLMWlS98)

[STEP Files](https://en.wikipedia.org/wiki/ISO_10303-21)

**MESH VS NURBS**

![](../extras/week02/mn.jpg)

**Nurbs**

![](../extras/week02/NURBS_surface.png)

!!! info
    *Spline Modeling—or NURBS—generates accurate geometry with the smoothest surface. These geometries are mathematically defined by control points that result in complex curves to form surfaces around them. To adjust surfaces you manipulate control points. A lot of 3D scans will produce models using NURBS to create an exact replica. Therefore this method is ideal for CAD-based programs because its more accurate, takes up less data space and is easier to translate into different programs.*


**Mesh**

![](../extras/week02/720px-Mesh_overview.svg.png)

!!! info
    *Polygon Modeling creates a 3D object non-mathematically. It’s the preferred method of choice for STL (3D) printing. By connecting points in space you produce edges on a 2D surface, then you can combine and move the said surfaces to form a shell. So it’s not as accurate as NURBS, because it doesn’t average the curve between points. Instead it uses polygons—triangles or quadrilaterals—to fabricate the approximation of a smooth edge. This makes Polygon Modeling easy to manipulate, easy to work with in subdivisions and easy to attach. Object smoothness is achieved by increasing the number of polygons for full detail and density level control within a model. However, this control will impact file size. So note to self when converting NURBS into Polygon Modeling, when you set the resolution this will affect the number of polygons, which will result in the final file size. Other file formats include FBX and OBJ.*

![](../extras/week02/pt.png)

"A mesh is a collection of triangles and quadrilaterals. It is defined internally as a list of vertices (points) and faces (each face connects either three or four vertices). Meshes also support per vertex normals and colours, as well as a host of other properties.

A surface is, well, a surface. It consists of a single smooth area but may have custom edges. If a surface is just a (deformed) rectangle and it has no custom edges, it is called an ‘untrimmed’ surface. Surfaces with custom edges or holes punched into it is called a ‘trimmed’ surface.

A brep is one or more surfaces joined together. If a brep only contains a single face, then it is the same thing as a surface. For surfaces to be joined together into a brep, they need to share some of their edges."

**When Are NURBS Used?**

![](../extras/week02/n2.jpg)

I’m sure you’re thinking this all sounds great! Why don’t we use NURBS more often?

That’s because I have yet to go into the limitations behind NURBS, which there are many.

NURBS objects are only ever 4 sided. This is a limitation to the fact that a Polygon is usually an N-gon consisting of any number of sides up from 3.

This makes the process of modeling with polygons a much simpler process. Expanding on this polygon modeling is a lot easier to manipulate and change, which again makes for far easier construction of meshes.

NURBS objects are always separate from each other as well, making them difficult to attach together for one model.

Due to their perfect calculations of curvature and position, this means you will never see the seams between them. But it is still a limitation in their workflow. If you wish to animate a NURBS object, you would first need to convert it into a polygonal mesh so that you can weld joints together so they will not come apart.

It is also not possible to UV unwrap a NURBS object.

For this reason, if it’s a model you need to texture, it is better to use a polygon mesh. That way you have the ability to adjust how it’s projected onto the mesh.


#### Parametric Design

<script async class="speakerdeck-embed" data-id="a4b941d371fb4f7a83eb5ea14f443cf1" data-ratio="1.41241379310345" src="//speakerdeck.com/assets/embed.js"></script>

[Download PDF presentation](https://speakerd.s3.amazonaws.com/presentations/a4b941d371fb4f7a83eb5ea14f443cf1/ParametricDesignExplanationEnglish.pdf)

#### Blender Intro

![](assets/week02-a4b44af9.png)

[Link to class documentation](https://hackmd.io/ojtXMAVIQYyJJ86pPzzNpA)

#### Last years classes

- [Introduction to OpenScad by Xavi Domínguez](../extras/week02/openscad)

## Tutorials

### CAD - Resources

* **2D Design Tools**
     * [Inkscape](https://inkscape.org/en/) *(Free + Opensource Graphic Editor)*
        * [Inkscape Introduction](http://archive.fabacademy.org/archives/2016/doc/inkscape.html)
        * [Inkscape - Lecture](https://www.youtube.com/watch?v=ylHJbLId2cg)

     * [Gimp](https://www.gimp.org/) *(Free + Opensource Image Editor)*
        * [Gimp and Bitmap Introduction - Lecture](https://www.youtube.com/watch?v=wLSvubMGb8A)

     * [QCad](https://qcad.org/en/) *(Free + Opensource for CAD in 2D)*
        * [QCad Introduction - Lecture](https://www.youtube.com/watch?v=doDway3Cq1c)

* **3D Design Tools**
    * [3D CAD: 3D Modeling Tools for Beginners](http://archive.fabacademy.org/archives/2016/doc/3D_CAD.html)

    * [Blender](https://www.blender.org/) *(Opensource 3D creation)*
        * [Blender Introduction - Lecture](https://www.youtube.com/watch?v=e5XORqVkVlE)
        * [Blender Master Class 2017](https://www.youtube.com/watch?v=Y0xqculb124)

    * [Freecad](http://www.freecadweb.org/?lang=eng_EN) *(Free + Opensource parametric 3D modeler)*
        * [ Freecad Introduction - Lecture](https://www.youtube.com/watch?v=7nTPc4zFd9w)
        * [ Freecad for Beginners - Spanish!](https://www.youtube.com/watch?v=2_DbFzFV9D4&list=PLmnz0JqIMEzWQV-3ce9tVB_LFH9a91YHf)

    * [Rhinoceros 3D](http://www.rhino3d.com/en/) *(Commercial 3D Cad software)*
        * [ Rhinoceros 3D - Nurbs](https://www.youtube.com/watch?v=SXReLvfCA_Y)

  * **Grasshopper- Rhinoceros Resources**

    * [Grasshopper Master Class 2017](https://www.youtube.com/watch?v=mktp0ibtcTw&t=6815s)
        * [File - Grasshopper Master Class](https://drive.google.com/file/d/0B1WvpUt-D07aOGpGdHVkRF91NFdESlk5TUNNYXdBY0J0UFJR/view?usp=sharing)

    * [SolidWorks](http://www.solidworks.es/) *(Commercial 3D Cad Software)*
        * [Video tutorials for beginners, advanced, etc](https://www.youtube.com/channel/UCtwaWPOXEBysZLh1rrPzwFw)
        * [Lego tutorial by Ferdinand Meier](https://www.youtube.com/watch?v=6d2p6RXOMtA&t=21s)

    * [Onshape](https://www.onshape.com/)
    * [OpenScad](http://www.openscad.org/)
