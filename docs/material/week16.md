#16. Wildcard week

## Fablab BCN Classes
## Robotic arm fabrication

**Robot Programming with Robots plugin WIKI steps by step guide [Link](https://wiki.fablabbcn.org/ROBOTS)**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0BbDR5Q6XtwUwwY_Waz7m8NkvOilB-uvHqjXN-tI1qJkHj5iMTj5AzTMXOxdY5hcZaCC2m4NDI2QS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQsXRRYoknYFcZT_gCSPxGQl_l9ri6jXSgNlBzWkZhnncgI9Htsr_uytl-0tTWv6Yzy9BulVK0hhetS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

[DOWNLOAD LINK FOR ROBOTS PLUGIN FOR GRASSHOPPER & ROBOTS LIBRARY](https://drive.google.com/drive/folders/1wmBZL8tYLowkqEA6wXxM4Izn2tAjXYeh?usp=sharing)

[MORE INFO ON ROBOTIC ARM CLASS] (https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/clubs/makeclub/roboticarm/)


[Grasshopper Basic Script ROBOTS](extras/week16/assets/SCRIPT-RobotsBasics.gh)
[Grasshopper Advanced Script ROBOTS](extras/week16/assets/SCRIPT-RobotsAdvanced.gh)
[Grasshopper Full Script ROBOTS](extras/week16/assets/SCRIPT-RobotsFull.gh)


## Remote control of CNC
How to make a remote controlled CNC draw in the sand:

- [The CNC by the sea](https://gitlab.com/fablabbcn-projects/learning/the-cnc-by-the-sea/-/tree/master)

### FAQ:Can I make the assignment by hand?

!!! note ""
    - Although there can be manual processing steps, to count as digital fabrication the workflow should pass through design in a computer and some kind of automated output process. So programming the embroidery of circuits would count, but hand-stitching them wouldn't.

## References and resources

### Composites:

- [Vacuum Machine](http://nabuurs.com/det_producto.php?idMod=70&idioma=ES)
![](extras/week16/assets/week16-0727c015.jpg)
![](extras/week16/assets/week16-32f883de.jpg)
![](extras/week16/assets/week16-f606b045.jpg)

**Previous fabacademy students**

- http://fabacademy.org/archives/nodes/barcelona/hisatsune.taichi/classes12.html
- http://fabacademy.org/archives/nodes/barcelona/armengol_altayo.daniel/htm/12.html
- http://archive.fabacademy.org/archives/2016/fablabbcn2016/students/27/exercise14.html
- http://archive.fabacademy.org/archives/2016/fablabbcn2016/students/375/week14/
- http://fabacademy.org/archives/2015/eu/students/vloet.frank/week12.html
- http://archive.fabacademy.org/2016/charlottelatin/students/87/exercise14.html
- http://fabacademy.org/archives/2014/students/garita.robert/12.Composites.html
- http://fabacademy.org/archives/nodes/barcelona/tanner_pasetti.gabriel/week12.html

**Other interesting links:**
<iframe width="560" height="315" src="https://www.youtube.com/embed/7Oo2H-W7d6A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe src="https://player.vimeo.com/video/124022849" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

### Textile:

**Fabricademy 2017 Class 11 - Soft robotics - Lily Chambers**

<iframe src="https://player.vimeo.com/video/245973919" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

- Soft robotics [Fabricademy wiki](http://wiki.textile-academy.org/fabricademy2017/classes/softrobotics)

- [Lily Chambers presentation]( http://wiki.textile-academy.org/_media/fabricademy2017/classes/soft_robotics_final.pptx)

- [This is the assignment that we had to do]( http://wiki.textile-academy.org/_media/fabricademy2017/classes/soft_robotics_assignment_final.pptx)

**links**

- [Adrianna Cabrera presentation we had at the Fabricademy Bootcamp in Milan]( https://docs.google.com/presentation/d/1y7X3aREhmD9Hj9qdqsiK_SpWE57eFRcOmbxsnYAz06U/edit)
- [Here some quick video and pictures of the tests developed during the bootcamp in one day]( https://www.facebook.com/adriana.cabrera.1048/posts/10155251032951631)
- [Bioplastic](https://fabtextiles.org/the-secrets-of-bioplastic/)
