# Principles and practices - project management

<iframe width="560" height="315" src="https://www.youtube.com/embed/5iFnzr73XXk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Global Class

- [Principles and Practices](http://academy.cba.mit.edu/classes/principles_practices/index.html)
- [Project Management](http://academy.cba.mit.edu/classes/project_management/index.html)

## Local Class

**Introduction to web design / git-gitlab**
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTlR_gYYka8IYPYpUigS_Ji5jBFsbe2UxNxlnDpEQpYC8SaC5M6Pb4Orh9dsuJU0N3RDmNM-i7gi-ab/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Git

- [Git Basics](extras/week01/gitbasics.md)
- [Full Guide on Git for FabAcademy](http://archive.fabacademy.org/2017/at3flo/students/3/index.html#Get%20started%20with%20GIT)
- [Explain Git like I am 5 years old](https://hackernoon.com/understanding-git-fcffd87c15a3)

**VISUAL COMMANDS EXPLAINED**
- [Visual executable commands](http://www.ndpsoftware.com/git-cheatsheet.html#loc=workspace;)

### Web

- [Web Basics](extras/week01/webbasics.md)
- [Static Site Generators](extras/week01/ssgs.md)

### Project Management

- [How to plan a FabAcademy](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/week2-tips/)
