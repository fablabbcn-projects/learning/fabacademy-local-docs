# Electronics Design

## Classes

### Global Class

- [Electronics Design](http://academy.cba.mit.edu/classes/electronics_design/index.html)

!!! Note "Assignments and assessment"

    **Fabacademy students**: visit the [assessment](https://fabacademy.org/2021/docs/fabacademy-assessment/electronics_design.html)

### Local Class

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT57ZzInJanXhKj2wXPRF4Sm-psd4iwDuO4_lEl__x_bZZptALMSPi6HrRIvS21kT6GTn-s26sQWOtR/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


#### EDA

This week will be talking about EDAs. From [Wikipedia](https://en.wikipedia.org/wiki/Electronic_design_automation):

Traditionally, we have done two classes for EDAs in Fabacademy (Eagle and Kicad). We will only do Kicad from now on. Either way, you can find both classes here:

- [Autodesk EAGLE](extras/week06/eagle.md)
- [KiCad 5](extras/week06/kicad.md)

!!! Danger "Making classes easier"
    Please, come with the following software downloaded and installed in your machine:

    - [Kicad 5](http://kicad-pcb.org/download/)
    - A vector manipulation program (preferrably [Inkscape](https://inkscape.org/))
    - **Optional**: An image manipulation program (GIMP, Photoshop)


#### MDEF Example board

Here you can find an example header designed for the Huzzah ESP32 board, with a button, an LED and a 3 pin connection for an external sensor.

[Download Kicad files from here](../assets/ExampleHeader.rar)
![Schematic of the board](../assets/Schematic.png)
![PCB Layout](../assets/PCB-Layout.png)


## Resources

!!! Note "Curated references"
    - [All the libraries of Attiny,Atsam etc compilation link](https://github.com/arduino/Arduino/wiki/Unofficial-list-of-3rd-party-boards-support-urls)
    - [Circiut diagram](https://www.circuit-diagram.org/)
    - [How to self-learn electronics](https://news.ycombinator.com/item?id=16775744)
    - [Circuit simulation](https://hackaday.com/2019/11/30/circuit-simulation-in-python/?mc_cid=5bce5b5fcb&mc_eid=348b67951f)
    - [Ultimate Electronics tutorial](https://ultimateelectronicsbook.com/)
    - [Guerrilla electronics](http://lcamtuf.coredump.cx/electronics/)
    - [Students Guide](http://archive.fabacademy.org/2018/labs/fablabdassault/students/saba-ghole/exercise06.html)
    - [Basics of electronics](http://electronicsforum.com/)
    - [Kokopelli - CAD design](http://academy.cba.mit.edu/classes/electronics_design/pcb.cad)
    - [Multiboard](http://archive.fabacademy.org/archives/2016/fablabaachen/students/197/)
    - [Drawing pcb](http://fab.cba.mit.edu/classes/863.15/section.CBA/people/Schaad/week6-electronics-design.html)
    - [Free Simulation Tools ( Mac, Windows, Linux, Android, iOS )](https://6002x.mitx.mit.edu/wiki/view/UsefulFreeTools)

### Videos

- [Soldering Master Class](https://www.youtube.com/watch?v=vIT4ra6Mo0s)
- [Big Clive](https://www.youtube.com/user/bigclivedotcom)
- [Julian Illet](https://www.youtube.com/user/julius256)
- [Mike Electric Stuff](https://www.youtube.com/user/mikeselectricstuff)

### Books

- [“Make: Electronics Learning by Discovery Charles Platt” (e-book)](https://www.amazon.es/Make-Electronics-Learning-Through-Discovery/dp/0596153740)
- [“Make: Encyclopedia of Electronic Components Volume 1: Resistors, Capacitors, Inductors, Switches, Encoders, Relays, Transistors” (pdf)](https://www.amazon.es/Make-Encyclopedia-Electronic-Components-Transistors/dp/1449333893)

### Extra stuff

**Components**

- [Pull up](https://learn.sparkfun.com/tutorials/pull-up-resistors)
- [Resistor SMD code](http://www.resistorguide.com/resistor-smd-code/)

**How current flows in a circuit**

- [What is electric current](https://youtu.be/kYwNj9uauJ4)
- [Voltage, Current, Resistance, and Ohm's Law](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law)
- [Conventional Versus Electron Flow](https://www.allaboutcircuits.com/textbook/direct-current/chpt-1/conventional-versus-electron-flow/)
- [Electric current](https://en.wikipedia.org/wiki/Electric_current)

**Simulation:**

- [Falstad](http://www.falstad.com/circuit/)
- [Partsim](https://www.partsim.com/simulator)

**Fab Academy students documentation:**

- [Jundy Wang](http://archive.fabacademy.org/archives/2016/fablabshangai/students/80/week6.html)
- [Steven Chew](http://archive.fabacademy.org/archives/2016/fablabsingapore/students/98/exercise06.html)
- [Gérard Bandini](http://archive.fabacademy.org/archives/2016/fablabajaccio/students/277/week06.html)
- [Vincent Dupuis](http://archive.fabacademy.org/archives/2016/fablabdigiscope/students/456/W6_electronics_design.html)

## Tutorials

- [Schematic + Layout (old version Eagle but cool! ) -> Step by Step tutorial](https://www.youtube.com/watch?v=1AXwjZoyNno&list=PL868B73617C6F6FAD)
- [AUTODESK Guided Tour (17 videos about 4 min/video new version): Schematic, PCB, Checking the components, how to import a library…](https://www.youtube.com/watch?v=W0WyNDV_AI0&index=4&list=PL1rOC5j_Fyi4tnJRfeiUmMF3xEyFbSdIk)
- [Assignment Tutorial ( step by step ):](http://archive.fabacademy.org/archives/2017/doc/electronics_design_eagle.html)
- [SPARKFUN ( Schematic ):](https://learn.sparkfun.com/tutorials/using-eagle-schematic)
- [SPARKFUN ( Board Layout ):](https://learn.sparkfun.com/tutorials/using-eagle-board-layout)
- [Making Custom Outlines to the PCB](http://archive.fabacademy.org/archives/2017/doc/custom-outlines.html)
- [What resistor should I use with my LED?](http://led.linear1.org/1led.wiz?VS=5;VF=1.8;ID=40)
- [123Circuit](http://archive.fabacademy.org/archives/2016/fablabaalto/students/294/articles/electronics-design/)
