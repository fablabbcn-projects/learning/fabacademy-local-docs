# Computer-Controlled Machining

## Classes

### Global Class

- [Computer-Controlled Machining](http://academy.cba.mit.edu/classes/computer_machining/index.html)

!!! Note "Assignments and assessment"
    **Fabacademy students**: visit the [assessment](https://fabacademy.org/2021/docs/fabacademy-assessment/embedded_programming.html)


### Local Class


<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRURGjB4KNTaGVKJgDuSrJV3TY7X3qOM5SKyje2p6GRyGqEKKjPnGxfGtCZ0e2AYZgWLLSlezbqMtrG/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTvm4WeteM3076ChbUENNgZ0lO_1E7RBIqaQEMO5tT-L1dHJbdTbq1QEscxwAi3wGSpBFEUT92VmTFP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTxKQgpGwZ0RX5hGdTm5AlIhQrWmRZhJI1Hp0KILawDjGi9CNScyW9f6ubn_leDhVtSHlIr3PmvRvKD/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT8nKZkfoTXtP4CQZjkJWBhSngq2gyNUHNjkO5nvyNDhBNp7h1_BYN_iIoktfa_sg25g6NNd884PaQY/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

#### Process

  1. Measure material
  2. Design something BIG
  3. CAM -> Gcode generation
  4. Gcode is checked by someone
  5. Design milled
  6. Design assembled
  7. Design finished

**Love Letter to Plywood**

<iframe src="https://player.vimeo.com/video/44947985?title=0&byline=0&portrait=0&badge=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

### 1961 introduction to CNC for Boeing engineers.
![](extras/week07/assets/intro0.png)
![](extras/week07/assets/intro1.png)
![](extras/week07/assets/intro2.png)
![](extras/week07/assets/intro3.png)

**RHINOCAM VIDEO TUTORIAL**

<iframe width="560" height="315" src="https://www.youtube.com/embed/bNNX4G2K0Ts" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If you want to use the big CNC RAPTOR XLS milling machine of Pujades 102 you need to install the postprocessor for rhinocam. You can find it [here](https://github.com/EDUARDOCHAMORRO/CNC-StepPostprocessor)

## References and resources

### CNC concepts
- [Guerrilla Guide to CNC machining](http://lcamtuf.coredump.cx/gcnc/)
- [Opendesk](https://drive.google.com/open?id=0B3wpVpnNyxI0ZmdLUDRIYk9mNk02UUVTUDA0R2tLSGFBVXJz)

#### Rhino

- [Rhinocam](https://mecsoft.com/rhinocam-mill/)
- [Rhinocam Getting Started Guide (PDF)](https://drive.google.com/open?id=0B3wpVpnNyxI0SGJ0RlBzNl9fQ3c)
- [Rhinocam Tutorial ( PDF ) ( Basics )](https://drive.google.com/file/d/0B3wpVpnNyxI0Z3VmOWFZUkt6a0k/view)
- [Rhinocam Tutorial ( PDF ) ( Software Guide and a list of tutorials)](https://drive.google.com/file/d/0B3wpVpnNyxI0UDZmX29YblQ4VXM/view)

**RhinoCAM EXAMPLE WORKFLOW**

```
RhinoCam

1.Select Machine - 3 axis CNC_STEP_BCN


2.Box Stock:

    Stock Geometry: origin in the top left buttom corner
    Corner coordinates: 0, 0, 0
    Dimensions: L, W, H

3.Screw material and then around the area you want to cut

4.Draw points in Rhino where you have located your screws

5.Go to Machining operations - 2 axis
    Roughing
    Pocketing (emptying part of the material)
    Engraving (on cut) (choose it for making the "screws" spot and then select the points where you want to make the mark

6.Create/Select Tool

7.Fablab just have bold and flat mills.

    For the screws choose flat (next parameters should be according to the one you are going to use)

    Name it (this is an example of mill bit values, you have to choose your own)
    Holder Dia: 30
    Holder Len: 45
    Shank Dia: 6
    Tool Len: 105
    Shoulder Len: 60
    Flute Len: 45
    Tool Dia: 6
    Flute: 1
    Spindle Parameters: Speed: 18000 RPM, Direction: CW, Cut (calculate with cheapload equation): 4500, Retract: same as Cut, Departure: same as Cut. Plunge ("half of cut"): 2000, Approach: same as plunge and Engage: same as plunge.
        Clearance Plane: Clearance Plane Definition: Part Max Z + Dist: 20 (Never Automatic)
        Cut Parameters: we only care about "Cut Depth Control": 3mm
        Entry/Exit:
    Sorting: Minimum Distance Sort


    For pocketing:

    Feeds and Speeds: 18000 RPM
    Clearance Plane: Clearance Plane Definition: Part Max Z + Dist: 20 (Never Automatic)
    Global Parameters: Climb
    Cut Patter: Offset
    Do everything else!!!!!


For profiling/outside: use along path! when using profiling the machine doesn't recognize other pieces. So we'd like to use along path so it can slide and approach directly to the selected piece. Remember to add taps (bridges).


```

**RhinoCAM MILL Quick Start**

<iframe width="560" height="315" src="https://www.youtube.com/embed/LQdsXYhWWDk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Fusion 360
- [Prepping your model for milling](https://medium.com/@arstein/prepping-your-model-for-milling-9e6271acfa3c)

**Bookshelf Tutorial**

<iframe width="560" height="315" src="https://www.youtube.com/embed/VZU_Jpyyc5M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Joinery

- [50 digital wood joints]()
- [dukta](http://dukta.com/)

### G-Code Viewer

- [webgcode](https://nraynaud.github.io/webgcode/)
- [chilipeppr](http://chilipeppr.com/)

## References

- [Beautiful](http://www.playwood.it/gallery/)
- [Furniture](https://www.opendesk.cc/)
- [Houses](https://wikihouse.cc/)
- [Design](http://www.mediodesign.com/)

### Alumnni references from past cycles

- [Student 1](http://archive.fabacademy.org/archives/2016/fablabbcn2016/students/19/Week7.html)
- [Student 2](http://fab.cba.mit.edu/classes/863.14/people/morgen_sullivan/week_4.html)
- [Student 3](http://www.dyvikdesign.com/site/portfolio-jens/products/the-layer-chair)
- [Student 4](http://fabacademy.org/archives/2013/students/bilgoray.shmulik/localhost/wordpress/archives/469.html)
- [Student 5](http://fabacademy.org/archives/2015/eu/students/monaco.lina/08_cncbig.html)
- [Student 6](http://archive.fabacademy.org/archives/2017/fablabbcn/students/29/week07.html)
