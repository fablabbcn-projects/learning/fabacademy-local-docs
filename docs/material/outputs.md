# Output Devices - FAB LAB BCN classes

- [Output Devices](http://academy.cba.mit.edu/classes/output_devices/index.html)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQD6cyX90tAkrs0hTUGcUjfwiDB4vqMXyoitgUYoCaIaevyCQ473i8TJTogLbFRuwYp_2CLPSJBLm9_/embed?start=false&loop=false&delayms=10000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

!!! Note "Assignments and assessment"

    **Fabacademy students**: visit the [assessment](https://fabacademy.org/2020/docs/fabacademy-assessment/output_devices.html)

    **MDEF students**: visit your [assessment guide](../../mdef/#weekly-assignments)

### Output Device & Debugging

- [Output Device Class](../extras/ouputs/outputdevices)

### DIY Outputs

 - [DIY Output Class](../extras/ouputs/diyOutputs)

### Motors-batteries-wires-connectors

**DC**
![](https://i.ytimg.com/vi/e1vxxgKHbpA/maxresdefault.jpg)
![](https://pbs.twimg.com/media/DbkXRlOVAAENkOX.jpg)

**BRUSHLESS**
![](https://www.rcgroups.com/forums/showatt.php?attachmentid=9362386)
![](https://www.thanksbuyer.com/image/data/deacriptionpic/201510/42748-2.jpg)
![](https://www.himodel.com/img_sub/rc_img/15/01/10959-te2.jpg)

**ESC**
![](https://fpv.tv/wp-content/uploads/2015/09/what-is-esc.jpg)
![](https://fpvmax.com/wp-content/uploads/2016/12/ESC_Diagram.png)

**BATTERIES-POWER**
![](https://image.dfrobot.com/image//data/DFR0579/DFR0579-Power%20Firebeetle%20Series%20with%20Small%20Solar%20Panel.jpg)
![](https://cdn.arstechnica.net/wp-content/uploads/2017/11/2oa2QHEoq4Ke16su-800x600.jpg)
![](https://ae01.alicdn.com/kf/HTB1DA7sBOCYBuNkHFCcq6AHtVXa3/6v-1800mah-NI-MH-M-Battery-for-RC-toys-Cars-Boats-ship-robot-Upgrade-high-capacity.jpg)
![](https://components101.com/articles/different-types-of-batteries-and-their-uses)
![](https://components101.com/sites/default/files/components/Different-Types-of-Batteries.jpg)
![](https://www.researchgate.net/profile/Xuan_Truong_Nguyen2/publication/327237748/figure/fig1/AS:664135549321219@1535353662880/Comparison-of-Energy-Density-in-Battery-Cells-NASA-National-Aeronautics-and-Space.png)
![](https://im.rediff.com/gadget/2015/may/cell-type-comparison.png)
![](http://eduardochamorro.github.io/beansreels/workshops/beandrone/2872-lipo-exploded-view_m.jpg)
![](http://eduardochamorro.github.io/beansreels/workshops/beandrone/lipo_Batt_foilpouch_300pics.jpg)
![](https://www.myfixguide.com/manual/wp-content/uploads/2015/11/Xiaomi-Mi-Power-Bank-Teardown-4.jpg)
![](https://images-na.ssl-images-amazon.com/images/I/6153y5deqxL._AC_SX425_.jpg)

**CONNECTORS**
![](https://s3-ap-southeast-1.amazonaws.com/a2.datacaciques.com/wm/MTU5MzA/2742501996/1727204719.jpg)
![](https://www.wirebarn.com/Wire-Calculator-_ep_41.html)
![](https://blog.banggood.com/types-of-multicopter-lipo-power-cable-connectors-28526.html)
![](https://leap.tardate.com/electronics101/connectors/power/)
![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjkV7tpWNgZlqc4nzVsYKdBNOYugiFFfjO1ikPRdzbQYgfoj5nkg&s)

**DC ENCODERS**
![](https://i.stack.imgur.com/sxUXp.png)
![](https://www.nidec.com/-/media/www-nidec-com//technology/capability/brushless/img/img_brushless_02en)
![](https://www.allaboutcircuits.com/uploads/projects/Magnetic_Encoder_in_motor_used_in_this_tutorial.png)
![](https://naylampmechatronics.com/2307-thickbox_default/motor-dc-con-encoder-y-caja-reductora-25ga-12v-350rpm.jpg)
![](https://www.youtube.com/watch?v=TXC4NchfqaA)
![](https://robokits.co.in/bmz_cache/b/b39c504fe8dff081bcb41498afe4def9.image.1066x800.jpg)

**PID**
![](https://www.youtube.com/watch?v=fusr9eTceEo)
![](https://www.youtube.com/watch?v=K7FQSS_iAw0)
![](https://hackster.imgix.net/uploads/attachments/381900/capture_cRCm0OxSeq.JPG?auto=compress&w=900&h=675&fit=min&fm=jpg)

**SERVOMOTORS INDUSTRIAL**
![](https://d2nlp3g45tnwoj.cloudfront.net/staticpic/servomotor/zt0510box-05.jpg)
![](https://howtomechatronics.com/wp-content/uploads/2018/03/Servo-Motor-Closed-Loop-System.jpg)

## References and resources

- [H-bridge](https://en.wikipedia.org/wiki/H_bridge)
- [Unipolar and bipolar](https://learn.adafruit.com/all-about-stepper-motors/types-of-steppers)
- [Rotary_encoder](https://en.wikipedia.org/wiki/Rotary_encoder)
- [How Rotary Encoder Works](https://howtomechatronics.com/tutorials/arduino/rotary-encoder-works-use-arduino/)
- [Optical Encoder Project](http://groups.csail.mit.edu/mac/users/pmitros/encoder/)
- [How Rotary Encoder Works](https://howtomechatronics.com/tutorials/arduino/rotary-encoder-works-use-arduino/)
- [DC Motors: The Basics](https://itp.nyu.edu/physcomp/lessons/dc-motors/dc-motors-the-basics/)
- [Pulse Width Modulation](https://learn.sparkfun.com/tutorials/pulse-width-modulation)

#### How to Use an Oscilloscope

<iframe width="560" height="315" src="https://www.youtube.com/embed/u4zyptPLlJI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

- [How to Use an Oscilloscope](https://learn.sparkfun.com/tutorials/how-to-use-an-oscilloscope)
