#15. Molding and Casting

### Class

- [Molding and Casting](http://academy.cba.mit.edu/classes/molding_casting/index.html)


### FAB LAB BCN  MOLDING & CASTING


<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT1OfpAcEDu6hlIjD1_8UNGdJQTCYZUsjV4Eq-ZrT71Zkk3rI7I8_JHf95cxu_EzyFMjPptslJWanaz/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRBtg8Z5cE2ONJc4RZkqz1Rz3gPSfFU74f2YArI8Tcbi0DaQzcEV0_YgSb766dydL6FsVZ18Vv3QVZo/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

- [3D Demo files](extras/week10/assets/MoldingAndCastingFileClass.rar)

### Procees

![](extras/week10/assets/week10-ced79873.jpg)
![](extras/week10/assets/week10-38dc8d86.jpg)

----


## References and resources

- [Mold making with FUSION 360](https://fabacademy.org/2019/labs/sorbonne/students/hanneuse-luc/assignments/week10/)
- [Introduction to Molding and Casting](https://www.youtube.com/watch?v=5FQC7IepTTk) - A lecture about Molding and Casting from Opendot/WeMake in Milan
- [Molding and Casting Guide and Tips](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week12_molding_and_casting/molding_fabmodules.html)
- [Guerrilla Guide](http://lcamtuf.coredump.cx/gcnc/)
- [Mastering CAD and CAM](http://lcamtuf.coredump.cx/gcnc/ch3/)
- [Resin Casting](http://lcamtuf.coredump.cx/gcnc/ch4/)

### Food
- [http://formx.es/products/siliconas/smooth-sil-series/index.php](http://formx.es/products/siliconas/smooth-sil-series/index.php)
- [DANGEROUS POPSICLES](https://vimeo.com/108829150)

<iframe src="https://player.vimeo.com/video/108829150" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/108829150">Dangerous Popsicles</a> from <a href="https://vimeo.com/charlienordstrom">Charlie Nordstrom</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

- [NOISY JELLY](https://vimeo.com/38796545)

 <iframe src="https://player.vimeo.com/video/38796545" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/38796545">NOISY JELLY</a> from <a href="https://vimeo.com/user3131794">Rapha&euml;l Pluvinage</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

- [Chocolates with food safe silicon](http://archive.fabacademy.org/fabacademy2017/fablabseoul/students/217/wk12.html)


### SCAN

- [http://fabacademy.org/archives/2015/eu/students/ferreira.alessandra/html/week09.html](http://fabacademy.org/archives/2015/eu/students/ferreira.alessandra/html/week09.html)
![](http://fabacademy.org/archives/2015/eu/students/ferreira.alessandra/images_template1/modelaplayer01.jpg)


### METAL
- [http://fabacademy.org/archives/2014/students/begle.moritz/week10.html](http://fabacademy.org/archives/2014/students/begle.moritz/week10.html)

 ![](http://fabacademy.org/archives/2014/students/begle.moritz/images/mold.jpg)

- [http://archive.fabacademy.org/archives/2017/fablabbcn/students/108/week12.html](http://archive.fabacademy.org/archives/2017/fablabbcn/students/108/week12.html)

![](http://archive.fabacademy.org/archives/2017/fablabbcn/students/108/images/Image-1%20(1).jpg)

- [http://fab.cba.mit.edu/classes/863.14/tutorials/MoldMaking/MoldingCasting.html](http://fab.cba.mit.edu/classes/863.14/tutorials/MoldMaking/MoldingCasting.html)

### TWO SIDE MOLDS

- [Really good student example](http://archive.fabacademy.org/2017/fablabverket/students/100/web/assignments/week12/index.html)
![](http://archive.fabacademy.org/2017/fablabverket/students/100/web/assignments/week12/images/machine/air.jpg)

- [Chamfers something to care about](http://fab.academany.org/2018/labs/fablabbrighton/students/andrew-sleigh/assignments/2018/03/23/wk10-molding-and-casting.html)
 ![](http://fab.academany.org/2018/labs/fablabbrighton/students/andrew-sleigh/assets/images/SRP4.jpg)

### SIX SIDE MOLDS

https://hackmd.io/pk_UkEsJRSCdoNwy3fNOKQ?both

- Student reference [https://hackmd.io/pk_UkEsJRSCdoNwy3fNOKQ?both](https://hackmd.io/pk_UkEsJRSCdoNwy3fNOKQ?both)

### Students References

  1. Silli Saverio [http://www.fabacademy.org/archives/2015/eu/students/silli.saverio/exercise09.html](http://www.fabacademy.org/archives/2015/eu/students/silli.saverio/exercise09.html)
  2. Ferdinand Meier [http://fabacademy.org/archives/2013/students/meier.ferdinand/molding\_casting.html](http://fabacademy.org/archives/2013/students/meier.ferdinand/molding_casting.html)
  3. Massimo Menichinelli [http://fabacademy.org/archives/2012/students/menichinelli.massimo/index.html#](http://fabacademy.org/archives/2012/students/menichinelli.massimo/index.html#)
  4. Santi Fuentemilla [http://fabacademy.org/archives/2012/students/fuentemilla.santiago/week7.html](http://fabacademy.org/archives/2012/students/fuentemilla.santiago/week7.html)
  5. Tomas Martin Agud [http://archive.fabacademy.org/archives/2017/fablabbcn/students/29/week12.html](http://archive.fabacademy.org/archives/2017/fablabbcn/students/29/week12.html)
  6. [http://archive.fabacademy.org/archives/2017/fablabbcn/students/51/w12.html](http://archive.fabacademy.org/archives/2017/fablabbcn/students/51/w12.html)


### Shops

  1. The shop is very close to fablab:
    1. [https://www.google.es/maps/place/Form+X/@41.398701,2.199695,17z/data=!3m1!4b1!4m2!3m1!1s0x12a4a315c4aae101:0xe94fa13c43e9734c](https://www.google.es/maps/place/Form+X/@41.398701,2.199695,17z/data=!3m1!4b1!4m2!3m1!1s0x12a4a315c4aae101:0xe94fa13c43e9734c)


**Vacuum molding machine**

- Mayku:[https://www.youtube.com/watch?v=wnlskvJJRdc](https://www.youtube.com/watch?v=wnlskvJJRdc)
- [http://archive.fabacademy.org/2017/fablabfct/students/387/final.html](http://archive.fabacademy.org/2017/fablabfct/students/387/final.html)

8. Rotomolding machine

- [http://fabacademy.org/archives/2015/eu/students/silli.saverio/project07.html](http://fabacademy.org/archives/2015/eu/students/silli.saverio/project07.html)
