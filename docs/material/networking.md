#14. Networking and Communications

<p><a href="https://commons.wikimedia.org/wiki/File:Internet_map_1024.jpg#/media/File:Internet_map_1024.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Internet_map_1024.jpg/1200px-Internet_map_1024.jpg" alt="Internet_map_1024.jpg"></a><br>By <a href="//commons.wikimedia.org/w/index.php?title=Barrett_Lyon&amp;action=edit&amp;redlink=1" class="new" title="Barrett Lyon (page does not exist)"> The Opte Project</a> - Originally from the <a href="https://en.wikipedia.org/wiki/Main_Page" class="extiw" title="en:Main Page">English Wikipedia</a>; description page is/was <a href="https://en.wikipedia.org/wiki/en:Image:Internet_map_1024.jpg" class="extiw" title="w:en:Image:Internet map 1024.jpg">here</a>., <a href="https://creativecommons.org/licenses/by/2.5" title="Creative Commons Attribution 2.5">CC BY 2.5</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=1538544">Link</a></p>


## FABLAB BCN classes

### Wired and Wireless Networking

- [Introduction](../extras/networking/local)
- Networking Classes: [wired](../extras/networking/wired) and [wireless](../extras/networking/wireless)
- [Networking Examples ](../extras/networking/examples)

### MQTT

 - [MQTT Network Class](../extras/networking/mqtt)
 - [MQTT in ESP32 Feather](https://hackmd.io/@fablabbcn/rydUz5cqv)

### What we learnt from onions

 - [OSI Model and advanced networking](../extras/networking/onions)

## References and resources

- [A list of interesting links](https://docs.google.com/document/d/1O2r1D6gphP14gYeQeZh1JOS3mA_JXKZFv-saHMyJR60/edit?usp=sharing)
- [How to choose a Wifi Module ( spanish )](https://polaridad.es/esp8266-modulo-wifi-elegir-caracteristicas/)
- [Chip MRF89XAM8A](http://ww1.microchip.com/downloads/en/DeviceDoc/70651A.pdf)
- [Bluetooth](http://archive.fabacademy.org/archives/2016/fablabaachen/students/197/projects/week06.html)

### Raspberry pi tutorials

- [Pinout 1](https://pinout.xyz/#)
- [Pinout 2](https://elinux.org/RPi_Low-level_peripherals)
- [UART on a Pi](https://www.raspberrypi.org/documentation/configuration/uart.md)
- [Using UART in Raspberry Pi](https://www.electronicwings.com/raspberry-pi/raspberry-pi-uart-communication-using-python-and-c)
- [NRF24L01 on a Pi directly via SPI](https://www.raspberrypi.org/forums/viewtopic.php?p=908161)
- [Another one](https://www.riyas.org/2014/08/raspberry-pi-as-nrf24l01-base-station-internet-connected-wireless.html)
