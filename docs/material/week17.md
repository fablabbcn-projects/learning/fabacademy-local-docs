# Mechanical Design, Machine Design

## Classes

### Global Class

- [Mechanical Design](http://academy.cba.mit.edu/classes/mechanical_design/index.html)
- [Machine Design](http://academy.cba.mit.edu/classes/machine_design/index.html)

!!! Note "Assignments and assessment"
    **Fabacademy students**: visit the [assessment](https://fabacademy.org/2021/docs/fabacademy-assessment/principles_and_practices.html)

    On the group page (linked on your Lab page), has your group:

    - Shown how your team planned and executed the project
    - Described problems and how the team solved them
    - Listed future development opportunities for this project
    - Included your design files, 1 min video (1920x1080 HTML5 MP4) + slide (1920x1080 PNG)

### Local Class

#### Mechanics

- [Mechanics](extras/week17/mechanics.md)

#### G-Code

- [G-Code](../clubs/makeclub/gcode.md)
- [The CNC by the sea](https://cncbythesea.fablabbcn.org)
- [The CNC by the sea - reference](https://gitlab.com/fablabbcn-projects/learning/the-cnc-by-the-sea/-/tree/master)

## References and resources

- [A list of interesting links](https://docs.google.com/document/d/11rGmQPq11U0WjCxnEsi2rzbD6SGEw0HB8dT-C7DynbE/edit?usp=sharing)

**CNC Hardware:**

- [Arduino CNC Shield](https://blog.protoneer.co.nz/arduino-cnc-shield/)
![](extras/week14/assets/week14-cf1fa507.jpeg)
- [Grbl](https://github.com/gnea/grbl/wiki/Connecting-Grbl)
- [chilipeppr](http://chilipeppr.com/grbl)
- [MTM Stages](http://mtm.cba.mit.edu/machines/science/)
![](extras/week14/assets/week14-6cd83f5c.jpg)
- [Getting Started With Gestalt Nodes](http://fabacademy.org/archives/2015/doc/MachineMakingNotes.html)

**Thingiverse:**

- [Gear Generator](http://geargenerator.com/#200,200,100,6,1,0,0,4,1,8,2,4,27,-90,0,0,16,4,4,27,-60,1,1,12,1,12,20,-60,2,0,60,5,12,20,0,0,0,2,-563)
- [Involute Spur Gear Builder](http://hessmer.org/gears/InvoluteSpurGearBuilder.html)
- [Parametric pulley](https://www.thingiverse.com/thing:16627)
- [Plate Bracket for Aluminium Extrusion Profiles](https://www.thingiverse.com/thing:2503622)

### Barcelona Machine Groups

- [link](https://docs.google.com/spreadsheets/d/1l7f0c6apN5XFGc4O367qckOscBvvVF9OLojs-MU8_LQ/edit#gid=316828658)
