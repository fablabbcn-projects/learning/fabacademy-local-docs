# Embedded Programming

![](https://3.bp.blogspot.com/-LGclV0RtEIY/U5pYy5uY2GI/AAAAAAAAAXE/mJQWV4NXfq8/s1600/bg-tetris-5.png)

## Classes

### Local Class

#### Introduction to embedded programming

- [Introduction to embedded programming](../extras/week08/introduction)

#### Unofficial list of 3rd party boards support urls

- [All the libraries of Attiny,Atsam etc compilation link](https://github.com/arduino/Arduino/wiki/Unofficial-list-of-3rd-party-boards-support-urls)

### Embedded programming Extended class ATSAMD11C

- [Unpacking the ATSAMD11C](https://hackmd.io/XHrDcQuYRJW2vC2WD02ftg?view)

<iframe src="https://player.vimeo.com/video/690020467?h=f6939e4eb7" width="640" height="420" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/690020467">Global Open Time 2022-03-19 Embedded Programming</a> from <a href="https://vimeo.com/academany">Academany</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

#### Advanced embedded programming

- [Advanced embedded programming](../extras/week08/embeddedprogramming)

#### Platformio

- [Plaftormio](../extras/week08/platformio)

## References and resources

- [A list of interesting links](https://docs.google.com/document/d/1AjxysgPJQ1X0N3smNhSx6yKmGEc6trmumc8wqB0zpsU/edit?usp=sharing)
* [Information Theory](https://www.youtube.com/watch?v=p0ASFxKS9sg&list=PLP6PHJ8SLR6D4ytpHhZBdylPNcazU5m7o)
* [More on information Theory and Coding (for those who enjoyed the videos above)](https://www.youtube.com/playlist?list=PLzH6n4zXuckpKAj1_88VS-8Z6yn9zX_P6)
* [How computers think](https://www.youtube.com/watch?v=dNRDvLACg5Q)
* [Why we use binary](https://www.youtube.com/watch?v=thrx3SBEpL8)
* [How computers work](https://www.youtube.com/watch?v=nN9wNvEnn-Q)
* [How computer memory works the insides](https://www.youtube.com/watch?v=XETZoRYdtkw&t=6s)
* [Colossus the first electronic computer](https://www.youtube.com/watch?v=knXWMjIA59c)
* [How they design the computers they landed astronauts to the moon](https://www.youtube.com/watch?v=xQ1O0XR_cA0)
* [UNIX or what an operative system does](https://www.youtube.com/watch?v=tc4ROCJYbm0)
* [Looking at how things are built and work to learn how to design](https://www.youtube.com/playlist?list=PLvOlSehNtuHsy89OdSxBajult8e5srVLA)
* [An introduction to Arduino towards future assignments](https://www.youtube.com/watch?v=_h1m6R9YW8c)
* [ABC of connexions - request access!](https://drive.google.com/file/d/18QY3bjonCGwGFEkA5xm32AY-xARujYnQ/view)

### Other approaches

- [Alhambra FPGA](https://alhambrabits.com/alhambra/)
- [FPGAs](https://github.com/Obijuan/digital-electronics-with-open-FPGAs-tutorial/wiki)
- [FPGA biorobots](https://github.com/jcarolinares/fpga-biorobots)

### Other frameworks

- [ESP-IDF](https://github.com/espressif/esp-idf)

### Other languages

- [Micropython](https://micropython.org/)
- [Docs micropython](https://docs.micropython.org/en/latest/)

### Other years

- [A mini arduino with a ATTINY44-STEP1](http://archive.fabacademy.org/archives/2017/fablabbcn/students/271/project/week-06.html)
- [A mini arduino with a ATTINY44-STEP2](http://archive.fabacademy.org/archives/2017/fablabbcn/students/271/project/week-08.html)
- [A mini arduino with a ATTINY44-STEP3](http://archive.fabacademy.org/archives/2017/fablabbcn/students/271/project/week-13.html)
