#10. Applications and Implications

### Class Week 10

- [Applications and Implications week](http://academy.cba.mit.edu/classes/applications_implications/index.html)

**Lesson12 applications and implications**

<iframe src="https://player.vimeo.com/video/329709689" width="640" height="336" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

**Review11: input devices**

<iframe src="https://vimeo.com/400735149" width="640" height="336" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

**Recitation06: programs**

<iframe src="https://player.vimeo.com/video/329160398" width="640" height="240" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

---
!!! Note "Assignments and assessment"

    **Fabacademy students**: visit the [assessment](https://fabacademy.org/2020/docs/fabacademy-assessment/input_devices.html)

    **MDEF students**: visit your [assessment guide](../../mdef/#weekly-assignments)
-----
