# Processing

This is a simple tutorial where we will introduce Processing and the basic features that it has. 

## Introduction

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQEEVBB1UKYvKD2PevGfnYRs-_dGXD2YrcGY21J9DpVEiG-fZyjzY8Ht7KUVZMPM9Fpc_jCfdgD4T9t/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Processing vs P5js

| Processing          | Features               |P5js|
| ----------------- |:----------------------- |:-----------------------
|Java       | Language   |JavaScript
| Processing Libraries | Libraries     | JavaScript Libraries
| Desktop         | Application type     |Web
| Easy        | Connectivity with hardware    | Difficult
| [Processing](https://www.cs.bham.ac.uk/~cxp291/ri/processing_cheat_sheet_english.pdf)   | CheatSheat |[P5js](https://bmoren.github.io/p5js-cheat-sheet/)

## Basics:

### Bouncing Ball

This example covers the basics of Processing, how to create and use the software to create a basic interface. 
![](https://i.imgur.com/Riq9Ecf.gif)

~~~ java

  float locationY = 450; 
  float speedY = 0;     
  float gravity = 0.2;  

  void setup() {
    size(1600,900);
  }

  void draw() {
    background(255,204,0);
    fill(0,204,255);
    noStroke();
    translate(800,locationY);   
    ellipse(0,0,100,100); 
    locationY += speedY;
    speedY += gravity;
    if ((locationY >= height)) { 
      speedY = speedY * -0.75;
      locationY = height;
    }
  }
~~~

### Drawing with the mouse

Creating an interactive Interface using the coordinates of the mouse.

![](https://i.imgur.com/kdE9faG.gif)

``` java
  int mouseclick = 0;

  void setup() {
    size(1600,900);
    background(0);
  }

  void draw() {
    if (!mousePressed) {
      fill(0,20);
      rect(0,0,width,height);
      mouseclick = 0;
    }
    if (mousePressed && mouseclick == 0) {
      mouseclick = 1;
      background(0);
    }
    fill(255,255,255);
    ellipse(mouseX,mouseY,100,100);
  }
```

## Connecting with Arduino

<iframe src="https://player.vimeo.com/video/342766631" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<a href="http://fabacademy.org/2019/labs/barcelona/students/josep-marti/Week15.html">Josep's Interface documentation</a> 

### Serial communication

The Arduino board (and other boards like the Barduino, NodeMCU, ATtiny...) can talk to the computer through the USB using the Serial port. This communication take place bit by bit, so it's important to try to optimize it in a way that makes it easy for the computer, to increase the speed and accuracy.

#### Serial.print() vs Serial.write()

Both functions send information through the Serial port, but they work in a different way. To decide wich one to use, it's important to know wich information we want to send. Serial.print() will send information as *char* translating your ASCII code to binary, while Serial.write(), send bytes one by one. Serial.write() is more simple and fast.

```
  Serial.write(0x48);   // H
  Serial.write(0x45);   // E
  Serial.write(0x4C);   // L
  Serial.write(0x4C);   // L
  Serial.write(0x4F);   // O
```
![](https://i.stack.imgur.com/nVEeo.jpg)
```
  Serial.print("HELLO");
```
![](https://i.stack.imgur.com/nVEeo.jpg)

### LDR interface

Draw the reading of the LDR

![](https://i.imgur.com/FsHH5C9.png)

Components:

* Arduino UNO
* LDR
* 1KΩ Resistor

![](https://i.imgur.com/YuZO3d6.png)

Arduino code:

```java
  void setup() {
    pinMode(A0,INPUT);
    Serial.begin(9600);
  }

  void loop() {
    int lecture = analogRead(A0);
    Serial.println(lecture);
    delay(100);
  }
```

Processing Code:

```java
  import processing.serial.*;
  String lecture = "0";
  int val = 0;

  Serial myPort;

  void setup() {
    size(1600, 900);
    myPort = new Serial(this, "COM17", 9600);
  }

  void draw() {
    if (myPort.available() > 0) {
      lecture = myPort.readStringUntil('\n');
      println(lecture);
      if (lecture != null) {
        lecture = trim(lecture);
        val = Integer.parseInt(lecture);
      }
    }
    background(20);
    fill(255,204,0);
    rect(100,350,val,200);
    fill(255);
    textSize(40);
    text(val, 100+(val/2), 470);
  }
```

### LDR Pro interface

```java=
  import processing.serial.*;
  String lecture = "0";
  int val = 0;
  float valmap = 0;

  Serial myPort;

  void setup() {
    size(1600,900,P3D);
    myPort = new Serial(this, "COM5", 9600);
  }

  void draw() {
    if (myPort.available() > 0) {
      lecture = myPort.readStringUntil('\n');
      if (lecture != null) {
        lecture = trim(lecture);
        val = Integer.parseInt(lecture);
        println(val);
      }
    }
    background(0);
    noStroke();
    valmap = map(float(val),0.0,1023.0,0.0,255.0);
    directionalLight(valmap,valmap,valmap,1,0,-valmap/255);
    translate(width/2, height/2);
    fill(255,204,0);
    sphere(200);
  }
```

### Interfacing with 3 sensors

Use three potentiometers to control the backgorund color RGB values.

![](https://i.imgur.com/7ky5Ig3.png)

Components:

* Arduino UNO
* 3 Potentiometers

![](https://i.imgur.com/kF6PSq6.png)

Code: Under Arduino Examples - 04.Communication - VirtualColorMixer

Arduino:
```java
  const int redPin = A0;		// sensor to control red color
  const int greenPin = A1;	// sensor to control green color
  const int bluePin = A2;		// sensor to control blue color

  void setup() {
    Serial.begin(9600);
  }

  void loop() {
    Serial.print(analogRead(redPin));
    Serial.print(",");
    Serial.print(analogRead(greenPin));
    Serial.print(",");
    Serial.println(analogRead(bluePin));
  }
```

Processing:
```java
  import processing.serial.*;

  float redValue = 0;        // red value
  float greenValue = 0;      // green value
  float blueValue = 0;       // blue value

  Serial myPort;

  void setup() {
    size(600, 600);

    // List all the available serial ports
    // if using Processing 2.1 or later, use Serial.printArray()
    println(Serial.list());

    // I know that the first port in the serial list on my mac
    // is always my  Arduino, so I open Serial.list()[0].
    // Open whatever port is the one you're using.
    myPort = new Serial(this, Serial.list()[0], 9600);
    // don't generate a serialEvent() unless you get a newline character:
    myPort.bufferUntil('\n');
  }

  void draw() {
    // set the background color with the color values:
    background(redValue, greenValue, blueValue);
  }

  void serialEvent(Serial myPort) {
    // get the ASCII string:
    String inString = myPort.readStringUntil('\n');
    print(inString);

    if (inString != null) {
      // trim off any whitespace:
      inString = trim(inString);
      // split the string on the commas and convert the
      // resulting substrings into an integer array:
      float[] colors = float(split(inString, ","));
      // if the array has at least three elements, you know
      // you got the whole thing.  Put the numbers in the
      // color variables:
      if (colors.length >=3) {
        // map them to the range 0-255:
        redValue = map(colors[0], 0, 1023, 0, 255);
        greenValue = map(colors[1], 0, 1023, 0, 255);
        blueValue = map(colors[2], 0, 1023, 0, 255);
      }
    }
  }
```

P5js:

Check the code in P5js here: https://editor.p5js.org/Pepo41/sketches/7M6d2f660

```javascript
  let serial; // variable for the serial object
  let colors = [0,0,0];

  function setup() {
    createCanvas(windowWidth, windowHeight);
    // serial constructor
    serial = new p5.SerialPort();

    // serial port to use - you'll need to change this
    serial.open('COM5');

    // what to do when we get serial data
    serial.on('data', gotData);

  }

  // when data is received in the serial buffer

  function draw() {
    let red = int(map(colors[0],0,1023,0,255));
    let green = int(map(colors[1],0,1023,0,255));
    let blue = int(map(colors[2],0,1023,0,255));
    background(red,green, blue);
    text(str(red)+"  "+str(green)+"  "+str(blue), width/2,height/2);
  }

  function gotData() {
    let currentString = serial.readLine(); // store the data in a variable
    trim(currentString); // get rid of whitespace
    if (!currentString) return; // if there's nothing in there, ignore it
    colors = split(currentString, ',');
  }
```


### Controlling RGB interface

![](https://i.imgur.com/N27i0rg.png) 

![](https://i.imgur.com/hsvVJHI.jpg)

Components:

* Arduino UNO
* 3 LED's
* 3 Push Buttons
* 3 222 Ω Resistors

![](https://i.imgur.com/UXvqkr3.png)

Arduino code:

```java
  byte mode = 0x10000000;
  byte state = 0x00000000;
  char lecture = 0;

  void setup() {
    pinMode(3, OUTPUT);  
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT); 
    digitalWrite(3,HIGH);
    digitalWrite(4,HIGH);
    digitalWrite(5,HIGH);
    Serial.begin(9600);
  }

  void loop() {
    if (Serial.available()>0) {
      lecture = Serial.read();
      if (lecture == 16) {
        readState();
      }
      else {
        writeState();
      }
    }
  }

  void readState() {
    pinMode(3, INPUT);  
    pinMode(4, INPUT);
    pinMode(5, INPUT);
    bitWrite(state,0,!digitalRead(3));
    bitWrite(state,1,!digitalRead(4));
    bitWrite(state,2,!digitalRead(5));
    Serial.write(state);
  }

  void writeState() {
    DDRD = DDRD | B01111000;
    PORTD = B00111000;
    PORTD &= ~(lecture << 3);
  }
```

```java
  import processing.serial.*;
  int mouseR = 0;
  int mouseL = 0;
  int msg = 0;
  int readmsg = 0;
  color ModeColor = color(50);
  color ModeTextColor = color(250);
  color RColor = color(50);
  color GColor = color(50);
  color BColor = color(50);
  String ModeText = "Writing";
  int Mode = 0;
  int rB = 0;
  int gB = 0;
  int bB = 0;
  color ButtonTextColor = color(255);

  int r = 0;
  int g = 0;
  int b = 0;

  Serial myPort;

  void setup() {
    size (1600, 900);
    myPort = new Serial(this, "COM17", 9600);
  }

  void draw() {
    if ((mousePressed) && (mouseButton == LEFT) && (mouseL == 0) && overRect(650,100,300,100)) {
      mouseL = 1;
      r=0;
      g=0;
      b=0;
      if (Mode == 1) {
        Mode = 0;
        ModeText = "Writing";
        ModeColor = color(50);
        ModeTextColor = color(250);
        writingMode();
        ButtonTextColor = color(255);
      }
      else if (Mode == 0) {
        Mode = 1;
        ModeText = "Reading";
        ModeColor = color(200);
        ModeTextColor = color(50);
        ButtonTextColor = color(50);
        RColor = color(200);
        GColor = color(200);
        BColor = color(200);
      }
    }
    if (Mode == 1) {
      readingMode();
    }
    
    if ((mousePressed) && (mouseButton == LEFT) && (mouseL == 0) && overRect(425,700,200,100) && Mode == 0) {
      mouseL = 1;
      if (rB == 0){
        rB = 1;
        r = 255;
        RColor = color(150,0,0);
      }
      else if (rB == 1) {
        rB = 0;
        r = 0;
        RColor = color(50);
      }
      writingMode();
    }
    
    if ((mousePressed) && (mouseButton == LEFT) && (mouseL == 0) && overRect(700,700,200,100) && Mode == 0) {
      mouseL = 1;
      if (gB == 0){
        gB = 1;
        g = 255;
        GColor = color(0,150,0);
      }
      else if (gB == 1) {
        gB = 0;
        g = 0;
        GColor = color(50);
      }
      writingMode();
    }
    
    if ((mousePressed) && (mouseButton == LEFT) && (mouseL == 0) && overRect(975,700,200,100) && Mode == 0) {
      mouseL = 1;
      if (bB == 0){
        bB = 1;
        b = 255;
        BColor = color(0,0,150);
      }
      else if (bB == 1) {
        bB = 0;
        b = 0;
        BColor = color(50);
      }
      writingMode();
    }
    
      
    
    background(r,g,b);
    fill(ModeColor);
    rect(650,100,300,100,30);
    fill(RColor);
    rect(425,700,200,100,30);
    fill(GColor);
    rect(700,700,200,100,30);
    fill(BColor);
    rect(975,700,200,100,30);
    textAlign(CENTER);
    textSize(60);
    fill(ModeTextColor);
    text(ModeText,800,170);
    fill(ButtonTextColor);
    text("Red",525,770);
    text("Green",800,770);
    text("Blue",1075,770);
    
    if( !mousePressed && (mouseButton == LEFT) && (mouseL == 1)) {
      mouseL = 0;
    }
  }

  boolean overRect(int x, int y, int width, int height)  {
    if (mouseX >= x && mouseX <= x+width && 
        mouseY >= y && mouseY <= y+height) {
      return true;
    } else {
      return false;
    }
  }

  void writingMode() {
    msg = bB+2*gB+4*rB;
    myPort.write(msg);
  }

  void readingMode() {
    myPort.write(16);
      if (myPort.available() >0) {
        readmsg = myPort.read();
      }
      if ((readmsg & 1) > 0) {
        b = 255;
      }
      else { 
        b=0;
      }
      if ((readmsg & 2) > 0) {
        g = 255;
      }
      else { 
        g=0;
      }
      if ((readmsg & 4) > 0) {
        r = 255;
      }
      else { 
        r=0;
      }
  }
```

## OSC + Syphon

- [OSC](https://en.wikipedia.org/wiki/Open_Sound_Control): documentation [here](http://www.sojamo.de/libraries/oscP5/)
- [Syphon (only MAC)](http://syphon.v002.info/): A client [here](https://www.sigmasix.ch/syphoner/)

```java
  //OSC
  import oscP5.*;
  import netP5.*;
  OscP5 oscP5;
  NetAddress myRemoteLocation;

  //SYPHON
  import codeanticode.syphon.*;
  public PGraphics canvas;
  public SyphonServer server;

  void settings(){
    size(1280,720,P2D);
    PJOGL.profile=1;
    
    // Start OSC 
    oscP5 = new OscP5(this, 12000);
    myRemoteLocation = new NetAddress("127.0.0.1",1234);
  }

  void setup(){
    frameRate(60);
    
    //Start SYPHON
    server = new SyphonServer(this,"HAND");
  }

  void draw(){
    // Send screen to another application listenting on 127.0.0.1:1234
    server.sendScreen();

    /* create an osc bundle */
    OscBundle myBundle = new OscBundle();
    
    /* createa new osc message object */
    OscMessage myMessage = new OscMessage("category_1");
    myMessage.add(appX_rightHand);

    myMessage.setAddrPattern("patter_1");
    myMessage.add(right_hand_opacity);
    myBundle.add(myMessage);
    
    /* reset and clear the myMessage object for refill. */
    myMessage.clear();
    
    /* refill the osc message object again */
    myMessage.setAddrPattern("pattern_2");
    myMessage.add(left_hand_opacity);
    myBundle.add(myMessage);
    /* send the osc bundle, containing 2 osc messages, to a remote location. */
    
    /* either in 10s
    myBundle.setTimetag(myBundle.now() + 10000);
    /* Or inmediately */
    oscP5.send(myBundle, myRemoteLocation);
  }
```
