# Static site generators

![](https://www.thecrazyprogrammer.com/wp-content/uploads/2016/11/Difference-between-Static-and-Dynamic-Websites.png)
_source:[the crazy programmer](https://www.thecrazyprogrammer.com)_

Note:
  The main difference is, dynamic webpages are not served as-is by the web server as static pages are. They are constructed for every HTTP request sent by each client.
- Static: The content is typically stored in flat files rather than databases, they apply it against layouts or templates and generate a structure of purely static HTML files that are ready to be delivered to the users ([Apple](https://www.apple.com), [NASA](https://www.nasa.gov/))
- Dynamic: When a visitor gets to a website expecting the latest content, a server-side script  queries one or multiple databases to get the content. Then, it passes the results to a templating engine who will format and arrange everything properly and generate an HTML file for the user to consume ([Twitter](https://www.twitter.com), [Amazon](https://amazon.com), [Quora](https://www.quora.com)...)

## Website structure

### Main Skeleton

```
<html>
  <head>
    ...
  </head>
  <body>
    ...
  </body>
</html>
```

HTML documents consist of two major parts:

*  The body is where the visible content of the web page is placed. The content in the body is organized by a wide variety of semantic elements such the header, main, and footer elements.
*  The head is placed before the body and includes information about the web page and instructions for web browsers and search engine web crawlers.

### Head

```
  <head>
    <title>Your Webpage Title Goes Here</title>
    Many other busy stuff 
  </head>
```

### Body

```
  <body>
    <header>
      <nav>...</nav>
    </header>
    <main>
      <nav>...</nav>
      <article>
        <aside>...</aside>
        <section>...</section>
        <address>...</address>
      </article>
      <aside>
        <section> ... </section>
      </aside>
    </main>
    <footer>
      <address> ... </address>
    </footer>
  </body>
```

## What is a static site generator?

A static site generator (SSG) takes text written in _a_ **markup language** and it churns through layouts to create a **static website**.

> With a SSG, we write dynamically and publish statically.

!!! info
      An SSG is an intelligent counter-measure for avoiding security threats in server-side applications. It maintains the benefits of templating systems with less vulnerability threads.

### Examples

* [MKDocs](https://www.mkdocs.org/)
* [HUGO](https://gohugo.io/)
* [Jekyll](https://jekyllrb.com/)
* [DocPad](https://docpad.org/)
* [Wintersmith](http://wintersmith.io/)

## Markup/down?

Markdown is two things:

- (1) a plain text formatting syntax; and 
- (2) a software tool, written in Perl, that converts the plain text formatting to HTML.

### Markup/down References / Tutorials

* [Markup](https://en.wikipedia.org/wiki/Markup_language) is a generic term for a language that describes a document's formatting: i.e.: HTML, LaTeX, XML
* [Markdown](http://daringfireball.net/projects/markdown/) is a specific markup library

![](https://i.imgur.com/FCmsksS.png)

## Fab Academy Repository

You have a mkdocs template available:

1. Clone Repo
2. Start customising the mkdocs.yml file in the root folder
3. The first edit to the website will trigger the build process and publish the website

## MKDOCS

You need to have python installed. You can follow [this guide](https://docs.python-guide.org/starting/installation/) to install it. 

!!! danger "Python Version"
      Do not use python 2! It's no longer supported.

Once you have it, you can do the following:

```
pip install mkdocs
```

Or, if there is a `requirements.txt` file:

```
pip install -r requirements.txt
```

And then, after cloning your repo:

```
git clone <your-repo.git>
cd your-repo
mkdocs serve
```

Enjoy!

## Jekyll

`Jekyll` is a **static site generator** based on **ruby**. It can take HTML, markdown, etc. files and convert them into sites with styling, based on layouts.

### How to install it?

**Visit**:

[https://jekyllrb.com/docs/installation/#guides](https://jekyllrb.com/docs/installation/#guides)

- [Download Jekyll](https://jekyllrb.com/)
- [Jekyll Tutorial](https://youtu.be/T1itpPvFWHI)

**Digest**:

- We need `Ruby` Development Environment
- We need `Bundler` to install Gemfiles (lists of gems)
```
gem install bundler
```
- We need `jekyll` (which is actually a Ruby Gem)
```
gem install jekyll
```

### QuickStart (gitlab version)

- In your gitlab repo and add a new file:
```
Template type: .gitlab-ci.yml
Template: Jekyll
```

!!! info
      This file controls the build process for your site. Every push to the Git repository will trigger the runner and you can check its progress on the /pipelines page of your project.

- Make a `jekyll` project where you want to have your repo
```
jekyll new fabacademy-site
Running bundle install in .../fabacademy-site...
...
New jekyll site installed in .../fabacademy-site...
cd fabacademy-site
```
- Edit your jekyll `_config.yml` file:
```
baseurl: "/2019/labs/barcelona/students/john-doethemachine/" # the subpath of your site, e.g. /blog/
url: "http://archive.fabacademy.org"
```
- Add and commit changes
```
git add .
git commit -m "Start super nice jekyll project"
```
- Set your repo url to be the remote
```
git remote add origin git@gitlab.fabcloud.org:academany/fabacademy/2019/labs/barcelona/students/john-doethemachine.git
```
- Pull (the first time you need to tell it _from_ where)
```
git pull origin master
```
- Push (the first time you need to tell it _to_ where)
```
git push --set-upstream origin master
```
- Visit!

!!! info
      You can find your website in the `Settings > Pages` tab of Gitlab

### SuperQuickStart (local version)

- Find a template or use minima
- Clone it
```
git clone https://github.com/template-creator/SuperCoolTemplate
cd SuperCoolTemplate
```
- Serve it with `jekyll` (only locally)
```
jekyll serve
```
- View it in [127.0.0.1:4000](http://127.0.0.1:4000) (normally for `jekyll`)

### Understanding Jekyll

![](https://jekyllrb.com/img/jekylllayoutconcept.png)

### Example of last layer

```
<!DOCTYPE html>
<html>    
  {% include head.html %}

  <body>
    {% include header.html %}
    <br>
    <div class="page-content">
      <div class="wrapper">
        {{ content }}
      </div>
    </div>
    
    {% include footer.html %}
    
  </body>

</html>
```

### Example of intermediate layer

```
 ---
 layout: default
 ---

 <div class="post">
   <br>
   <header class="post-header">
     <h1 class="post-title">{{ page.title }}</h1>
   </header>
 
   <article class="post-content">
     {{ content }}
   </article>
 </div>
```

### The front matter

Defines the type of document, title, and generic METADATA for the page.

```
 ---
 layout: post
 title:  "Hello!"
 date:   2015-04-09 11:23:32
 categories: jekyll update
 ---
```

### The liquid syntax

Helps us place content inside the document. Normally between ```{{ curly brackets }}```

```
# Calling for content
{{ page.title }}
{{ content }}

# Calling for actions
{% include header.html %}
```

### The `_config.yml`

This file includes the configurations for our site:
- title
- baseurl
- url
- content structure
- plugings

!!! warning "SUPER IMPORTANT"
        It's a yml file. **Indentation** is key in yml!

#### Metadata

```
# Site settings
title: Your awesome title
motto: Your motto
email: your-email@domain.com
description: > # this means to ignore newlines until "baseurl:"
  Write an awesome description for your new site here. You can edit this
  line in _config.yml. It will appear in your document head meta (for
  Google search results) and in your feed.xml site description.
baseurl: "" # the subpath of your site, e.g. /blog/
url: "http://yourdomain.com" # the base hostname & protocol for your site
github_username:  oscgonfer
```

#### Adding navigation

```
# custom collections
collections:
  reflections:
    output: true
    permalink: /:collection/:title/
  hello:
    output: true
    permalink: /:collection/:title/

defaults:
  - scope:
      path: ""
      type: "reflections"
    values:
      layout: post
  - scope:
      path: ""
      type: "hello"
    values:
      layout: post
```

#### Adding plugings

```
# Build settings
permalink: pretty
markdown: kramdown
plugins:
  - jekyll-feed
  - jekyll/figure
  - jekyll-postfiles
```

#### Some extras

```
# include in the final build
include:
  - images/

# Exclude from processing
exclude:
  - README.md
  - Gemfile
  - Gemfile.lock
  - node_modules
  - vendor/bundle/
  - vendor/cache/
  - vendor/gems/
  - vendor/ruby/
```


!!! info "More references"
        * [Setting Up MkDocs by Krisjanis Rijnieks](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week01_principles_practices_project_management/mkdocs.html)
        * [Gitlab Pages Docs](https://docs.gitlab.com/ee/user/project/pages/)
        * [Jekyll Docs](https://jekyllrb.com/docs/)
        * [Hugo Docs](https://gohugo.io/documentation/)
        * [Official Markdown project](https://daringfireball.net/projects/markdown/)
        * [Markdown tutorial](https://www.markdowntutorial.com/)
        * [Strapdown.js -> From Markdown to HTML](http://strapdownjs.com/)