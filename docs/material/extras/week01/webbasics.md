# How the web works

!!! Note
    - [HTML SIMPLE TEMPLATE]()
    - [HTML SIMPLE TEMPLATE FOR ACADEMY](assets/Html-template-academy/index.html)
    - [Download it here](assets/Html-template-academy.rar)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSEWikJkQBGpCkUwxrVq9s84cYB_VK_V-BC1PKFPaQ5JdrhyxDYehdiyaQXkRXIt4SxEdLHORAz6DbP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Resources
- How the web works: [https://github.com/vasanthk/how-web-works](https://github.com/vasanthk/how-web-works)

### Bootstrap html + css generator

- [Online tool](https://www.layoutit.com/es)
