# Git Basics - Version Control

<div style="text-align: center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5iFnzr73XXk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## Our problem

How many times have you done this?

```
project_final.doc
project_final_final.doc
project_final_final_this_one.doc
project_final_final_this_one_yes.doc
project_final_final_final_mega_final.doc
```

## Our solution - Version Control Systems

<div style="text-align: center">
<img src="https://imgs.xkcd.com/comics/git_2x.png" width="300px">
</div>
_source: [https://xkcd.com/1597/](https://xkcd.com/1597/)_

**Version Control** is the management of changes to documents, computer programs, large websites and other collection of information. There are two types of VCS:

- Centralized Version Control System (CVCS)
- Distributed Version Control System (DVCS)

### Centralized VCS

Centralized version control system (CVCS) uses a central server to store all files and enables team collaboration. It works on a single repository to which users can directly access a central server.

![](https://i.imgur.com/06Z93zL.png)

_Drawbacks:_

- It is not locally available; meaning you always need to be connected to a network to perform any action
- Since everything is centralized, in any case of the central server getting crashed or corrupted will result in losing the entire data of the project

### Distributed VCS

In Distributed VCS, every contributor has a local copy or “clone” of the main repository i.e. everyone maintains a local repository of their own which contains all the files and metadata present in the main repository.

![](https://i.imgur.com/Mvru6ub.png)

## Welcome to Git

<img height="400px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/1200px-Git-logo.svg.png" alt="Git-logo.svg">

**What is git?**

> Git is a **distributed version control system** for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used **to track changes in any set of files**. Its goals include speed, data integrity, and support for distributed, non-linear workflows.

**Why Git?**

- Free and Open Source
- Speed
- Scalable
- Reliable
- Distributed Development

### Git Local/Remote Structure

As a reference, this is a basic structure of the local/remote structure using git.

![](https://image.slidesharecdn.com/gitbranchstregagycasestudywoogenius-140314152231-phpapp01/95/git-branch-stregagy-case-study-2-638.jpg?cb=1413975847)

!!! warning "Time to follow a guide"
    Follow [this guide](../../../../guides/code/gitsetup) to set it all up

## Interactions

Here there are some basic interactions you will normally do with your git repositories.

### Cloning

Cloning takes a remote repository and literally _clones it_ into a local one:

```
git clone git@gitlab.com;flu/supermegasuperguay.git
```

### Status

Shows what is the status of your local/remote repository - if there are local or remote changes, or things to be pushed:

```
git status
```

### Log

Shows the history of the local and remote repositories as a list of commits with more info:

```
git log
```

### Basic changes workflow

You will most likely be using this workflow for git

#### Pull-add-commit-push

- Download the last copy from the online repository and merge it with your local working directory:

    ```
    git pull
    ```

- Add the new files you added to git
    
    ```
    git add index.html
    ```

    or to review each `sloc`:

    ```
    git add -p
    ```

- Now name your update, so you know what you changed with this push.
    
    ```
    git commit -m "COMMIT MESSAGE"
    ```

!!! danger "About the commit message"
    This is a general point of failure for many many students (and instructors) that do not make a relevant commit message. 

    Write a **meaningful commit message**. This should answer the question: 
    > "If I apply this commit, I will... <commit message>". 
    
    For example: 
    > "uploading final project idea"
    
    This is not OK at all and will not help anyone to trace problems (and they will happen):

    ![](assets/bad-repo.png)

- Upload to the Repository

    ``` 
    git push
    ```

!!! warning "This is 1% of what git does"
    Git is much more than this. Do not hesitate to visit [the docs](https://git-scm.com/doc) to get more info or to follow a tutorial on more advanced features.

## Guidelines

!!! warning "Fabacademy Only"
    Students can login on gitlab using their **email address** (the one they use for registration) and entering their Student ID as a temporary password (they need to change it asap)

### Sizes

!!! warning "Fabacademy Only"
    There's a soft limit on **300mb**.
    Sites reaching 300mb will be listed on a dedicated page in the archive for public _shame_. Site over 300mb for two weeks in a row will get special consideration.

The size of the repository is very important for faster deploys and image loads. **Images in a website don't need to weight more than 1Mb**.

!!! Note "Pro tip"
    Image optimization and why to do it:
    [Automating Image Optimization](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/automating-image-optimization/)

### Tips and tricks

!!! warning "New with the terminal?"
    Visit [this guide](guides/code/terminal) to get started!

**Check the file-size of your folder!** 

You should commit 1-2MB per week. Not more!

Run the following command (Terminal or GitBash) in your folder to see folder and file-sizes:

```
du -sk * | sort -n
```

Each time you want to update your website, you should go through the following commands:

    ```
    cd ~/Desktop/fablabbcn
    git pull
    <change files - modify the files in your folder> 
    git status
    git add .
    git commit -m "<commit message>"
    git push
    ```

!!! Note
    If you end up in Vim-Editor, quit without saving by typing: `:q!`

!!! warning 
    Your website might not update in your Browser until you clear the cache (Hard Refresh!)
    
    If you still get error messages, [study git](https://try.github.io/) or write an email to your instructor.

## Resources

 * [A graphic explanation](https://onlywei.github.io/explain-git-with-d3/)
 * [Book](https://git-scm.com/book/en/v2)
 * [Git Branching](https://learngitbranching.js.org/)
 * [GitLab-Atom-Basics](https://doane-ccla.gitbook.io/docs/git-version-control/git-workflow)
 * [Guide for terminal basics](https://www.digitalcitizen.life/command-prompt-how-use-basic-commands)

 ![](https://i.imgur.com/tIsStU6.png)