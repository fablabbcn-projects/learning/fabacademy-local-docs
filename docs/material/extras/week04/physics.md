# Basic Electronics: the physics

## Basic concepts

In this class we'll try to get a basic understanding of the physics of electricity that make possible modern electronics:

- Particle's electric charge and their role in atoms
- Voltage, current and resistance
- Ohm's law

!!! info "Learning from the best"
    [This page](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law) summarizes the basics in a very nice way.

**Particles charge**

Objects can hold an **electric charge** (either positive or negative), or not (meaning they are neutral). In subatomic particles, **protons hold positive charge**, **electrons hold negative charge** and **neutrons are neutral**. We find these particles in atoms and they define the world as we see it, being the underlying structure of what we understand as elements (Hydrogen, Helium, Uranium...). 

In an atom, the core holds protons and neutrons together, while electrons are orbitting around them:

![](https://cdn.sparkfun.com/assets/3/4/1/a/3/51a65d7bce395f156c000000.png)

**Energy**

Electrons can be forced out of their stable orbit around the atom's core **if we apply some energy**. In this situation, we can create areas in the material with **a relative positive charge**, and others with **a more relative negative charge** (like the poles of a battery or when we load up a balloon with electrostatic charge).

![](https://cdn.sparkfun.com/r/400-400/assets/a/6/2/4/4/519fb817ce395fff0a000000.png)

**Voltage**

When we **force** electrons to group in a certain area, leaving another area without electrons, we create a **difference in voltage**. This voltage is the relationship between the energy we applied and the electric charge: _E = V x Charge_

!!! Note "The units"
	Energy is measured in Jules (J), voltage in Volts (V) and Charge in Coulombs (C). Note that all the units that come from the discoverer's name are capitalised!

When two objects have a difference in voltage, we can say that their electrons will try to jump from one another, to balance out the situation and become stable. Voltage is expressed in Volts (V). This voltage can be constant with time, or alternating.

**Current**

When two objects are subject to a difference voltage, electrons will try to come back to their position. When doing so, we say there is an _electric current_ or just _current_. This movement of electrons inside a material is measured in Amperes (A) or just Amps. If we have alternating voltage, we will have also alternating current (AC), and the same with constant voltage, in which case we will have DC.

![](https://cdn.sparkfun.com/assets/9/5/6/1/4/519fcd42ce395f804c000000.gif)

![](https://cdn.sparkfun.com/assets/a/0/9/4/0/51a52b62ce395f2f25000001.gif)

**Ohm's law**

For electrons to go from one point to another, when subject to voltage, they will have it more or less difficult to go through. How difficult it is, is called **resistance**, and it's measured in Ohms (Ω). Georg Ohm discovered that voltage (V), resistance (R) and current (I) go with the following formula: _V = I R_

Meaning that when the resistance is two high, there is almost no current (and when the current is 0, it means we have an _open circuit_).  When the resistance is almost 0, the current can be very big, leading to what we call a _short_.

![](https://i.imgur.com/XHc0dg6.jpg)

## What is a multimeter

The multimeter is a device that combines several measurement functions in one single unit. At it's minimum can measure **Voltage**, **Resistance** and **Current**. It also serves as a debugging tool to check for **continuity between points** in the circuit, either intentional or unintentional.

![](http://i.ebayimg.com/images/i/271944706906-0-1/s-l1000.jpg)

Here are some resources for better understanding the multimeter and learning how to use it!:

- [Sparkfun tutorial](https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/all)
- [How to use a multimeter](https://www.sciencebuddies.org/science-fair-projects/references/how-to-use-a-multimeter): this page also contains a summary of the symbols and different forms in which they are represented in diferent multimeters

## Hands on time!

!!! info "BOM (per group)"
    For doing this, you'll need:
        - 1x arduino (UNO is enough) + power cable
        - 3 - 4 different resistors
        - LED
        - A Diode
        - Big electrolitic capacitor
        - Jumper cables
        - A switch

**Basic Diode + Resistor**

For this example, you will need no code.

![](https://i.imgur.com/HIxEDPI.png)

1. Visualise the effect of changing the resistor on the LED
2. Use the multimeter to measure the voltage at the ends of the resistor

**Basic Diode + Resistor** - 

For this example, you will need to run the _Blink code_.

![](https://i.imgur.com/HIxEDPI.png)

1. Does the LED blink when we press the button?

**Low pass filter**

For this example, you will need no code.

![](https://i.imgur.com/btMpOW0.png)

1. Charge the capacitor for some seconds
2. Disconnect the power from the breadboard
3. Press the button!
4. Charge the capacitor again
5. Disconnect the USB (pay attention to the LEDs in the arduino)

**Low pass filter**

For this example, you will need to run the _Blink code_.

![](https://i.imgur.com/glXyJmh.png)

1. How does the LED blink now? Is it smoother?

**Low pass filter with diode protection** - No code

![](https://i.imgur.com/lORaOc1.png)

1. Charge the capacitor for some seconds
2. Disconnect the power from the breadboard
3. Press the button!
4. Charge the capacitor again
5. Disconnect the USB (pay attention to the LEDs in the arduino)
Is there any difference with respect to the previous example?
