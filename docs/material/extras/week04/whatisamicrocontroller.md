# What is a microcontroller?

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/p0ASFxKS9sg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Representing information

![](https://i.imgur.com/WIbvuIC.png)

How do we represent information using electric phenomena? Let's start representing information with our very basic circuit:

![](assets/batt_fan.png)

!!! Note "A **couple of questions**"
    - How much information are we able to represent with this circuit?
    - How many possibilities do we have?

### Digital Logic in an Analog World

> A single switch can be on or off, enabling the storage of 1 bit of information. Switches can be grouped together to store larger numbers. This is the key reason why binary is used in digital systems.

![](https://usercontent1.hubstatic.com/13809446.gif)

With a bank of eight switches we can _store_ 2^8^ = 256 possible numbers. Each of those switches is called **1 bit**, and 8 of them are a **byte**:

![](https://usercontent2.hubstatic.com/8735065_f520.jpg)

This implies then, that to represent information with electricity, we have one very important limitation. **Information becomes discrete**:

![](https://i.imgur.com/9D2GKW6.png)

Of course, depending on the amount of bits we use we have a more precise curve:

![](http://pediaa.com/wp-content/uploads/2015/08/Difference-Between-Analog-and-Digital-Signals-A2D_2_bit_vs_3_bit.jpg)

And if we put enough of them, we can represent more complex things like this:

![](https://i.imgur.com/4MAPJzp.png)


<iframe width="560" height="315" src="https://www.youtube.com/embed/gI-qXk7XojA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Logic Levels

Now that we have talked about representing information with single bit, we have to agree on how things are going to talk to one another. For this, we standardise what we consider as a **1** (high voltage) and **0** or low voltage. We call this **Logic levels**:

![](https://i.imgur.com/zG4NPwb.png)

### The core component: Transistor

Wouldn't it be ideal if we had a tiny mini switch we could control by ourselves? The transistor is nothing else than an electronic switch.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/BJT_symbol_NPN.svg/1200px-BJT_symbol_NPN.svg.png)

And it basically works like this:

![](https://www.build-electronic-circuits.com/wp-content/uploads/2014/05/transistor-current-explanation.png)

We will see how this transitor helps us build more complex things, by controlling how they are connected together.

!!! Note "Reference Tutorials"
    - [Binary](https://learn.sparkfun.com/tutorials/binary)
    - [Why we use binary](https://www.youtube.com/watch?v=thrx3SBEpL8)
    - [Logic Levels](https://learn.sparkfun.com/tutorials/logic-levels/all)
    - [Guerrilla electronics](http://lcamtuf.coredump.cx/electronics/)

## Putting things together: circuits

![](https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.makerspaces.com%2Fwp-content%2Fuploads%2F2017%2F05%2Fled-circuit-breadboard-800x474.jpg&f=1)

![](https://www.autodesk.com/products/eagle/blog/wp-content/uploads/2017/02/LED-circuit.png)

![](https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.khominhvi.com%2Fwp-content%2Fuploads%2F2013%2F05%2FBreadboard_complex.jpg&f=1)

![](http://i.stack.imgur.com/AI4oM.png)

### Combinational vs sequential

Circuits can be [**combinational**](https://en.wikipedia.org/wiki/Combinational_logic), when they don't have any clue about their own state (like a mechanism):

![](http://2.bp.blogspot.com/_11-1LH0aw4k/SkyxdsoJKZI/AAAAAAAAAAs/F2UGFxSJAEo/w1200-h630-p-nu/dld+combinational.bmp)

Here you have them being used in a Marshall Amplifier:

![](https://www.gearslutz.com/board/attachments/so-many-guitars-so-little-time/382906d1391228908-low-wattage-marshall-style-amp-image_1180.jpg)

But they can also be [**sequential**](https://en.wikipedia.org/wiki/Sequential_logic), when they are _aware_ of their state:

![](https://i.imgur.com/1eOZden.png)

In **combinational logic**, the output is a function of the present inputs only. The output is independent of the previous outputs.

**Sequential logic** is the form of Boolean logic where the output is a function of both present inputs and past outputs. In most cases, the output signal is fed back into the circuit as a new input. Sequential logic is used to design and build finite state machines.

![](https://i.ytimg.com/vi/JQ0JOfxUAgY/maxresdefault.jpg)

Truth table for 1-bit Full Adder (above):

![](https://codestall.files.wordpress.com/2017/06/truthtable_1bit_adder_yz.png)

!!! example "Hands-on"
    Let's make these first combinational circuits (choose one or both):

    **AND gate**

    ![](assets/circuit_03_and.png)

    **OR gate**

    ![](assets/circuit_03_or.png)

## Going integrated

The Apollo Guidance Computer:

![](http://pop.h-cdn.co/assets/17/11/1489433744-screen-shot-2017-03-13-at-33525-pm.png)

With some integrated circuits in flat-pack design:

<div style="text-align:center">
<a href="https://commons.wikimedia.org/wiki/File:Agc_flatp.jpg#/media/Archivo:Agc_flatp.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/1/13/Agc_flatp.jpg" alt="Agc flatp.jpg"></a></div><br>

Now IC's come in tons of flavours:

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/a/9/8/b/8/51c1ea70ce395f5f0d000000.jpg">
</div><br>

![](https://cdn.sparkfun.com/r/600-600/assets/7/a/6/9/c/51c0d009ce395feb33000000.jpg)

<div style="text-align:center">
<iframe width="600" height="400" src="https://www.youtube.com/embed/Fxv3JoS1uY8" frameborder="0" allow="autoplay; encrypted-media; picture-in-picture" allowfullscreen></iframe></div>

And then they become **programmable**:

![](https://i.imgur.com/n7xYW3y.jpg)

Early computing machines were programmable in the sense that they could follow the sequence of steps they had been set up to execute, but the "program", or steps that the machine was to execute, were set up usually by changing how the wires were plugged into a patch panel or plugboard.

A **stored-program computer** is a computer that stores program instructions in electronic memory.

![](https://i.imgur.com/GcIumJD.jpg)

_By Parrot of Doom - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=8318196_

## Now actually, what is a microcontroller?

A microcontroller (MCU for microcontroller unit) is a small computer on a single integrated circuit. A microcontroller contains one or more CPUs (processor cores) along with memory and programmable input/output peripherals. In 1974 Intel released the first microcontroller, the Intel 4004:

<div style="text-align:center">
<a href="https://commons.wikimedia.org/wiki/File:Intel_C4004.jpg#/media/File:Intel_C4004.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Intel_C4004.jpg/1200px-Intel_C4004.jpg" alt="Intel C4004.jpg" width=600></a>
</div><br>

It was a revolution (specially for printers :joy:):

<div style="text-align:center">
<img src="http://2.bp.blogspot.com/-_RtOTJIuT2g/TsUy9TI6itI/AAAAAAAAAS4/lfWcoTSIdEc/s1600/intel+4004+1971.jpg" width=600>
</div><br>

This is one the most used microcontrollers in the class:

![](https://www.theengineeringprojects.com/wp-content/uploads/2018/08/introduction-to-ATtiny85-2.png)

And from them on:

<div style="text-align:center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Transistor_Count_and_Moore%27s_Law_-_2011.svg/1200px-Transistor_Count_and_Moore%27s_Law_-_2011.svg.png" width=600>
</div><br>

Here are some (very brief) characteristics about microcontrollers:

- Microcontrollers are **dedicated to one task** and run one specific program. The program is stored in ROM (read-only memory) and generally does not change.
- Microcontrollers are often **low-power devices**. A desktop computer is almost always plugged into a wall socket and might consume 50 watts of electricity. A battery-operated microcontroller might consume 50 milliwatts.
- Microcontrolles have **inputs and outputs** to communicate with the systems they control and other microcontrollers.

![](https://electrosome.com/wp-content/uploads/2014/09/PIC-16F877A.jpg)

This content below will be dedicated to understanding the need of an ISP and why we make one in the Fabacademy course.

### The memories in a microcontroller

Let's start from the very basics and understand how a microcontroller manages it's **memory**. In general, there are three types of memory in a microcontroller:

- **Data memory** : memory for storing the data processed during run time (including also the registers, stack, etc.). This memory is a volatile memory, meaning, when the device is reset, it will be lost.
- **Program memory**: memory in which the program (or code) is stored. This memory is non volatile, meaning that it won't be lost if the device is reset.
- **EEPROM** : memory which can be used for storing non volatile data and changeable during run-time.

![](https://www.arduino.cc/en/uploads/Tutorial/MemoryMap.jpg)

_Image source: Arduino_

Depending on the type of microcontroller, each memory will have different sizes, or it might not even exist. Here, we will focus on the **Program Memory**, sometimes also called **Flash Memory**. Program Memory (also **R**ead-**O**nly-**M**emory) is used for permanently saving the program (**CODE**) being executed, and it can be divided into two sections: **Boot Program** section and the **Application Program** section.

### The boot loader section

In the image above, you might have noticed that there is a dedicated part of the program memory to something called **Boot Flash Section**. This is also sometimes called **bootloader or boot program section** and it's a part of the program that executes after every time there is a _reset_ of the microcontroller. This area of the memory is protected against deletion and can not be rewritten unless one specifically means to do so. It also has the capability to change the major part (application section) of the flash content. The purpose of this bootloader area is to connect the microcontroller to the outer world, for example a PC, and transfer a new firmware into the main part of the flash memory. This is normally done at the very beginning of the microcontroller wake-up and, in case of: _1._ failure in the transfer or _2._ no PC requesting to send a firmware into the flash, the bootloader should finish it's job and hand-over the control to the main part of the firmware, if any.

**What a bootloader is not**
![](http://archive.fabacademy.org/archives/2016/fablabseoul/students/62/images/img_1048.jpg)

Now, **what happens with the chips we are programming?** Two things:

- Some chips, like the ATtinys, **simply have no bootloader section in the flash memory** (Page 159 of the [ATTiny Datasheet](http://www.mouser.com/ds/2/268/doc8006-1066127.pdf))
- The first time we program them, out of the box, **the chips are not waiting for any external device to program it via Serial Protocol**, such as USB. This means that we need other means to access the flash memory of the microcontroller and, for this, we will use a **in-circuit programmer**.

### What is a Programmer?

[In-system programming (ISP)](https://en.wikipedia.org/wiki/In-system_programming), also called in-circuit serial programming (ICSP), is the ability of some programmable logic devices, microcontrollers, and other embedded devices to be programmed while installed in a complete system, rather than requiring the chip to be programmed prior to installing it into the system.

An ISP programmer is therefore a specialized hardware that will be used to reflash microcontrollers without this bootloader section, which we won't be able to connect directly to a PC. Also, it can be used to reflash the bootloader into the microcontroller. Further reading can be found [here](https://electronics.stackexchange.com/questions/27486/what-is-a-boot-loader-and-how-would-i-develop-one
) regarding bootloaders and ISP, as well as the [Arduino Reference](https://www.arduino.cc/en/Hacking/Bootloader) for this topic. For the different types of chips we have, we will have different programmers.

<div style="text-align: center">
<img src="https://i.imgur.com/oZ2q03c.png">
</div>

!!! Danger "Don't worry"
	We have untangled the mess in [this guide](microcontrollerssummary.md)

!!! Note "More reference"
	Finally, to have further reference, much more detailed information can be found in this [ATMEL/Microchip Application Note](http://ww1.microchip.com/downloads/en/AppNotes/Atmel-0943-In-System-Programming_ApplicationNote_AVR910.pdf).

### What are the pins in a microcontroller?

The microcontroller needs of means to be able to receive power, ground, input signals, and send outputs to the outter world. This are generally in the shape of pins. Sometimes, the pins are accessible for manual soldering, but it's not always the case:

<div style="text-align: center">
<img src="https://i.imgur.com/g33COmd.png">
</div>

There are many different types of pins, some of the more common ones are listed below:

- Voltage / GND
- Digital Input/Output
- Special peripherals, such as ADC
- Timer
- Interrupts, RESET

Normally, these pins are grouped in **PORTS**. In 8-bit architectures, these ports normally comprise a total of 8-pins, and are saved in 8-bit registers. Internally, we can read these registers of these ports, or even change their function.

!!! Note "Arduino"
	The arduino framework and community provide an useful abstraction to this by providing a great amount of libraries that manage the internals of the microcontroller in a more user-friendly way.

**Analog/Digital pin**

A digital pin is able to detect whether there is a voltage present on a pin or not. An analog pin is connected to an internal ADC (Analog to Digital Converter) and can measure the actual voltage on the pin.

A digital pin is suited to detect if a button has been pressed, while an analog pin might for example measure the position of a potentiometer.

![](http://www.droneuplift.com/wp-content/uploads/2016/03/Analog-digi.png)

**Input/Output**

Input pins are used to read the **logic state on some device**. Pin can read if it is logic state “1” or “0”.

Output pins are used to send logic states to some devices. They can be set as **push-pull** or **open collector**. Push-Pull pin means that it can **source and sink** relatively big electrical current to connected hardware (turning ON or OFF one LED). In the case of open collector, it means that they can **sink current only**, but they cannot source it. Generally, all output pins are push-pull in common microcontrollers.

When reading inputs, it's useful to use pull-up/down resistors, in order to avoid shorts, stabilise measurements and avoid faulty readings. The larger the resistance for the pull-up, the slower the pin is to respond to voltage changes.

A pull up resistor is used to bring the pin to the "1" digital level when there is no input:

![](https://i.imgur.com/9kWQ2i9.jpg)

_Image Credit: Sparkfun_

A pull-down resistor is used to force the pin to the "0" digital level when there is no input.

!!! Note "Learn more"
	[Pull-up Resistors](https://learn.sparkfun.com/tutorials/pull-up-resistors/all)

**ADC**

ADC stands for Analog to Digital Converter. It's a system that converts an analog singal into a digital one. The resolution of the converter indicates the number of discrete values it can produce over the range of analog values.

<div style="text-align: center">
<img src="https://sub.allaboutcircuits.com/images/04252.png">
</div>

**GPIO**

A general-purpose input/output (GPIO) is an uncommitted digital signal pin on an integrated circuit or electronic circuit board whose behavior—including whether it acts as input or output—is controllable by the user at run time. 	The ATtiny44, 84 has only one pin as a GPIO.

!!! Note "GPIO in a Raspberry Pi"
	A Raspberry Picontains a great deal of GPIOs that can be used for many things: I2C, Serial, SPI or PWM. Find out more [here](https://www.raspberrypi.org/documentation/usage/gpio/)

### How to talk to a microcontroller

Let's see some basics before going into the specific protocols.

**Serial vs. Parallel**

Parallel interfaces transfer multiple bits at the same time. They usually require buses of data - transmitting across eight, sixteen, or more wires.

<div style="text-align: center">
<img src="https://cdn.sparkfun.com/r/700-700/assets/c/a/c/3/a/50e1cca6ce395fbc27000000.png">
</div>

_Image source: Sparkfun_

Serial interfaces stream their data with a reference signal, one single bit at a time. These interfaces can operate on as little as one wire, usually never more than four:

<div style="text-align: center">
<img src="https://cdn.sparkfun.com/r/700-700/assets/e/5/4/2/a/50e1ccf1ce395f962b000000.png">
</div>

_Image source: Sparkfun_

!!! Note "More"
	Check the [Serial Communication Tutorial](https://learn.sparkfun.com/tutorials/serial-communication) to understand why we use Serial communication in the ISP

**Synchronous vs. Asynchronous**

"Asynchronous" (not synchronous) means that data is transferred without support from an external clock signal. This transmission method is perfect for minimizing the required wires and I/O pins, but it does mean we need to put some extra effort into reliably transferring and receiving data.

<div style="text-align: center">
<img src="https://i.imgur.com/mpK1muw.png">
</div>

_Image source: Sparkfun_

"Synchronous" data interface always pairs its data line(s) with a clock signal, so all devices on a synchronous bus share a common clock. This makes for a more straightforward, often faster transfer, but it also requires at least one extra wire between communicating devices.

<div style="text-align: center">
<img src="https://i.imgur.com/AWi49fR.png">
</div>

_Image source: Sparkfun_

!!! Note "Serial communication"
	[Learn more](https://learn.sparkfun.com/tutorials/serial-communication/all)

#### Some standards

**FTDI**

Future Technology Devices International, commonly known by its acronym FTDI, is a Scottish privately held semiconductor device company, specializing in Universal Serial Bus (USB) technology.[1]

It develops, manufactures, and supports devices and their related software drivers for converting RS-232 or TTL serial transmissions to USB signals, in order to allow support for legacy devices with modern computers.[2]

FTDI provides application-specific integrated circuit (ASIC) design services. They also provide consultancy services for product design, specifically in the realm of electronic devices.

<div style="text-align: center">
<img src="https://i.imgur.com/DSVOCSn.jpg">
</div>

The pinout:

<div style="text-align: center">
<img src="https://i.imgur.com/UnYEgTP.jpg">
</div>

!!! Note "FTDI Needs drivers!"
	Download them [here](https://www.ftdichip.com/Drivers/VCP.htm)

**SPI**

Serial Peripheral Interface (SPI) is a synchronous serial protocol, used for short-distance communication, primarily in embedded systems.

<div style="text-align: center">
<img src="https://i.imgur.com/083jT4a.png">
</div>

_Image Credit: SparkFun_

It uses a master-slave architecture with a single master. The master device originates the frame for reading and writing. Multiple slave-devices are supported through selection with individual slave select (SS) lines.

SPI is used by most programmers to flash the microcontroller's code onto the flash memory.

!!! Note "Want to know more?"
	Check the [SPI section](./networking#SPI) on the networking page, to learn more!

**USB**

The universal serial bus or USB is an standard communication protocol used to unify the connection of peripherals like keyboards, video cameras, printers, attached to personal computers, both to communicate and to supply electric power. USB cables are limited in length, as the standard was meant to connect to peripherals on the same table-top, not between rooms or between buildings.

USB uses [differential signalling](https://en.wikipedia.org/wiki/Differential_signaling) to transfer information, using two signals. The technique sends the same electrical signal as a differential pair of signals, each in its own conductor. The pair of conductors can be wires (typically twisted together) or traces on a circuit board.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;margin: auto 0px;}}</style>
<div class="tg-wrap"><table class="tg">
  <tr>
    <th class="tg-c3ow"><strong>Pin Number</strong></th>
    <th class="tg-c3ow"><strong>Cable Colour</strong></th>
    <th class="tg-c3ow"><strong>Function</strong></th>
  </tr>
  <tr>
    <td class="tg-c3ow">1</td>
    <td class="tg-c3ow">Red</td>
    <td class="tg-c3ow">VBUS (5 volts)</td>
  </tr>
  <tr>
    <td class="tg-c3ow">2</td>
    <td class="tg-c3ow">White</td>
    <td class="tg-c3ow">D-</td>
  </tr>
  <tr>
    <td class="tg-c3ow">3</td>
    <td class="tg-c3ow">Green</td>
    <td class="tg-c3ow">D+</td>
  </tr>
  <tr>
    <td class="tg-c3ow">4</td>
    <td class="tg-c3ow">Black</td>
    <td class="tg-c3ow">Ground</td>
  </tr>
</table></div>
<br>

!!! Note "USB in a nutshell"
	Find it [here](http://www.beyondlogic.org/usbnutshell/usb2.shtml)

More advanced references:

- [**Clipper Circuits**](https://www.allaboutcircuits.com/textbook/semiconductors/chpt-3/clipper-circuits/)
- [**Differential Signal Circuits**](https://ieee.li/pdf/essay/differential_signals.pdf)
