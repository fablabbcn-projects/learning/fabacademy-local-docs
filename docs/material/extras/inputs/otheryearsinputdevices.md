# Electronics concepts - Input devices

## Input devices #1 [Guillem]

**:date: 28/03/2019**

### Core ideas

* Why we need sensors
* Neurons, I/O and Bio indicators
* Sensing the environment
* Close loop systems
* Inferring metrics
    * Bike speed from counting wheel turns
    * Water tank distance from distance from above
* Composed sensors
    * Radar: emitter (output) and receiver (input) working in sync
* Complex post processing: Compur vision
    * From a list of colors (pixel matrix) to meaningful data (i.e. object location based in color tracking)


### How to choose and use a sensor

_The section is aimed at **INPUTS** (sensors) but many concepts work also for **OUTPUTS** (motors, LEDs)_

* How to choose sensor?
    * Integrated circuits, breakout boards, and off-the-shelf devices
        * [IC Component](https://eu.mouser.com/ProductDetail/Bosch-Sensortec/BNO055)
        * [Adafruit IMU Breakout](https://www.adafruit.com/product/2472)
        * [Wii Nunchuck](https://en.wikipedia.org/wiki/Wii_Remote#Nunchuk) (check in Wallapop, Cash converters, Ebay...)
* How to wire a sensor?
    * We will always look for well documented sensors [learn.sparkfun.com](https://learn.sparkfun.com/), [learn.adafruit.com](https://learn.adafruit.com/)
    * Download the EAGLES from the Breakout boards to help you, [example](https://github.com/adafruit/Adafruit-GUVA-Analog-UV-Sensor-Breakout-PCB)
    * Electronic recipes
        * [Voltage dividers](https://learn.sparkfun.com/tutorials/voltage-dividers/all)
        * [Pull up and pull down resistors](https://learn.sparkfun.com/tutorials/pull-up-resistors/all)
* How to code for a sensor?
    * [Fab Academy sensors](http://academy.cba.mit.edu/classes/input_devices/index.html)
    * Sensor interfaces:
        * Is it on? [DigitalRead](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalread/)
        * What's the voltage? Analog to Digital Converter [AnalogRead](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/)
        * How many times on? [Pulses](https://www.arduino.cc/reference/en/language/functions/advanced-io/pulsein/)
        * Tell me so[Digital Communication](https://learn.sparkfun.com/tutorials/serial-communication/all): [Serial / UART](https://www.arduino.cc/en/reference/serial), [SPI](https://www.arduino.cc/en/Reference/SPI), [I2C](https://www.arduino.cc/en/reference/wire) :warning:  *We will always use an specific sensor library implementing the comunication*


## Input devices #2 [Guillem]

**:date: 29/03/2019**

### Playing with your Hello Board

* [Code Examples] https://github.com/fablabbcn/arduino-workshops/tree/master/Hello%2044

### When the ATtiny is not enough: Other microcontrollers!

_The ATtiny can do amazing things but will require us to optimize the code to an uncessary level when we just want a protoype..._

* [ATtiny44](https://www.microchip.com/wwwproducts/en/ATtiny44)
    * Example board: [HelloBoard](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo)
    * FLASH 4KB
    * RAM 256B
    * **Pro:** Digital and Analog Inputs and Outputs
    * **Con:** No Hardware I2C or Serial

* [ATtiny84](https://www.microchip.com/wwwproducts/en/ATtiny44)
    * Example board: [HelloBoard](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo)
    * FLASH 8KB
    * RAM 256B
    * **Pro:** Digital and Analog Inputs and Outputs
    * **Con:** No Hardware I2C or Serial

* [ATMEGA328](https://www.microchip.com/wwwproducts/en/ATmega328)
    * Example board: [FabDuino](http://fabacademy.org/archives/2015/doc/projects/fabkit-0.4.html)
    * FLASH 32KB
    * RAM 2KB
    * **Pro:** Hardware I2C, Serial and SPI
    * **Con:** No USB Hardware (requires an FTDI)

* [ATSAMD21G18](https://www.adafruit.com/product/3405)
    * Example board: [Adafruit Feather M0](https://www.adafruit.com/product/2772)
    * FLASH 256KB
    * RAM 32KB
    * **Pro:** Multiple I2C, SPIs, Serial and full speed USB
    * **Con:** No WIFI or Bluetooth

* [ESP32](https://www.espressif.com/en/products/hardware/esp32/overview)
    * Example board: [ESP32 Feather Board](https://www.adafruit.com/product/3405)
    * FLASH 4MB
    * RAM 520KB
    * **Pro:** WIFI and Bluetooth
    * **Con:** No USB Hardware (requires an FTDI)

## Output devices #1 [Victor]

**:date: 04/04/2019**

* Actuating, closing the loop.
* Controlling electricity to generate movement or light.

### Types of actuator
![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/DoorBell_001.jpg/1920px-DoorBell_001.jpg)
![](https://upload.wikimedia.org/wikipedia/commons/9/95/Electric_Bell_animation.gif)

* Electromagnetic
    * DC motors
    * Brushless motors
    * Servo motors
    * Stepper motors
    * Relays, solenoids, valves, etc
    * Speakers
* Electroluminescence
    * Leds
    * RGB
    * Addressable
    * LCD's (polarized light and liquid crystal)
* Piezoelectric
    * Buzzer and speaker.

* [Fab Academy sensors](http://academy.cba.mit.edu/classes/output_devices/index.html)
### Controlling outputs
* LED's
    * One color, PWM for intensity
    * RGB, PWM
    * Addressable -> libraries [guides](https://learn.adafruit.com/adafruit-neopixel-uberguide)
    * Charlieplexing [image](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/3-pin_Charlieplexing_with_common_resistors.svg/1920px-3-pin_Charlieplexing_with_common_resistors.svg.png)
* DC motor
    * H-bridges
    ![](https://cdn.sparkfun.com/assets/learn_tutorials/1/9/3/h-bridge-circuit-600w.gif)
* Brushless motor
    * Search
* Stepper Motor
    * Unipolar
    * Bipolar
    * [IC Component](https://www.digikey.com/product-detail/en/allegro-microsystems-llc/A4982SETTR-T/620-1340-1-ND/2237994)
    * [Step stick](https://www.pololu.com/product/1182)
* Buzzer and Speaker
    * Arduino Tone function
    * Amplifiers

### How to wire an output?
* We will always look for well documented output [learn.sparkfun.com](https://learn.sparkfun.com/), [learn.adafruit.com](https://learn.adafruit.com/)
* Download the EAGLES from the Breakout boards to help you, [example](https://github.com/adafruit/Adafruit-TB6612-Motor-Driver-Breakout-PCB)
* Electronic recipes
    * [Voltage dividers](https://learn.sparkfun.com/tutorials/voltage-dividers/all)
    * [Power Transistors](https://learn.sparkfun.com/tutorials/transistors/all)
    * Integrated Circuits: H-Bridges, Darlington Arrays...
### How to code for an output?
* Output interfaces:
    * On/Off: [DigitalWrite](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/)
    * Pulse With Modulation: [AnalogWrite](https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/)
    * [Pulses](https://www.arduino.cc/reference/en/language/functions/advanced-io/pulsein/)
    * [Digital Communication](https://learn.sparkfun.com/tutorials/serial-communication/all): [Serial / UART](https://www.arduino.cc/en/reference/serial), [SPI](https://www.arduino.cc/en/Reference/SPI), [I2C](https://www.arduino.cc/en/reference/wire) :warning:  *We will always use an specific sensor library implementing the comunication*

### More outputs recipes

![](https://i.imgur.com/uug5r4L.jpg)
![](https://i.imgur.com/AjcJBRI.jpg)
![](https://i.imgur.com/i4KVRjS.jpg)
![](https://i.imgur.com/db4IDdg.jpg)
![](https://i.imgur.com/a2DAcQR.jpg)
![](https://i.imgur.com/ntOaYfM.jpg)
![](https://i.imgur.com/kFhHwv3.jpg)
![](https://i.imgur.com/cIRinZX.jpg)
![](https://i.imgur.com/wfHSsG7.jpg)
![](https://i.imgur.com/ZfaGMG9.jpg)
![](https://i.imgur.com/sObt2x4.jpg)
![](https://i.imgur.com/gzf5pUh.jpg)
![](https://simple-circuit.com/wp-content/uploads/2018/10/arduino-uinpolar-stepper-motor-control-uln2003a-circuit-1024x453.png)
![](https://i1.wp.com/www.circuitspecialists.com/blog/wp-content/uploads/2012/01/bipolar-vs-unipolar-stepper-motors-circuit-specialists-blog.gif?resize=451%2C305&ssl=1)

## Debugging inputs and outputs [Victor]

**:date: 05/04/2019**

* Divide and conquer.
* Always subdivide the system as much as posible.
* Metodic tests

### Hardware
* Prototyping before building
![](http://3.bp.blogspot.com/-OlQldAQqZq0/TyeizTbznNI/AAAAAAAAAEU/E8HemqLdZqg/s1600/IMG_0633.JPG) vs ![](https://i.redd.it/5zigf4c0ilwz.jpg)
* Multimeter checks
* Continuity before and after soldering components
* Voltages
* Isolating hardware parts, cutting traces, solder bridges, jumpers, designing for debug.
* Unconsistent tests --> heat or power problems.

### Software
* Creating small sketches for testing specific parts
* Using Serial print.
* Using defines to build a better Serial Print debug
* Using comments to disable code
* Structuring the code in functions
* Always review compiler error messages.
* Get someone else to review your code

## Advanced inputs and outputs recipes and coding [Guillem]

* Notes [here](https://hackmd.io/Uw1XxGa7S3qNrL7nChGrcA)

==================
This is a copy of the Guillem's class notes. You can find the latest version [here](https://hackmd.io/uEv4u6B3QPG2QpZk6vOCeg?both)
====================
