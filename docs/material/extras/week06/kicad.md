---
title: Kicad class
Autors: Santi Fuentemilla, Rutvij Pathak, Eduardo Chamorro, Xavi Dominguez, Óscar González, Esteban Gimenez, Josep Martí
---

# Kicad Electronics design

## Basic workflow

Our workflow in the lab:

![](assets/workflow-eda.png)

The industrial workflow:

![](assets/workflow-eda-industrial.png)

## What we'll use: Kicad

KiCad is an **open source**, cross-platform Electronics design program. It can be found in the [official-kicad-website](http://kicad-pcb.org/download/).

### Basic concepts

**Symbols**

Symbols are the representation of components in the schematic of any EDA. They look like this:

![](https://electronicsmaker.com/wp-content/uploads/2018/07/kicadrelease-300x221.png)

**Footprints**

Footprints represent the projection of the physical component onto the pcb so that we can make the connection. They look like this:

<div style="text-align: center">
<img src="https://www.build-electronic-circuits.com/wp-content/uploads/2018/01/kicad-footprint.png" width="400px">
</div>

### Understanding Kicad

Kicad is a set of different tools that are used for:

- Create schematics with **Eeschema**
- Create PCB layouts with **Pcbnew**
- Generate and see Gerber files with **Gerber Viewer** (we won't use this yet)

The workflow goes like:

- Import libraries into Kicad (only to be done once)
- Create the schematics with Eeschema
- Create a Netlist
- Import the Netlist into the Pcbnew editor
- Create the PCB layout
- Export into SVG
- Edit the file into a vector editing program (Inkscape or similar)
- Machine it
- Solder it

You can see an overview of the hole workflow below:

![](assets/kicad_flowchart.png)

!!! info "Where to find help?"
	- [Kicad Docs](https://docs.kicad.org/5.1/en/)
	- [Kicad tutorials](https://kicad.org/help/tutorials/)

### Getting the libraries

During Fabacademy, we will be using the FAB library, which is adapted for our inventory and SMD (or _surface mount devices_). You can find the libraries, adapted for Kicad V5.* in the Global Fabacademy repository [DOWNLOAD THE OFFICIAL FABACADEMY LIBRARIES HERE](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad).

We'll use some official libraries too like the [kicad-symbols](https://github.com/KiCad/kicad-symbols) libraries. If you want to keep it up to date, get:

```
git clone git@github.com:KiCad/kicad-symbols.git
```
or download them [here](https://github.com/KiCad/kicad-symbols)

For the [kicad-footprints](https://github.com/KiCad/kicad-footprints) you can also get them:

```
git clone git@github.com:KiCad/kicad-footprints.git
```
or download them [here](https://github.com/KiCad/kicad-footprints)

### Import libraries

First, we have to include the **fab library - fab.lib**, specifically for Kicad. For this, we will:

1. Click on **Preferences > Manage Symbol Libraries...**.
![](assets/kicad_manage_symbol_libraries.png)
2. There, click on _Add existing library to table_ and select the **fab.lib** library we have just downloaded:
![](assets/symbol_import.png)
3. Press OK and come back to the main screen.
4. Click on **Preferences > Manage Footprint Libraries**:
![](assets/kicad_footprint_libraries.png)
5. There, click on _Add existing library to table_ and select the **fab.pretty** library we have just downloaded:
![](assets/kicad_footprint_libraries_select.png)
6. We are all set!

### Create an schematic

Now, we can launch the **Eeschema - Electronic schematic editor** and begin with the schematics design. In order to add components, we will click on the **Place component** icon and selecting the elements needed from either the fab library we have included or other custom libraries of our own. Refer to [the docs](https://docs.kicad.org/5.1/en/eeschema/eeschema.html#schematic-creation-and-editing) for more details.

!!! Note "Kicad like a pro"
	Some quick tricks for Kicad: hover over the component and click on:

	- _R_ to rotate the component
	- _C_ to copy it
	- _M_ to move it
	- _Delete_ to delete it
	- _W_ to draw a wire

!!! Warning
	Also, one has to be careful of what we click on, since the values, titles or the component themselves are different entities. Finally, a very convenient tool in Kicad is the **Place global label**, marking lines or ports with labels, in order to make the schematics clearer.

In the schematic we basically want to place components and connect them with wires. As simple as that. Once we have finished with the schematics design, having assigned all the names, values and footprints to the components, we should click on **Generate Netlist**, which is a list of components and references which serves as a link between the schema and the PCB layout editor:

![](assets/kicad_generate_netlist.png)

![](assets/kicad_generate_netlist_2.png)

This will create a `.net` file that we will be import later into the **Pcbnew editor**.

### PCB-design

We can now close the Eeschema editor and open PCBnew. First thing is to load the Netlist by clicking on **Read netlist**. We can keep normal settings and click on **Read Current Netlist**. One should carefully read the Messages area in case of any error (normally in red) and review the schematic in Eeschema in case of errors.

!!! Warning "Common mistakes"
	A common mistake is to not set properly the names to the components (repeated names) or not assigning the footprints properly:

![](assets/kicad_read_netlist.png)

!!! Note "Basic definition"
	In kicad, each potential label or signal is called _a Net_. For instance, GND, SCK, VCC are **nets**.

Now, it is time to set the **design rules**. For this, click on **Setup > Design rules...>**. We see two tabs, the **Net Classes Editor** and the **Global Design Rules**.
The latter applies to all the design, and we will use a minimum track width of 0.4mm, leaving the rest to default (since we are not placing vias). In the **Net Classes**, we can specify classes for each Net. For example, we would maybe want to specify different rules for the power signals and the I2C protocol ones. We can do so in this tab.
We will be using the same width for all the tracks, and for this we will edit the Default profile. If we wanted, we could add more, and generate for example a different one for power (see the example below).

The set of rules to be used are shown below, which comply with the tools we'll be using for the tracks (1/64"). Sometimes, you can use 0.2mm for the clearance, if you want to pass between the pads of the ISP header, for instance:

![](assets/kicad_class_design_rules.png)

Note that when setting these design rules, the dropdown at the top should change to the stablished rule:

![](assets/kicad_class_rules_ok.png)

!!! Note "Example of more complex Net Classes Rules"
	In a more complex set of rules, we could have different widths for our ISP and Power tracks.

	![](assets/kicad_class_design_rules_top.png)

	To change them, select the tracks you want to change on the **multiple selection box** on the left. Select a target on the dropdown on the right, and click on the _>>>_ button to move them over.

	![](assets/kicad_class_multiple_net_rules.png)

!!! Note "Kicad like a pro"
	If you want to change the design rules in the middle of your PCB design, you can hover over the desired trace and **press U**. This will select the whole trace. Then, if you **press E**, you can enter this dialog box:

	![](assets/kicad_class_pcb_trace.png)

	In there, you can click on **Use net class width** and it will automatically use the defined width in the Net Classes Rules.

	Also!, if you want to drag tracks, you can hover over and **press D** (to keep nice 45 degree angles). Then, you can adapt easily the tracks without having to redraw the whole thing.


Now it's time to do the routing. At the begining, all the components are disconected (only with white lines) and all on a pile. With the help of the _M key_ we can arrange them quickly and comence with the routing. We can add tracks between the pads by clicking on _Place trace_ and selecting the pads. The tracks and pads are added in the F.Cu layer, while the cuts are done in the Edge.Cuts layer. We can draw the cuts using the **Add graphic line or polygon** tool.

!!! Note "Kicad like a pro"
	If you want, you can make a zone of your PCB filled with a certain part of your circuit, for instance, **ground**. Sometimes, this is helpful when the circuit is subject to external interference. For this, select either the F.Cu or B.Cu layer and click on the _Add filled zone_ icon on the right panel:

	![](assets/kicad_class_filled_area.png)

	Click where you would like to start the filled zone, and configure the zone in the dialog below:

	![](assets/kicad_class_filled_zone_dialog.png)

	The most important setting is the **Net**, which is the actual signal or net that your zone is going to be connected to. The default settings work fine, but make sure 	that the _Default Pad Connection_ is set to _Thermal Relief_, so that heat does not disperse through your filled zone that easily while soldering.
	Once you are done, make a polygon where the zone is going to be in, and **Double click to exit**.

#### Cut-out

The PCB cut-out is done in the Edge.Cuts layer. In this layer, we will use the **Add Graphic Lines** tool:

![](assets/kicad_class_edges.png)

Here, it's interesting to have the cross-hair cursor (6th button on the left pannel) to help us drawing.

!!! Note "Wider line?"
	Zoom in and **double click** on the line to edit it's width.

### Export

!!! Note "The super nice way"
	Check this repo for more instructions: https://gitlab.com/fablabbcn-projects/electronics/kicad-exports

Once we have it routed, it's time to go for the fabrication. There are several ways to export, and the common way of doing so if the PCB was to be manufactured outside of a Fablab, would be to _Generate the [gerber](https://en.wikipedia.org/wiki/Gerber_format) files_. In our case, we will export an SVG file, that we can later on import in [Fabmodules](http://fabmodules.org), or generate a PNG.

If you want to export it to SVG and then use a vector program to edit it ([Inkscape](http://inkscape.org), or Illustrator), click on **File > Export SVG** with the following configuration:

![](assets/kicad_export_svg.jpg)

Pay attention that we will save **all of the layers into the same file**, because we will be editing the SVG in a vector program.

!!! Note "Directly to fabrication?"
	Then, export with the layers in separate files, and you will have the cuts and the tracks in different svgs.

### Traces file creation

!!! Warning "Exported SVG to fabmodules?"
	Ignore this if you have exported an SVG with the intention to load it into Fabmodules.

Now, in a program where we can edit vectors, such as Inkscape, we will generate the PNG file that will go into fabmodules or mods. We will:

- Import the SVG file
- Arrange the layers into Traces and Cut
- Export each layer separately into pngs:

First, we import the SVG file and we arange the components into layers:

![](assets/kicad_layers_inkscape.png)

For this:

1. We select **Layer > Layers...** and add _Traces_ and _Cut_.
2. Then, we select the traces components and move them to the _Traces_ layer. We hide this layer.
3. We select the cuts and put them in _Cut_
4. We fill the object with the _paint bucket_ in white
5. We add a rectangle, with black fill that covers the hole cuts area and move it to the background
6. We activate show the traces layer
7. Select everything and move it to (0,0)

!!! Note "Inkscape units"
	It is better to work in milimeters in _Inkscape_. For this, select mm under **File > Document Properties > Document Units**

	![](assets/kicad_inkscape_doc_prop.png)

Once we have done all this, we can export each layer to a PNG. Take care of the position of the export (X0, Y0 and X1, Y1 or width / height) to match the actual size. In here, the background rectangle from step 5 is useful:

![](assets/kicad_inkscape_export_png.png)

And we are done! Now we can send to the milling machine!

!!! Note "Want to do it with code?"

	Check [this repository](https://github.com/oscgonfer/gerber2png.py) to export gerber files from kicad and save them as png directly.

## Class example

[This is the example](assets/Class.zip) we did during class. It won't work at all, but serve as an example.
