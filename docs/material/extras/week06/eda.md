# EDAs

From [Wikipedia](https://en.wikipedia.org/wiki/Electronic_design_automation):

> **Electronic design automation** (EDA), also referred to as **electronic computer-aided design (ECAD)**, is a category of **software tools for designing electronic systems** such as integrated circuits and printed circuit boards. The tools work together in a design flow that chip designers use to design and analyze entire semiconductor chips. Since a modern semiconductor chip can have billions of components, EDA tools are essential for their design.

We have done two classes for EDAs in Fabacademy. Find the classes here:

- [Autodesk EAGLE](../../../wa/week6extra/eagle)
- [KiCad 5](../../../wa/week6extra/kicad)