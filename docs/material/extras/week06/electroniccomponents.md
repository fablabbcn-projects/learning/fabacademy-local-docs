# Electronics components and circuits

## Components

### Resistors

<p><a href="https://commons.wikimedia.org/wiki/File:Electronic-Axial-Lead-Resistors-Array.jpg#/media/File:Electronic-Axial-Lead-Resistors-Array.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Electronic-Axial-Lead-Resistors-Array.jpg/1200px-Electronic-Axial-Lead-Resistors-Array.jpg" alt="Electronic-Axial-Lead-Resistors-Array.jpg"></a><br>By <a href="//commons.wikimedia.org/wiki/User:Evan-Amos" title="User:Evan-Amos">Evan-Amos</a> - <span class="int-own-work" lang="en">Own work</span>, Public Domain, <a href="https://commons.wikimedia.org/w/index.php?curid=71267805">Link</a></p>

> A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. In electronic circuits, resistors are used to reduce current flow, adjust signal levels, to divide voltages, bias active elements, and terminate transmission lines, among other uses.

- [Resistors](https://learn.sparkfun.com/tutorials/resistors/all)
    - Reading resistors:
        - https://wonderfulengineering.com/wp-content/uploads/2014/06/What-is-a-resistor-9.jpg
        - https://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-resistor-color-code-4-band
        - https://www.hobby-hour.com/electronics/smdcalc.php
- [Resistor wiki](https://en.wikipedia.org/wiki/Resistor#/)

### Capacitors

<p><a href="https://commons.wikimedia.org/wiki/File:Capacitors_(7189597135).jpg#/media/File:Capacitors_(7189597135).jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Capacitors_%287189597135%29.jpg/1200px-Capacitors_%287189597135%29.jpg" alt="Capacitors (7189597135).jpg"></a><br>By <a rel="nofollow" class="external text" href="https://www.flickr.com/people/28643050@N06">Eric Schrader</a> from San Francisco, CA, United States - <a rel="nofollow" class="external text" href="https://www.flickr.com/photos/28643050@N06/7189597135/">12739s</a>, <a href="https://creativecommons.org/licenses/by-sa/2.0" title="Creative Commons Attribution-Share Alike 2.0">CC BY-SA 2.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=37625896">Link</a></p>

> A capacitor is a device that stores electrical energy in an electric field. It is a passive electronic component with two terminals. 

- [Capacitor wiki](https://en.wikipedia.org/wiki/Capacitor)
- [Applications for capacitors](https://learn.sparkfun.com/tutorials/capacitors/application-examples)
    - Ceramic capacitors: non polarized
    - Electrolytic capacitors: polarized

### Diodes

<p><a href="https://commons.wikimedia.org/wiki/File:Diode-closeup.jpg#/media/File:Diode-closeup.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Diode-closeup.jpg/1200px-Diode-closeup.jpg" alt="Diode-closeup.jpg"></a><br>By The original uploader was <a href="https://en.wikipedia.org/wiki/User:Morcheeba" class="extiw" title="wikipedia:User:Morcheeba">Morcheeba</a> at <a href="https://en.wikipedia.org/wiki/" class="extiw" title="wikipedia:">English <span title="free online encyclopedia that anyone can edit">Wikipedia</span></a>. - Transferred from <span class="plainlinks"><a class="external text" href="https://en.wikipedia.org">en.wikipedia</a></span> to Commons., <a href="https://creativecommons.org/licenses/by-sa/2.5" title="Creative Commons Attribution-Share Alike 2.5">CC BY-SA 2.5</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=2542965">Link</a></p>

> A diode is a two-terminal electronic component that conducts current primarily in one direction (asymmetric conductance); it has low (ideally zero) resistance in one direction, and high (ideally infinite) resistance in the other.

- [Diodes](https://learn.sparkfun.com/tutorials/diodes/)
- [More on diodes](https://lcamtuf.coredump.cx/electronics/#19)
- [Diode wiki](https://en.wikipedia.org/wiki/Diode#/)

#### Zener diodes

<p><a href="https://commons.wikimedia.org/wiki/File:Zener_Diode.JPG#/media/File:Zener_Diode.JPG"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Zener_Diode.JPG/1200px-Zener_Diode.JPG" alt="Zener Diode.JPG"></a><br>By <a href="https://en.wikipedia.org/wiki/User:Teravolt" class="extiw" title="en:User:Teravolt">Teravolt</a> (<a href="https://en.wikipedia.org/wiki/User_talk:Teravolt" class="extiw" title="en:User talk:Teravolt">talk</a>) (Transferred by <a href="//commons.wikimedia.org/wiki/User:Nk" title="User:Nk">Nk</a>/Originally uploaded by <a href="https://en.wikipedia.org/wiki/Teravolt" class="extiw" title="en:Teravolt">Teravolt</a>) - I (<a href="https://en.wikipedia.org/wiki/User:Teravolt" class="extiw" title="en:User:Teravolt">Teravolt</a> (<a href="https://en.wikipedia.org/wiki/User_talk:Teravolt" class="extiw" title="en:User talk:Teravolt">talk</a>)) created this work entirely by myself. (Originally uploaded on en.wikipedia), <a href="https://creativecommons.org/licenses/by/3.0" title="Creative Commons Attribution 3.0">CC BY 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=15245249">Link</a></p>

> A Zener diode is a special type of diode designed to reliably allow current to flow "backwards" when a certain set reverse voltage, known as the Zener voltage, is reached. [...] They are used to generate low-power stabilized supply rails from a higher voltage and to provide reference voltages for circuits, especially stabilized power supplies. They are also used to protect circuits from overvoltage, especially electrostatic discharge (ESD). 

- [Zener Diodes wiki](https://en.wikipedia.org/wiki/Zener_diode)

#### Schottky diodes

<p><a href="https://commons.wikimedia.org/wiki/File:Schottky.jpg#/media/File:Schottky.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Schottky.jpg/1200px-Schottky.jpg" alt="Schottky.jpg"></a><br>By Steffen Maisch - photograph, Public Domain, <a href="https://commons.wikimedia.org/w/index.php?curid=6425112">Link</a></p>

> The Schottky diode (named after the German physicist Walter H. Schottky), also known as Schottky barrier diode or hot-carrier diode, is a semiconductor diode formed by the junction of a semiconductor with a metal. It has a low forward voltage drop and a very fast switching action.

- [Schottky diode wiki](https://en.wikipedia.org/wiki/Schottky_diode)

### Oscillators

<p><a href="https://commons.wikimedia.org/wiki/File:Integrierter_Quarzoszillator_(smial).jpg#/media/File:Integrierter_Quarzoszillator_(smial).jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Integrierter_Quarzoszillator_%28smial%29.jpg/1200px-Integrierter_Quarzoszillator_%28smial%29.jpg" alt="Integrierter Quarzoszillator (smial).jpg"></a><br>By User <a href="https://de.wikipedia.org/wiki/User:Smial" class="extiw" title="de:User:Smial">Smial</a> on <a class="external text" href="https://de.wikipedia.org">de.wikipedia</a> - <span class="int-own-work" lang="en">Own work</span>, <a href="https://creativecommons.org/licenses/by-sa/2.0/de/deed.en" title="Creative Commons Attribution-Share Alike 2.0 de">CC BY-SA 2.0 de</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=1052486">Link</a></p>

> An electronic oscillator is an electronic circuit that produces a periodic, oscillating electronic signal, often a sine wave or a square wave or a triangle wave.

- [Oscillators](https://www.sparkfun.com/tutorials/95)
- [Oscillators wiki](https://en.wikipedia.org/wiki/Electronic_oscillator)

### Transistors

<p><a href="https://commons.wikimedia.org/wiki/File:Transistorer_(cropped).jpg#/media/File:Transistorer_(cropped).jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/2/21/Transistorer_%28cropped%29.jpg" alt="Transistorer (cropped).jpg"></a><br>By <a href="https://en.wikipedia.org/wiki/User:Transisto" class="extiw" title="wikipedia:User:Transisto">Transisto</a> at <a href="https://en.wikipedia.org/wiki/" class="extiw" title="wikipedia:">English Wikipedia</a> - <span class="int-own-work" lang="en">Own work</span>, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=11885148">Link</a></p>

> A transistor is a semiconductor device used to amplify or switch electronic signals and electrical power. Transistors are one of the basic building blocks of modern electronics.[1] It is composed of semiconductor material usually with at least three terminals for connection to an external circuit. A voltage or current applied to one pair of the transistor's terminals controls the current through another pair of terminals. Because the controlled (output) power can be higher than the controlling (input) power, a transistor can amplify a signal.

- [Transistor](https://www.build-electronic-circuits.com/wp-content/uploads/2014/05/transistor-current-explanation.png)
- [Transistors wiki](https://en.wikipedia.org/wiki/Transistor)

![](https://www.build-electronic-circuits.com/wp-content/uploads/2014/05/transistor-current-explanation.png)

#### MOSFETs

<p><a href="https://commons.wikimedia.org/wiki/File:Transistor_y_disipador.jpg#/media/File:Transistor_y_disipador.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Transistor_y_disipador.jpg/1200px-Transistor_y_disipador.jpg" alt="Transistor y disipador.jpg"></a><br>By <a href="//commons.wikimedia.org/wiki/User:Willtron" title="User:Willtron">Willtron</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=362298">Link</a></p>

- [**Mosfets**](https://reibot.org/2011/09/06/a-beginners-guide-to-the-mosfet/)
- [**Wiki on mosfets**](https://en.wikipedia.org/wiki/MOSFET)
- [Mosfet must read I](assets/mosfet-must-read.pdf) and [II](assets/mosfet-must-read-2-slup169.pdf)

### Regulators

#### LDOs

<p><a href="https://commons.wikimedia.org/wiki/File:VREG-SO23-X-V33D-AD_(16237026677).jpg#/media/File:VREG-SO23-X-V33D-AD_(16237026677).jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/VREG-SO23-X-V33D-AD_%2816237026677%29.jpg/1200px-VREG-SO23-X-V33D-AD_%2816237026677%29.jpg" alt="VREG-SO23-X-V33D-AD (16237026677).jpg"></a><br>By <a rel="nofollow" class="external text" href="https://www.flickr.com/people/33504192@N00">oomlout</a> - <a rel="nofollow" class="external text" href="https://www.flickr.com/photos/snazzyguy/16237026677/">VREG-SO23-X-V33D-AD</a>, <a href="https://creativecommons.org/licenses/by-sa/2.0" title="Creative Commons Attribution-Share Alike 2.0">CC BY-SA 2.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=40551728">Link</a></p>

![](https://www.gmelectronic.com/data/product/1024_1024/pctdetail.912-067.1.jpg)
_By [GM Electronic](https://www.gmelectronic.com)_

> A low-dropout regulator (LDO regulator) is a DC linear voltage regulator that can regulate the output voltage even when the supply voltage is very close to the output voltage.[1]

- [LDO wiki](https://en.wikipedia.org/wiki/Low-dropout_regulator)

## Circuits

!!! warning "Not yet"
    This part will come soon!

## References
!!! info "Read for the geeks"
    Guerrilla electronics: https://lcamtuf.coredump.cx/electronics/
