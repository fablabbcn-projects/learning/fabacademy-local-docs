00:19:29	Magdalena Mojsiejuk:	on this nerdiest of days
00:19:39	Magdalena Mojsiejuk:	may the fourth be with you!!
00:19:50	benjaminscott:	hehe
00:20:47	David Prieto:	http://fabacademy.org/2020/labs/barcelona/students/david-prieto/assignments/week12/
00:21:06	Oscar Gonzalez:	http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/week12/
00:21:31	Oscar Gonzalez:	http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/week12/#a-frame
00:21:32	David Prieto:	may the forth be with you also, Magdalena!
00:23:04	FABACADEMY_BCN ZOOM:	http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/extras/week12/blender
00:50:10	Daniil Koshelyuk:	Is there a way to sort of record actions you do in Blender?
00:52:50	benjaminscott:	https://blender.stackexchange.com/questions/56005/how-to-create-macro-record-actions-while-modeling
00:53:01	benjaminscott:	https://www.blendernation.com/2012/11/12/tutorial-creating-macros/
01:05:46	David Prieto:	This is the TCP / UDP joke
01:06:03	David Prieto:	https://preview.redd.it/wixamyrqxra01.png?width=640&crop=smart&auto=webp&s=e563bf093bfbc3316d4db70ea1cb458ed79fa585
01:07:23	Daniil Koshelyuk:	UDP is fast, but can some times drop packages and has limited size. TCP is somehow smart - it keeps track of all parts thus can manage bigger messages but as Victor said - slower
01:10:17	Daniil Koshelyuk:	just a side note establishing TCP connection in code can be a major pain
01:15:53	vico:	phyphox.org
01:24:53	Daniil Koshelyuk:	If I don't unregister it will be accessible in other files?
02:15:11	David Prieto:	https://godotengine.org/
02:15:41	Daniil Koshelyuk:	Unity has some integration of blender - it can for example open directly blend files
