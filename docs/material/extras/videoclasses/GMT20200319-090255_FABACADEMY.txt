00:11:42	Eduardo Chamorro Martin:	https://www.instructables.com/id/DIY-Mini-CNC-Laser-Engraver/
00:14:13	Zoe:	Edu can we buy these things from another place than Amazon?
00:14:57	Eduardo Chamorro Martin:	usually all the local electronic shops sell this kind of components
00:15:16	Eduardo Chamorro Martin:	why not amazon? it doesn't ship to your locatlion
00:15:59	Eduardo Chamorro Martin:	I will repeat again- please switch ON YOUR WEBCAM
00:16:17	Eduardo Chamorro Martin:	this is VIDEOCONFERENCE no a phone call
00:16:36	Eduardo Chamorro Martin:	it is really annoying to give a class just seing black screens
00:16:37	FABACADEMY_BCN ZOOM:	https://www.youtube.com/feed/my_videos
00:17:00	FABACADEMY_BCN ZOOM:	https://www.youtube.com/channel/UCHFNP9EAtbghMo5SQgahCiQ/videos
00:17:10	LynnDika:	I really don’t have a camera, sorry Eduard.
00:17:30	Roger:	same, i'm on the desktop
00:18:47	antoine jaunard:	hello everybody, my webcam isn't working as well :(
00:30:50	Eduardo Chamorro Martin:	https://platformio.org/
00:31:47	Pablo Zuloaga Betancourt:	AH ok…perf…I was in this one   https://platform.io/ (wrong one)
00:31:48	David:	Be easy on us! xD
00:34:33	Hala Alzawaydeh:	are we recording?
00:34:47	natalia:	yes
00:35:00	natalia:	I mean, recording is blinking on left corner
00:35:03	Hala Alzawaydeh:	please share after class
00:35:05	Hala Alzawaydeh:	awesome
00:35:38	laurafreixasconde:	Could we have this presentation?
00:43:51	Tue:	http://fabacademy.org/2020/labs/barcelona/local/#material/week04/
00:43:55	Tue:	I think it's here
00:44:26	Tue:	How To Make A Computer (Guillem)
00:44:28	laurafreixasconde:	yes, thank you:)
00:45:09	FABACADEMY_BCN ZOOM:	we are recording
00:45:13	FABACADEMY_BCN ZOOM:	don’t worry
00:57:48	arman:	https://www.computerhistory.org/babbage/
01:03:33	FABACADEMY_BCN ZOOM:	http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/week08/#references-and-resources
01:16:33	arman:	http://www.asciitable.com/
01:17:39	benjaminscott:	http://sticksandstones.kstrom.com/appen.html
01:18:12	benjaminscott:	^ alphabet in binary
01:25:06	arman:	https://www.computerhistory.org/revolution/digital-logic/12/288/2220
01:25:52	arman:	https://youtu.be/d9SWNLZvA8g
01:26:14	arman:	From sand to IC
01:33:03	Jonathan Minchin:	Taiwan is the worlds Largest exporter of MicroChips
01:40:08	Daniil Koshelyuk:	I don't see a board
01:40:21	Anisa:	same
01:52:38	arman:	https://youtu.be/kRlSFm519Bo
01:58:47	Daniil Koshelyuk:	sorry guys I need to take a call we'll join right after
02:37:21	emaitee:	Does anyone have references/tutorials to understand the arduino chart we talked about?
02:44:25	Pablo Zuloaga Betancourt:	which chart Mit?
02:44:38	emaitee:	Compiling
02:48:26	Eduardo Chamorro Martin:	this is the link of all components we have in the lab 
02:48:26	Eduardo Chamorro Martin:	https://docs.google.com/a/iaac.net/spreadsheets/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html
02:48:33	Eduardo Chamorro Martin:	the fab lab inventory
02:48:46	Eduardo Chamorro Martin:	for searching the links of the datasheet of each component
02:51:22	Eduardo Chamorro Martin:	https://docs.google.com/spreadsheets/d/1sFTE5KQ79W1_I_wtV3kmd_NE7NsWmIP_n8XURuQYVAE/edit#gid=1779571110
02:53:12	guillem:	http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-data-sheet-40001995A.pdf
02:53:42	Andrea Bertran:	623 pages………………………………….
03:02:07	Daniil Koshelyuk:	why 64 if there are 8 pins?
03:02:32	Daniil Koshelyuk:	or 16*
03:04:09	David:	I/O registers can be more complex tan 1/0. I guess ^^U
03:05:51	LynnDika:	what time is the class tomorrow?
03:06:12	antoine jaunard:	4kb*16 ?
03:08:41	guillem:	Just in case we haven't share it yet. Here the presentation I used today https://speakerdeck.com/pral2a/how-to-make-a-computer
03:08:54	guillem:	It will be compiled with the current documentation after
03:09:47	laurafreixasconde:	thank you very much!
03:09:52	Daniil Koshelyuk:	Thank you!!
