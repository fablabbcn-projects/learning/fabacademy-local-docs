# From All Rights Retained to All Rights Relinquished

>When you develop a piece of hardware, software, or content, you are also getting some intellectual property rights. Sometimes these are automatic (like copyright and some sorts of design right), and sometimes they need to be applied for and registered (like patents, and other types of design right). **These rights allow you to stop other people from doing various things with your design** – usually around copying it, modifying it or distributing it. **A licence is the legal permission which gives people the right to do some of these things**, subject to conditions you impose. Traditional licences make payment a condition of the licence, and also tend to impose restrictions like limiting use to one computer, or to one purpose (for example, “home and student”). By contrast, open licences guarantee that you can take the design, code, data or content, use it and modify it and redistribute it.

> Some are very simple, and let you do all of these things, imposing only very lightweight conditions. This could be that if you redistribute the code you only have to acknowledge the copyright holder, and provide some disclaimer text. These are called “permissive” licences. More complex licences (so-called “reciprocal” or “copyleft” licences) required that if you do modify the code, design or content, and redistribute it, you have both to redistribute it under the same licences terms (guaranteeing the same freedom to your recipients), and also, in the case of software and hardware, provide a copy of the design material (source code, in the case of software), incorporating your modifications. “Modifications” is interpreted very widely: it can mean that if you incorporate a small amount of material in a copyleft-licensed design into a completely different design, you can’t distribute the resulting design or product made from it without making the whole of the design material available with the source, and all licensed under the same copyleft licences.

via [Moorcrofts](https://moorcrofts.com/cern-launches-open-hardware-licence-v2/)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Software-license-classification-mark-webbink.svg/400px-Software-license-classification-mark-webbink.svg.png)

!!! Note
    [**Comparison of different types of licenses**](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses)

## From _All Rights Retained_ to _All Rights Relinquished_

![](https://i.imgur.com/lxpxU5z.jpg)
 
### Patent

A patent is a form of intellectual property that gives its owner the **legal right to exclude others from making, using, selling, and importing an invention for a limited period of years**, in exchange for publishing an enabling public disclosure of the invention. In most countries patent rights fall under **civil law** and the patent holder needs to sue someone infringing the patent in order to enforce his or her rights. In some industries patents are an essential form of competitive advantage; in others they are irrelevant.

A patent, being an exclusionary right, does not necessarily give the patent owner the right to exploit the invention subject to the patent. For example, many inventions are improvements of prior inventions that may still be covered by someone else's patent. If an inventor obtains a patent on improvements to an existing invention which is still under patent, they can only legally use the improved invention if the patent holder of the original invention gives permission, which they may refuse. 

### All Rights Reserved

Copyright grants to creators a bundle of exclusive rights over their creative works, which generally include, at a minimum, the right to reproduce, distribute, display, and make adaptations. The phrase “All Rights Reserved” is often used by owners to indicate that they reserve all of the rights granted to them under the law. When copyright expires, the work enters the public domain, and the rights holder can no longer stop others from engaging in those activities under copyright, with the exception of moral rights reserved to creators in some jurisdictions. 

### Public Domain

> No permission or license is required for a work truly in the **public domain**, such as one with an expired copyright; such a work may be copied at will. Public domain equivalent licenses exist because some legal jurisdictions[which?] do not provide for authors to voluntarily place their work in the public domain, but do allow them to grant arbitrarily broad rights in the work to the public.
> 
> The licensing process also allows authors, particularly software authors, the opportunity to explicitly deny any implied warranty that might give someone a basis for legal action against them. While there is no universally agreed-upon license, several licenses aim to grant the same rights that would apply to a work in the public domain.

**Licenses Examples**

- "Do What The Fuck You Want To Public License" ([WTFPL](https://en.wikipedia.org/wiki/WTFPL))
- [Creative Commons Zero](https://en.wikipedia.org/wiki/CC0)
- [Unlicense](https://en.wikipedia.org/wiki/Unlicense)

### Copyleft Licenses

#### GNU Licenses: GPL and LGPL

[Free Software Foundation](https://www.fsf.org/): Free as in freedom

> Nobody should be restricted by the software they use. There are four freedoms that every user should have:
> - the freedom to **use** the software for any purpose,
> - the freedom to **change** the software to suit your needs,
> - the freedom to **share the software** with your friends and neighbors, and
> - the freedom to **share the changes** you make.
> 
> When a program offers users all of these freedoms, we call it **free software**.

[Quick Guide to GNU GPL](https://www.gnu.org/licenses/quick-guide-gplv3.html)

[Why and why not LGPL](https://www.gnu.org/licenses/why-not-lgpl.en.html)

[Non-commercial?](http://www.ladyada.net/library/openhardware/license.html)

**GPL**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/GPLv3_Logo.svg/220px-GPLv3_Logo.svg.png)

Free software license by the Free Sofwware Foundation, which guarantees end users the freedom to run, study, share and modify the software. 

**Examples** of GNU GPL Project:

 - Linux Kernel
 - GCC

!!! Note "**100% of Supercomputers in the world** are running Linux"
    :clap: https://itsfoss.com/linux-runs-top-supercomputers/

**LGPL**

![](https://duckduckgo.com/i/8d088450.png)

The GNU Lesser General Public License is a free software license published by the Free Software Foundation. The license allows developers and companies to use and integrate software released under the LGPL into their own software without being required by the terms of a strong copyleft license to release the source code of their own components.

 **When to use it?** when there is no advantage for the software released to use this library, i.e. when the functionalities it gives are already available through other libraries.
 
#### Mozilla Public License (MPL)

It is a weak copyleft license, characterized as a middle ground between permissive free software licenses and the GNU General Public License (GPL), that seeks to balance the concerns of proprietary and open source developers.

**Examples**

- LibreOffice
 
### Permissive Licenses

Unlike both copyleft licenses and copyright law, permissive free-software licenses do not control the license terms that a **derivative work** falls under. Copyleft says that anyone who redistributes the software, with or without changes, must pass along the freedom to further copy and change it.

#### BSD License

BSD licenses are a family of permissive free software licenses, imposing minimal restrictions on the use and distribution of covered software. This is in contrast to copyleft licenses, which have share-alike requirements. The original BSD license was used for its namesake, the Berkeley Software Distribution (BSD), a Unix-like operating system. The original version has since been revised, and its descendants are referred to as modified BSD licenses. 

#### MIT License

Permissive Free Sofware license that permits reuse within proprietary software provided that all copies of the licensed software include a copy of the MIT License terms and the copyright notice. The MIT license is also compatible with many copyleft licenses, such as the GNU General Public License (GPL); MIT licensed software can be integrated into GPL software, but not the other way around.

!!! Note "**Patent license?**"
    Like the BSD license the MIT license does not include an express patent license. Both the BSD and the MIT licenses were drafted before the patentability of software was generally recognized under US law.

### Comparison

**Copyleft vs Permissive:**

> A major difference between the set of **permissive** and **copyleft free-software** licenses is that when the software is being redistributed (either modified or unmodified), permissive licenses do not force the redistributor to open the modified source code. Permissive licenses do not try to guarantee that future generations of the software will remain free and publicly available, in contrast to licenses which have reciprocity requirements which try to enforce this.
 
**Public Domain vs. permissive:**

> **Public domain** refers to works that have become widely shared and distributed under permission, and work that was deliberately put into the public domain. Permissive licenses are not actually equivalent to releasing a work into the public domain.
> 
> Permissive licenses often do stipulate some limited requirements, such as that the original authors must be credited (attribution). If a work is truly in the public domain, this is usually not legally required, but a United States copyright registration requires disclosing material that has been previously published, and attribution may still be considered an ethical requirement in academia. 

## Creative Commons

[Find them here](https://creativecommons.org/share-your-work/licensing-types-examples)

[![](https://i.imgur.com/GNXSa5o.png)](http://creativecommons.org.au/content/licensing-flowchart.pdf)

## Hardware

> Open source hardware is hardware **whose design is made publicly available** so that anyone can **study, modify, distribute, make, and sell the design or hardware based on that design**. The hardware's source, the design from which it is made, is available in the preferred format for making modifications to it.
> 
> Ideally, open source hardware uses readily-available components and materials, standard processes, open infrastructure, unrestricted content, and open-source design tools to maximize the ability of individuals to make and use hardware. Open source hardware gives people the freedom to control their technology while sharing knowledge and encouraging commerce through the open exchange of designs. 

[Open Source Hardware](https://en.wikipedia.org/wiki/Open-source_hardware)

[Free Hardware designs](https://www.gnu.org/philosophy/free-hardware-designs.html)

**Example**:

- [Reprap Project](https://www.reprap.org/wiki/RepRap)

**Counter-example**

- [Makerbot](https://www.cnet.com/news/pulling-back-from-open-source-hardware-makerbot-angers-some-adherents/)

### CERN OHL

[CERN Open Hardware License Project](https://ohwr.org/project/cernohl/wikis/home)

**Examples:**

https://ohwr.org/project/cernohl/wikis/CernOhlProjects

## Other Resources

- [Open Source Initiative - _The Open source definition_](https://opensource.org/docs/osd)
- [Open Source Hardware Definition (OSHW)](https://www.oshwa.org/definition/)
- https://choosealicense.com/licenses/

## Case Studies

[24 case studies in Made with Creative Commons](https://medium.com/@madewithcc)

[When CERN made the World Wide Web technology royalty free basis](https://home.cern/news/news/computing/twenty-years-free-open-web)

[The Arduino case](https://hackaday.com/2017/10/05/who-owns-arduino/
)

[Audicity Telemetry Debate](https://hackaday.com/2021/05/17/telemetry-debate-rocks-audacity-community-in-open-source-dustup/#more-476353)

[WRT54G History: The Router That Accidentally Went Open Source](https://tedium.co/2021/01/13/linksys-wrt54g-router-history/)

[Elastic Search Case](https://www.elastic.co/blog/why-license-change-AWS)

[Multimeters without a country: Flukes broad trademark bans yellow multimeter imports](https://hackaday.com/2014/03/19/multimeters-without-a-country-flukes-broad-trademark-bans-yellow-multimeter-imports/)


[The $12 “Gongkai” Phone](https://www.bunniestudios.com/blog/?page_id=3107)

[Kinect Hacking Case](https://blog.adafruit.com/2010/11/04/the-open-kinect-project-the-ok-prize-get-1000-bounty-for-kinect-for-xbox-360-open-source-drivers/)

[John Deere and the right-to-repair a tractor](https://hackaday.com/2020/03/09/john-deere-and-nebraskas-right-to-repair-the-aftermath-of-a-failed-piece-of-legislation/)

## Randoms Thoughts

[Protocols vs Platforms](https://knightcolumbia.org/content/protocols-not-platforms-a-technological-approach-to-free-speech)

[Open Source Synth Creator Interview](https://www.synthtopia.com/content/2018/01/22/open-source-synthesis-behind-the-scenes-with-vcv-rack-creator-andrew-belt/)

[On #Cryptoart](https://memoakten.medium.com/the-unreasonable-ecological-cost-of-cryptoart-2221d3eb2053)

[Brute-Forced Copyrighting: Liberating all the melodies](https://hackaday.com/2020/03/05/brute-forced-copyrighting-liberating-all-the-melodies/)
