/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

#include <WiFi.h>
#include <PubSubClient.h>


// Replace the next variables with your SSID/Password combination
const char* ssid = "Iaac-Wifi";
const char* password = "EnterIaac22@";


// Add your MQTT Broker IP address, example:
const char* mqtt_server = "164.92.158.130";
const char* mqttClientName = "Robolab";


WiFiClient espClient;
PubSubClient mqttClient(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;


int leftsensor = 0;
int rightsensor = 0;

// LED Pin
const int ledPin = 13;

void setup() {
  Serial.begin(115200);
 delay(500);
 // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");


  delay(500);
  mqttClient.setServer(mqtt_server, 1883);
  mqttClient.setCallback(callback);
  pinMode(13, OUTPUT);
  Serial.println("Finish Configuration - Loop Start");
}



void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageLED;
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageLED += (char)message[i];
  }
  Serial.println();
  if (String(topic) == "mdef1/led") {        // Feel free to add more if statements to control more GPIOs with MQTT
    Serial.print("Changing output to ");        // If a message is received on the topic esp32/output, you check if the message is either "on" or "off". 
    if(messageLED == "on"){                    // Changes the output state according to the message
      Serial.println("on");
      digitalWrite(ledPin, HIGH);
    }
    else if(messageLED == "off"){
      Serial.println("off");
      digitalWrite(ledPin, LOW);
    }
  }
}

void mqttConnect() {
  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect(mqttClientName)) {
      Serial.println("connected");
      // Subscribe
      mqttClient.subscribe("mdef1/led");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}




void loop() {
  if (!mqttClient.connected()) {
    mqttConnect();
  }
   mqttClient.loop();

  long now = millis();
  if (now - lastMsg > 1000) {                                               // Do this every 5000ms = 5 sec
    lastMsg = now;
            
            int leftsensor = analogRead(A2); 
            Serial.print("LeftSensor");
            Serial.println(leftsensor);
            if (leftsensor >= 2300){
                  char LeftString[8];                                             // Convert the value to a char array
                  sprintf(LeftString, "%u", leftsensor);
                  //Serial.print("LeftSensor");
                  //Serial.println(leftsensor);
                  mqttClient.publish("MDEF1/ouput","sensor to high");
                  }else{
                  mqttClient.publish("MDEF1/ouput","sensor to low");
                  }
  }
 
  delay(10);
}
