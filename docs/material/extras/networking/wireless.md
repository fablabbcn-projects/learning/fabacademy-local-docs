Networking and Communication
==================

## Wireless communication


![](https://media.giphy.com/media/xUA7aVzqJ05B90NHC8/giphy.gif)

![](http://sevenhorizons.pbworks.com/f/1438017529/frequency-spectrum.png)

![](https://www.rfpage.com/wp-content/uploads/2016/07/RF-spectrum-range.jpg)

![](https://qph.fs.quoracdn.net/main-qimg-28a22448b31645206dd5d878140e5514.webp)

Range vs Power

![](https://modernsurvivalblog.com/wp-content/uploads/2011/03/radiation-inverse-square-law.jpg)

[Range on geography](http://www.exeterars.co.uk/hamnet-meshnet/HilltopStablesToG7NBU_partA.png)

**Signal Penetration**

![](http://demo.screenteam.at/moeller_en//upload/tip/textbilder//Tipps/Komfort/funk.jpg)

![](https://radiojove.gsfc.nasa.gov/education/educ/radio/tran-rec/exerc/images/propo2.gif)

![](https://radiojove.gsfc.nasa.gov/education/educ/radio/tran-rec/exerc/images/propo1.gif)

**3G-4G**

![](https://talkingpointz.com/wp-content/uploads/2019/04/Frequencies-for-5G-1024x463-Annotated-600x271.png)

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIUcMP54IZ2Z-nSH5cdqmz-yIPE8v4esOchCRtdC4KsaPBgnD8&s)

## Antennas

![](https://oscarliang.com/ctt/uploads/2017/05/fpv-antenna-anatomy-active-element-ground-plane-coaxial-cable-connector.jpg)

![](https://oscarliang.com/ctt/uploads/2017/05/cloverleaf-antenna-plastic-protective-housing-case.jpg)

* Radiation pattern

![](https://i.imgur.com/R1Kxurv.png)

![](https://www.4-wheeling-in-western-australia.com/images/antenna3-6-9dB.jpg)

Antenna based on the frecuency

![](http://eduardochamorro.github.io/beansreels/workshops/beandrone/spw58-2_1359095259352.jpg)

[Video of wavelenght penetration comparison](https://www.youtube.com/watch?v=LltDtKs2sQ4)

#### Monopole

Monopole antenna is the simplest form of antenna, which is basically just a piece of un-shielded wire. It’s very common in radio receivers because they are cheap and easy to repair. However they are not as effective as Dipole antennas.

![](https://oscarliang.com/ctt/uploads/2017/05/monopole-antenna-mono-pole-fpv-vtx-vrx-rx-tx.jpg)

**Calculation of wire length for antenna**

If the antenna is a quarter wave monopole antenna, then its length should be a quarter of the signal wavelength.

				Length (m) = c / frequency / 4

The more precise wire length is, the better signal you should get ideally. If you are using Coaxial Cable, it doesn’t matter how long the cable is with shielding, all that matter is the exposed wire (the part that is without the shielding). Alternatively if you are using a wire like copper wire, just cut it to the calculated length and you have a monopole antenna :)

![](https://oscarliang.com/ctt/uploads/2015/02/dipole-antenna-fpv-video-transmitter-length-5-8ghz.jpg)

#### Dipole antenna

Dipole antennas has a simple design. It’s basically just a monopole antenna with a ground sleeve at under the active element. The ground sleeve can supposedly boost the performance considerably

![](https://oscarliang.com/ctt/uploads/2015/02/dipole-antenna-inside-fpv-video-transmitter-5-8ghz.jpg)

#### Half Wave dipole antenna

A half-wave dipole antenna consists of two quarter-wavelength conductors placed end to end for a total length of approximately L = λ/2

![](http://electronics-diy.com/schematics/537/dipole.jpg)

#### Helical antenna

Helical antennas are spring-shaped, directional circular polarized antennas. The number of turns of coil determines the gain of the antenna.

![](https://oscarliang.com/ctt/uploads/2016/07/ProDrone-FullSet-Antennah-3.5-turn-helical-diversity-rx.jpg)

#### Patch antenna

Patch antennas are also directional, and can be found in linear and circular polarization. They generally have less directionality than Helical, and smaller foot-print.

![](http://www.mcv-microwave.com/images/large/patch-antenna.jpg)

#### Make your own antennas

[Antenna step by step build tutorial](https://oscarliang.com/make-diy-cloverleaf-antenna/)

[BOOSTING Range with a CAN](https://makezine.com/2013/03/26/how-to-improve-your-wifi-signal-using-a-soda-can-in-6-steps/)

[LONG RANGE ON DIY](https://jacobsalmela.com/2013/09/07/wi-fi-cantenna-2-4ghz-how-to-make-a-long-range-wi-fi-antenna/)

["PRINGELS"CAN ANTENNA](https://www.makeuseof.com/tag/how-to-make-a-wifi-antenna-out-of-a-pringles-can-nb/)

## Range-Power-Dbi Gain

Power - range is exponential
![](https://www.researchgate.net/profile/Tahmina_Ajmal/publication/235644999/figure/fig2/AS:299794590388230@1448488008555/Power-at-the-antenna-output-versus-distance-from-the-transmitter-for-a-1kO-load.png)

You often hear “higher gain antenna has longer range”, but don’t be fooled! Antennas doesn’t amplify radio signals, but rather just changes the radiation pattern, which is a trade-off between coverage and range. Antenna gain is a reflection of the maximum range you can get.

![](https://oscarliang.com/ctt/uploads/2013/10/antenna-gain-8db.jpg)

We should avoid blindly increasing the power of video transmitter to achieve longer range, simply because it might not be legal, and it will consume more power and generate more heat. Always start with upgrading your antennas when trying to improve range :)

**What is dB?**

dB means decibels, we use this unit to measure the level of sound, but in radio we also use dB to measure signal strength.

dB is on a non-linear, logarithmic scale. For example, by increasing dB by 3, the signal strength doubles. (note: signal strength, not range)

Apart from signal strength, both antenna gain and VTX power can be expressed in dB too.

Why use dB you might wonder. Well, for some applications, dB is much easier to work with, because the math is simpler. You just add or subtract numbers, there is no multiplication or division.

Radio TX datasheet examples
![](https://oscarliang.com/ctt/uploads/2019/03/lollipop-v2-fpv-antenna-specifications-datasheet-1024x659.jpg)

The best thing to remember is, every 3dB increase, will double the signal strength, but an increase of 6dB is required to double the range. From this you can ascertain that replacing a 200mW VTX with a 400mW will NOT double your range, but increase it by approximately 50%. It is also worth noting that as the power goes up, the rate at which dB increases goes down, further diminishing returns.

**Caculation of FREE SPACE PATH loss**

>Distance = 10^((FSPL-LM-32.44)-20*log10(f))/20)

Where:

- FSPL (Free Space Path Loss) = TX Antenna Gain + RX Antenna Gain + TX Power – RX Sensitivity
- LM = Link Margin
- f = frequency in MHz (mega hertz)

[RANGE PATH LOSS CALCULATOR](https://oscarliang.com/js/fpvrange.html)

**Understand Antenna Gain**

Antenna gain is the measure of antenna power in decibel (dB), which is equal to 10*log(Pout/Pin).

Don’t worry about it, just remember that the higher the gain, the more directional an antenna is – more range, narrower beam width.

Every 6dB increase in antenna gain, should in theory, doubles the range. However, as mentioned, directional antennas are not amplifiers. It’s only focusing all the energy into a narrower beam.

![](https://oscarliang.com/ctt/uploads/2017/05/fpv-antenna-light-bulb-torch-flash-light-analogy.jpg)

A radiation pattern of the hypothetical isotropic antenna at 0db gain. It’s a nearly perfect sphere in both vertical and horizontal axis.
![](https://oscarliang.com/ctt/uploads/2013/10/antenna-gain-0db.jpg)

This is a standard omni-directional 3dB rubber duck antenna. Notice it has significant signal loss on the top and bottom (90/270 degree).
![](https://oscarliang.com/ctt/uploads/2013/10/antenna-gain-3db.jpg)
![](https://oscarliang.com/ctt/uploads/2017/05/pagoda-antenna-fpv-radiation-pattern-3d.jpg)

And here we have a directional antenna of 8dB (a patch antenna). As you can see, majority of the signal are focus on one direction to the right (0 degree) just as we expected from a directional antenna. But we are not getting much signal on the other direction (180 degree).

![](https://oscarliang.com/ctt/uploads/2013/10/antenna-gain-8db.jpg)

### Frecuencies

Frecuency spacing

![](https://radiofreeq.files.wordpress.com/2015/01/survivalist_prepper_channel_ssb_cb_freeband.png?w=500&zoom=2)

Professional HAM radio datachannel list

![](https://radiofreeq.files.wordpress.com/2013/08/modified_export_cb_radio_channel_frequency.jpg)

**AM-FM-SIGNAL**

![](https://upload.wikimedia.org/wikipedia/commons/a/a4/Amfm3-en-de.gif)

![](https://upload.wikimedia.org/wikipedia/commons/d/df/Frequency-modulation.png)

**Radio jamming**

![](https://i2.wp.com/krazytech.com/wp-content/uploads/Jamming-and-anti-Jamming-Techniques.jpg?fit=624%2C416&ssl=1&resize=1280%2C720)

Jamming gun
![](https://p.globalsources.com/IMAGES/PDT/BIG/047/B1144245047.jpg)

### Antenna Connectors

SMA, RP-SMA, MMCX and U.FL are the three types of connectors.

Some people find it difficult to get their heads around the different types. Even manufacturers could sometimes get it wrong. When buying FPV equipment with a SMA connector, we urge you to double check if the product description matches product image. If not, confirm with the seller to avoid surprises.
Differences of RP-SMA and SMA Antenna Connectors

SMA and RP-SMA connectors are the original connectors used in FPV equipment. They are still very common thanks to their robustness and versatility. However they are also fairly bulky and heavy.

Another advantage of SMA is the 500+ mating cycles, which a lot more than MMCX and U.FL. All the receivers I’ve come across use SMA or RP-SMA and rarely use other connectors, because weight and space is not really an issue on the receiving side.

SMA stands for Sub-Miniature Version A. These are coaxial RF connectors developed in the 1960s.

RP-SMA stands for Reverse Polarity SMA). It is a variation of the SMA connector which reverses the gender of the interface.

There is no difference with video/signal quality between these connectors, but you’ll read further down why we even have these 2 different types. Generally speaking, SMA appears to be more popular choice of connector especially in the mini quad (racing drone) industry.

Here is a comparison table of the SMA antenna and RP-SMA antenna connectors.

**SMA  MALE-FEMALE**
![](https://oscarliang.com/ctt/uploads/2014/01/sma-jack.jpg)
![](https://oscarliang.com/ctt/uploads/2014/01/sma-plug.jpg)

**SMA  MALE-FEMALE**
![](https://oscarliang.com/ctt/uploads/2014/01/rp-sma-jack.jpg)
![](https://oscarliang.com/ctt/uploads/2014/01/rp-sma-plug.jpg)

**U.FL Connectors**

As known as IPEX connectors sometimes, U.FL has been a popular connector choice in many small size video transmitters and antennas. They are also very popular in radio receivers due to their compact sizes.

The connector has no thread, it’s attached by popping them together. They are a lot more fragile than standard SMA/RPSMA connectors and they have a very limited mating cycle, only about 30+, according the datasheet.

![](https://oscarliang.com/ctt/uploads/2016/09/u.fl-ipex-connector-ufl-sma-size-comparison.jpg)

**MMCX Connectors**

![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSWlFIAmxfIYyuesnXiz1kACvGGpUwHJbTd1mH3_9WfD5NdyZ0J&usqp=CAU)

SMA is too big and heavy, U.FL is too fragile, and the MMCX is the great balance of the two!

MMCX connectors are slightly smaller and lighter than SMA connectors, but much tougher than U.FL. Rated for 100+ mating cycles, more and more VTX and antennas are picking up this connector

![](https://oscarliang.com/ctt/uploads/2017/11/AKK-VTX-video-transmitter-FX2-MMCX-antenna-connector-1024x768.jpg)

### Polarisation

[Article on polarization](https://oscarliang.com/linear-circular-polarized-antenna-fpv/)


## NRF24-Communication

**Caution working voltaje of : 3 ~ 3.6V Max**

The NRF24L01 integrates an RF transceiver (transmitter + receiver) at a frequency between 2.4GHz to 2.5GHz, a free band for free use. The transmission speed is configurable between 250 Kbps, 1Mbps, and 2 Mbps and allows simultaneous connection with up to 6 devices.

The NRF24L01 also incorporates the necessary logic to make communication robust, such as correcting errors and forwarding data if necessary, freeing the processor of this task. The control of the module is done through the SPI bus, so it is easy to control it from a processor such as Arduino.

The band of frequency is of 2400 to 2525 MHz, being able to choose between 125 channels spaced at the rate of 1MHz. It is recommended to use the frequencies from 2501 to 2525 MHz to avoid interference with Wi-Fi networks.

But before starting with the example, you have to know that the NRF24L01 module is a transceiver and not a receiver transmitter.

A receiving transmitting equipment can send radio messages and receive them simultaneously because both circuits, although very similar, are isolated from each other and can be operated independently.

- Wiring

![](https://www.luisllamas.es/wp-content/uploads/2016/12/arduino-NRF24L01-2.4ghz-conexion.png)

![](https://www.luisllamas.es/wp-content/uploads/2016/12/arduino-NRF24L01-2.4ghz-esquema.png)

The first thing you have to do is download the latest version of the RF24 library and import it into your Arduino IDE, for this in the menu bar go to Sketch> Import Library> Add Library

The following example shows the sending of a text string from an Arduino sender to an Arduino receiver, which upon receiving the text shows it by serial port.

**Sender Code**

```
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);

// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0xE8E8F0F0E1LL;

char data[16]="Hola mundo" ;

void setup(void)
{
	 radio.begin();
	 radio.openWritingPipe(pipe);
}

void loop(void)
{
	 radio.write(data, sizeof data);
	 delay(1000);
}
```

 **Receiver**

```
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);

// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0xE8E8F0F0E1LL;

char data[16];

void setup(void)
{
	 Serial.begin(9600);
	 radio.begin();
	 radio.openReadingPipe(1,pipe);
	 radio.startListening();
}

void loop(void)
{
	 if (radio.available())
	 {
			int done = radio.read(data, sizeof data);
			Serial.println(data);
	 }
}
```

!!! Note "More tutorials"
	- [Tutorial 1](https://www.luisllamas.es/comunicacion-inalambrica-a-2-4ghz-con-arduino-y-nrf24l01/)
	- [Tutorial 2](https://naylampmechatronics.com/blog/16_Tutorial-b%C3%A1sico-NRF24L01-con-Arduino.html)
	- [Tutorial Duplex](https://www.prometec.net/duplex-nrf2401/)
	- [Networking](https://howtomechatronics.com/tutorials/arduino/how-to-build-an-arduino-wireless-network-with-multiple-nrf24l01-modules/)

## HC12-Communication

The HC-12 is a half-duplex wireless serial communication module with 100 channels in the 433.4-473.0 MHz range that is capable of transmitting up to 1 km.

The HC-12 is a half-duplex 20 dBm (100 mW) transmitter paired with a receiver that has -117 dBm (2×10-15 W) sensitivity at 5000 bps.

Paired with an external antenna, these transceivers are capable of communicating up to and possibly slightly beyond 1 km in the open and are more than adequate for providing coverage throughout a typical house.

- Setup & Configuration [AT Serial Commands](https://quadmeup.com/hc-12-433mhz-wireless-serial-communication-module-configuration/)

- Wiring

![](https://howtomechatronics.com/wp-content/uploads/2017/07/Arduino-and-HC-12-Circuit-Schematic.png)

![](https://robotedu.my/image/robotedu/image/data/all_product_images/product-1166/arduino-hc12-433-si4463-wireless-serial-module-remote-1000m-robotedu-1801-11-robotedu%409.jpg)

**Pro´s**

 - Control and communication over Serial. Able to use simple software serial protocols.
- A receiving transmitting equipment can send radio messages and receive them simultaneously because both circuits, although very similar, are isolated from each other and can be operated independently.

**Con´s**

	- Not able to resolve lost package data
	 - Usually expensive setup

!!! Note "More and more"
	- [More examples](https://howtomechatronics.com/tutorials/arduino/arduino-and-hc-12-long-range-wireless-communication-module/)
	- [Example](https://www.allaboutcircuits.com/projects/understanding-and-implementing-the-hc-12-wireless-transceiver-module/)

**Example Code Send-Receive**

```
/*  HC12 Send/Receive Example Program 1
		By Mark J. Hughes


 Connect HC12 "RXD" pin to Arduino Digital Pin 4
		Connect HC12 "TXD" pin to Arduino Digital Pin 5
		Connect HC12 "Set" pin to Arduino Digital Pin 6

Transceivers must be at least several meters apart to work.

 */

#include <SoftwareSerial.h>

const byte HC12RxdPin = 4;                  // Recieve Pin on HC12
const byte HC12TxdPin = 5;                  // Transmit Pin on HC12

SoftwareSerial HC12(HC12TxdPin,HC12RxdPin); // Create Software Serial Port

void setup() {
	Serial.begin(9600);                       // Open serial port to computer
	HC12.begin(9600);                         // Open serial port to HC12
}

void loop() {
	if(HC12.available()){                     // If Arduino's HC12 rx buffer has data
		Serial.write(HC12.read());              // Send the data to the computer
		}
	if(Serial.available()){                   // If Arduino's computer rx buffer has data
		HC12.write(Serial.read());              // Send that data to serial
	}
}
```

## BlueTooth Communication - HC08

![](https://i2.wp.com/m.eet.com/media/1111248/wireless_audio_distribution_fig1t.jpg)

Both classic Bluetooth and low-energy Bluetooth apply the Adaptive Frequency Hopping (AFH) feature that detects interference from. For example, a WLAN device 802.11 b, g, n that transmits nearby, if such interference is detected, the channel is automatically placed in the blacklist. In order to handle the temporary interference, an implemented scheme retries the channels of the blacklist and if the interference has ceased the channel can be used. AFH prevents Bluetooth from interfering with other nearby wireless technologies.

- *The hardware that makes up the Bluetooth device is made up of two parts:*

		- a radio device, responsible for modulating and transmitting the signal.
		- a digital controller, consisting of a CPU, a digital signal processor (DSP - Digital Signal Processor) called Link Controller (or Link Controller) and interfaces with the host device.*

**The LC or Link Controller** is responsible for the processing of the baseband and the handling of the ARQ and FEC protocols of the physical layer; In addition, it handles both asynchronous and synchronous transfer functions, audio coding and data encryption.

Low-energy Bluetooth, also referred to as Bluetooth LE, Bluetooth ULP (Ultra Low Power) and Bluetooth Smart, is a new digital radio (wireless) interoperable technology for small devices developed by Bluetooth.

![](https://i0.wp.com/www.prometec.net/wp-content/uploads/2014/11/BT-masterslave.jpg)

**Master-Slaves**

BlueTooth devices can act as Masters or as Slaves. The difference is that a BlueTooth Slave can only connect to a master and nobody else, instead a BlueTooth master, can connect to several Slaves or allow them to connect and receive and request information from all of them, arbitrating information transfers (up to a maximum of 7 Slaves).

- Wiring:

![](https://ardudron.files.wordpress.com/2016/04/arduino_ble_hc08.jpg?w=1088)
![](https://http2.mlstatic.com/bluetooth-hc-08-hc08-40-ble-arduino-control-inalambrico-D_NQ_NP_664225-MCO25395542038_022017-F.jpg)


## WiFi

Before moving into the specifics of the code, it is important to know **who is who**.

_From the ESP8266WiFi library docs_: devices that connect to Wi-Fi network are called stations (**STA**). Connection to Wi-Fi is provided by an access point (**AP**), that acts as a hub for one or more stations. The access point on the other end is connected to a **wired network**. An access point is usually integrated with a router to provide access from Wi-Fi network to the internet. Each access point is recognized by a **SSID (Service Set IDentifier)**, that essentially is the name of network you select when connecting a device (station) to the Wi-Fi.

In the case of the ESP8266, or ESP32, they can work as:

- **Station**: station (STA) mode is used to get ESP module connected to a Wi-Fi network established by an access point.

![](https://i.imgur.com/oqbVBnz.png)

- **Soft-AP**: An access point (AP) is a device that provides access to Wi-Fi network to other devices (stations) and connects them further to a wired network. ESP8266 can provide similar functionality except it does not have interface to a wired network. Such mode of operation is called soft access point (soft-AP)

![](https://i.imgur.com/TKMRdin.png)

- **Both station and soft-AP**: Another handy application of soft-AP mode is to set up mesh networks. ESP can operate in both soft-AP and Station mode so it can act as a node of a mesh network.

![](https://i.imgur.com/HVO5CfA.png)

- **Client**: in this mode, the module can access services provided by servers in order to send, receive and process data. In this case, we can request a site from a server, and do something with it.

![](https://i.imgur.com/faX0G1g.png)

- **Server**: in this mode, the module can provide functionality to other devices, or simply serve a website:

![](https://i.imgur.com/XefrDDd.png)

### WiFi Broadcast

[Wifibroadcast](https://github.com/rodizio1/EZ-WifiBroadcast/wiki) is a project aimed at the live transmission of HD video (and other) data using wifi radios. One prominent use case is to transmit camera images.
In contrast to a normal wifi connection wifibroadcast tries to mimic the advantageous properties of an analog link (like graceful signal degradation, unidirectional data flow, no association between devices).

![](https://befinitiv.files.wordpress.com/2015/04/dataflow.png)

Wifibroadcast puts the wifi cards into monitor mode. This mode allows to send and receive arbitrary packets without association. Additionally, it is also possible to receive erroneous frames (where the checksum does not match). This way a true unidirectional connection is established which mimics the advantageous properties of an analog link.

### Extending range

ESP32 OVER 10KM JUST WITH ANTENNA MODIFICATIONS!

<iframe width="560" height="315" src="https://www.youtube.com/embed/yCLb2eItDyE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Expressif article](https://www.espressif.com/en/media_overview/news/esp32%E2%80%99s-wi-fi-range-extended-10-km-directional-antenna)
