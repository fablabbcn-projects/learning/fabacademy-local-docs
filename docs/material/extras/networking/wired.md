Wired Communication
==================

## Introduction

There are quite a few wired protocols in which microcontrollers can talk to one another. Before digging into them, it is important to understand the differences between their characteristics.

**Serial vs. Parallel**

**Parallel interfaces** transfer multiple bits at the same time. They usually require buses of data - transmitting across eight, sixteen, or more wires. This means that waves of data can be sent, with high speeds, but with a lot of wires.

![](https://cdn.sparkfun.com/r/700-700/assets/c/a/c/3/a/50e1cca6ce395fbc27000000.png)
_Image source: Sparkfun_

!!! Note "Have you seen this?"

	Many printers use parallel communication before the advent of serial communication!
	![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Mini-Centronics_36_pin_with_Micro-Centronics_36_pin.jpg/440px-Mini-Centronics_36_pin_with_Micro-Centronics_36_pin.jpg)

**Serial interfaces** stream their data with a reference signal, one single bit at a time. These interfaces can operate on as little as one wire, usually never more than four:

![](https://cdn.sparkfun.com/r/700-700/assets/e/5/4/2/a/50e1ccf1ce395f962b000000.png)

_Image source: Sparkfun_

**Synchronous vs. Asynchronous**

"Asynchronous" (not synchronous) means that data is transferred without support from an external clock signal. This transmission method is perfect for minimizing the required wires and I/O pins, but it does mean we need to put some extra effort into reliably transferring and receiving data.

![](https://i.imgur.com/mpK1muw.png)

_Image source: Sparkfun_

"Synchronous" data interface always pairs its data line(s) with a clock signal, so all devices on a synchronous bus share a common clock. This makes for a more straightforward, often faster transfer, but it also requires at least one extra wire between communicating devices.

![](https://i.imgur.com/AWi49fR.png)

_Image source: Sparkfun_

!!! Note "Serial communication"
	[Learn more](https://learn.sparkfun.com/tutorials/serial-communication/all)

In this section, we will focus on **serial communication**, making distinction between *synchronous* and *asynchronous*.

## Asynchronous communication

We use the asynchronous communication when data can be sent without timing constraints as well as _oftenly_ less speed, with the benefit of using one less wire, one less port on each side. The most well known asynchronous communication protocol is the RX/TX or simply Serial protocol (because it's the most important).

### RX/TX

By RX/TX we know the most common way of serial communication. It requires only two wires, appart from a common ground:

![](https://cdn.sparkfun.com/r/700-700/assets/2/5/c/4/5/50e1ce8bce395fb62b000000.png)

Data is sent asynchronously, so it means that both ends of the communication need to agree on some topics, being the speed the most important one (known as baud rate).

We also have to agree on how long the data strings are. Generally, they are grouped by _bytes_, with some extras, like parity bits or and synchronisation bits (start and stop). Usually, an asynchronous serial communication frame consists of a START bit (1 bit) followed by a data byte (8 bits) and then a STOP bit (1 bit), which forms a 10-bit frame as shown in the figure below. The frame can also consist of 2 STOP bits instead of a single bit, and there can also be a PARITY bit after the STOP bit.

![](https://www.electronicwings.com/public/images/user_images/images/Raspberry%20Pi/RaspberryPi_UART/1_PIC18F4550_USART_Frame_Structure.png)

!!! Note "More info"
	If you want to learn much more, you can visit [this tutorial](https://learn.sparkfun.com/tutorials/serial-communication/all)

**UART**

UART stands for _Universal Asynchronous Receiver/Transmitter_ and is the piece of hardware in charge of managing the data. Microcontrollers might have one, many or none UARTs:

![](https://i.imgur.com/TBTOzT2.png)

Sometimes it's also present in a hybrid way, as **UsART**, which implements both, synchronous and asynchronous communication.

!!! Note "No UART? No worries"
	Chips without UART still can implementing by bit-banging (using pins to send the data as voltage levels very quickly). The [SoftwareSerial](http://arduino.cc/en/Reference/SoftwareSerial) is for example used in this case.

**Libraries for Arduino**

- [Serial](https://www.arduino.cc/reference/en/language/functions/communication/serial/)

The most basic example of all time:

```
#include <Arduino.h>

void setup() {
	Serial.begin(9600);
	// OR, in some boards like the arduino Zero:
	SerialUSB.begin(115200);

	while (!Serial) {
		; // wait for serial port to connect. Needed for native USB port only
	}
}

void loop(){
	Serial.println("Hello Fabacademy");
}
```

- [SoftwareSerial](http://arduino.cc/en/Reference/SoftwareSerial)

Example using both, serial and software serial, when we only have one hardware serial comm:

```
#include <SoftwareSerial.h>
SoftwareSerial BT1(10, 11); // RX | TX

void setup() {
	
	Serial.begin(9600);
	
	BT1.begin(38400); // No need for it to be at the same speed!
}

void loop() {

	// Get something on BT1
	if (BT1.available()){
		// Read it and send it to Serial
		Serial.write(BT1.read());
	}
			
	// Get something on Serial
	if (Serial.available()) {
		// Read it and send it to BT1
		BT1.write(Serial.read());
	}
			
}
```

**Libraries for Python**

- [pyserial](https://pythonhosted.org/pyserial/)

```
import serial

PORT = '/dev/cu.usbmodem1421'
BAUDRATE = 115200

ser = serial.Serial(PORT, BAUDRATE)

print ser.readline().replace("\r\n", "")

ser.write('Hello')
```

### V-USB

[V-USB](https://www.obdev.at/products/vusb/index.html) is a library that allows to add USB connectivity to ATtiny microcontrollers (it was previously named tinyUSB).

!!! warning "USB 1.1"
	V-USB will create an USB 1.1 compliant low-speed device.

USB is comprised of 4 lines: Vcc, GND, D+ and D-. It is a [differential pair circuit](https://en.wikipedia.org/wiki/Differential_signaling) in which, **roughly**, the difference between D+ and D- is the actual signal we want to transmit.

![](https://www.electroschematics.com/wp-content/uploads/2010/01/usb-pinout.jpg)

The **data lines require 3.3V**, and for that reason we need to limit the voltage coming from many USB ports (computers, chargers, etc). Some ways to do this:

- Using an LDO in the Vcc
- Using zener diodes in the D+, D-

!!! Note "Great tutorials available here"
	Find a quite nice tutorial for the hardware [here](https://codeandlife.com/2012/01/22/avr-attiny-usb-tutorial-part-1/)
	And the actual implementation of V-USB [here](https://codeandlife.com/2012/01/29/avr-attiny-usb-tutorial-part-3/)

[V-USB](https://www.obdev.at/products/vusb/index.html) is a library that allows to add USB connectivity to ATtiny microcontrollers (it was previously named tinyUSB).

!!! warning "USB 1.1"
	V-USB will create an USB 1.1 compliant low-speed device.

!!! Note "Digested USB specification"
	Find it [here](http://www.beyondlogic.org/usbnutshell/usb2.shtml)

## Synchronous communication

When timing and speed are are important, and it's worth having more wires, we will use _synchronous communication_. This means that we will have a clock line that stablishes the rate at which data is transferred. Most common inter-chip communication is implemented like this.

!!! warning "Very important"
	I2C and SPI seen here are seen at a descriptive level. When facing a new design, sensor usage, etc. try to find already existing libraries from people like _Adafruit_, _Sparkfun_, _Arduino_ and others. Unless the set up is **super** simple, the implementation is not straight forward in many cases and it's better to follow a more *copy approach*.

### I2C

The Inter-integrated Circuit (I2C) Protocol is a protocol intended to allow multiple slave digital integrated circuits (chips) to communicate with one or more master chips. It is only meant for short distance communications within a single device. I2C is a convenient way of setting up communication because:

- It allows for several masters
- It allows for several slaves
- The number of wires does not change

It allows for several masters to be in the same system and for only the masters to be able to drive the data line high. This means that no slave will be able to lock the line in case other slave or master is talking:

![](http://archive.fabacademy.org/2018/labs/barcelona/students/oscar-gonzalezfernandez/images/10Week_I2C_NXP.jpg)
Image Source: _NXP semiconductors_

So, I2C bus consists of two signals (apart from VCC and GND): SCL and SDA. SCL is the clock signal, and SDA is the data signal. Each device is recognized by an **unique address** (whether it is a microcontroller, LCD driver, memory or keyboard interface) and can operate as either a **transmitter or receiver**, depending on the function of the device. In addition to transmitters and receivers, devices can also be considered as masters or slaves when performing data transfers. A master is the device which initiates a data transfer on the bus and **generates the clock signals to permit that transfer**. At that time, any device addressed is considered a slave.

!!! Note "More and more!"
	Check [this AAN](https://www.nxp.com/docs/en/user-guide/UM10204.pdf) by NXP semiconductors to learn much more about I2C.

!!! warning "Remember the pull-ups!"
	The data lines need to be driven high when they are not used, and for that they need pull-up resistors. Values for the pull up resistors could be around 5kΩ.

The actual protocol works with messages broken up into two types of frame: an address frame, where the master indicates the slave to which the message is being sent, and one or more data frames, which are 8-bit data messages passed from master to slave or vice versa. Data is placed on the SDA line after SCL goes low, and is sampled after the SCL line goes high.

![](https://cdn.sparkfun.com/r/600-600/assets/6/4/7/1/e/51ae0000ce395f645d000000.png)
Image Source: _Sparkfun_

!!! Note "Grove connector from Seeed"
	Grove, by Seeed, uses this connector as a way to interface with several protocols. This is how it looks for the I2C one:

	![](https://i.imgur.com/0xZ9LJ9.png)


!!! Note "Reference"
	[Adafruit i2c Addresses overview](https://learn.adafruit.com/i2c-addresses/overview)


**Libraries for Arduino**

- [Wire](https://www.arduino.cc/en/Reference/Wire)

!!! Note "Examples explained"
	The following examples are copied from [here](https://www.arduino.cc/en/Tutorial/MasterWriter) and [here](https://www.arduino.cc/en/Tutorial/MasterReader).

_Example 1 Master as receiver_: requesting information

Receiver:

```
#include <Wire.h>

void setup() {
	Wire.begin();        // join i2c bus (address optional for master)
	Serial.begin(9600);  // start serial for output
}

void loop() {
	Wire.requestFrom(8, 6);    // request 6 bytes from slave device #8

	while (Wire.available()) { // slave may send less than requested
		char c = Wire.read();    // receive a byte as character
		Serial.print(c);         // print the character
	}

	delay(500);
}

```

Sender:

```
#include <Wire.h>

void setup() {
	Wire.begin(8);                // join i2c bus with address #8
	Wire.onRequest(requestEvent); // register event
}

void loop() {
	delay(100);
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
	Wire.write("hello "); // respond with message of 6 bytes
	// as expected by master
}
```

_Example 2 Master as sender_: sending information

Sender:

```
#include <Wire.h>

void setup() {
	Wire.begin(); // join i2c bus (address optional for master)
}

byte x = 0;

void loop() {
	Wire.beginTransmission(8); // transmit to device #8
	Wire.write("x is ");        // sends five bytes
	Wire.write(x);              // sends one byte
	Wire.endTransmission();    // stop transmitting

	x++;
	delay(500);
}
```

Receiver:

```
#include <Wire.h>

void setup() {
	Wire.begin(8);                // join i2c bus with address #8
	Wire.onReceive(receiveEvent); // register event
	Serial.begin(9600);           // start serial for output
}

void loop() {
	delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
	while (1 < Wire.available()) { // loop through all but the last
		char c = Wire.read(); // receive byte as a character
		Serial.print(c);         // print the character
	}
	int x = Wire.read();    // receive byte as an integer
	Serial.println(x);         // print the integer
}
```

- [TinyWireS](https://github.com/rambo/TinyWire/tree/master/TinyWireS)

Example (for an analog pressure sensor that sends data via I2C):

```
#include <Arduino.h>
#include <TinyWireS.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

// Set I2C Slave address
#define I2C_SLAVE_ADDRESS 0x13

#ifndef TWI_RX_BUFFER_SIZE
#define TWI_RX_BUFFER_SIZE ( 16 )
#endif

// Sensor and Indicator Led Pins
#define SENSOR 1

// Measurement
#define MAX_TICK 50
unsigned int tick = 0;

//Smoothing Factor
#define LPF_FACTOR 0.5

volatile byte reg_position = 0;
const byte reg_size = sizeof(i2c_regs);
unsigned long lastReadout = 0;

// I2C Stuff
volatile uint8_t i2c_regs[] =
{
		0, //older 8
		0 //younger 8
};

void requestEvent()
{
	TinyWireS.send(i2c_regs[reg_position]);

	reg_position++;
	if (reg_position >= reg_size)
	{
			reg_position = 0;
	}
}

void setup() {
	analogReference(EXTERNAL);

	// Setup I2C
	TinyWireS.begin(I2C_SLAVE_ADDRESS);
	TinyWireS.onRequest(requestEvent);

	// set clock divider to /1
	CLKPR = (1 << CLKPCE);
	CLKPR = (0 << CLKPS3) | (0 << CLKPS2) | (0 << CLKPS1) | (0 << CLKPS0);
}

void loop() {
	unsigned long currentMillis = millis();

	// On tick value 0, do measurements

	if (abs(currentMillis - lastReadout) > MAX_TICK) {

		// Read the values
		int sensorReading = analogRead(SENSOR);

		// Treat them
		float Vs = 0;
		float pressure = 0;

		Vs = ((float) sensorReading  + 0.5 ) / 1024.0 * 5.0;
		pressure = (Vs*687.8/Va - 18.77); // in kPa

		i2c_regs[0] = pressure >> 8 & 0xFF;
		i2c_regs[1] = pressure & 0xFF;

		// Update the last readout
		lastReadout = currentMillis;
	}

}

```

**Libraries for Python**

- [sm.BUS](http://wiki.erazor-zone.de/wiki:linux:python:smbus:doc)

_Example 1, master as receiver_

```
import smbus
import time

bus = smbus.SMBus(1) # Indicates /dev/i2c-1
DEVICE_ADDRESS = 0x13
packet_size = 4

def ReadSensor(_address):

	i = 0
	_value = 0

	while (i < packet_size):

		_measure = bus.read_i2c_block_data(_address, 0, 1)

		_value |= _measure[0] << (8*(packet_size-(1+i)))
		i+=1

	return _value

while True:
		result = ReadSensor(DEVICE_ADDRESS)
		print result
		time.sleep(1)
```

_Example 2, master as sender_

```
import smbus

bus = smbus.SMBus(1)    # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)

DEVICE_ADDRESS = 0x15      #7 bit address (will be left shifted to add the read write bit)
DEVICE_REG_MODE1 = 0x00
DEVICE_REG_LEDOUT0 = 0x1d

#Write a single register
bus.write_byte_data(DEVICE_ADDRESS, DEVICE_REG_MODE1, 0x80)

#Write an array of registers
ledout_values = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]
bus.write_i2c_block_data(DEVICE_ADDRESS, DEVICE_REG_LEDOUT0, ledout_values)
```

!!! Note "WiringPi"
	If you are using a Raspberry Pi, you can use [WiringPi](https://projects.drogon.net/raspberry-pi/wiringpi/i2c-library/) (C++), instead of sm.BUS (Python).

!!! Note "More references"
	sm.BUS in python is not greatly documented with examples, but here you can find [some reference](https://raspberry-projects.com/pi/programming-in-python/i2c-programming-in-python/using-the-i2c-interface-2)

### SPI

Serial Peripheral Interface (SPI) is a synchronous serial protocol, used for short-distance communication, primarily in embedded systems.

![](https://i.imgur.com/083jT4a.png)

_Image Credit: SparkFun_

It uses a master-slave architecture with a single master. The master device originates the frame for reading and writing. Multiple slave-devices are supported through selection with individual slave select (SS) lines.

All these pins are explained in the SPI [tutorial](https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi) above. To summarize:

- In SPI, only one side generates the clock signal (usually called CLK or SCK for **S**erial **C**loc**K**). The side that generates the clock is called the “master”, and the other side is called the “slave”. There is always only one master (which is almost always your microcontroller), but there can be multiple slaves.

- When data is sent from the master to a slave, it’s sent on a data line called MOSI, for “Master Out / Slave In”. If the slave needs to send a response back to the master, the master will continue to generate a prearranged number of clock cycles, and the slave will put the data onto a third data line called MISO, for “Master In / Slave Out”.

- To select the Slave, we use the SS line, by dropping it, telling the particular slave to go active

![](https://cdn.sparkfun.com/assets/f/f/e/5/8/52ddb2dbce395fae408b4568.png)

_Image Credit: SparkFun_

We can summarize all this in the following image, but note we will be using a different Serial Communication on the PC side:

![](https://i.imgur.com/1ohG4RJ.png)

_Image Credit: ATMEL_

SPI is used by many programmers to flash the microcontroller's code onto the flash memory, mainly because it's fast, really fast. In many cases is the only protocol available, and in some other cases it's much faster than I2C.

!!! Note "Read the datasheet"
	For example, for the ATtiny, the **Section 19.5** explains all the necessary instructions for the Serial Programming. Below, the needed connections are summarized:

	|Symbol  |Pins  |I/O  |Description      |
	|:----:  |:---: |:--: |:----------------|
	| MOSI   | PA6  | I   | Serial Data In  |
	| MISO   | PA5  | O   | Serial Data Out |
	| SCK    | PA4  | I   | Serial Clock    |

A master can communicate with multiple slaves (however only one at a time). It does this by asserting SS for one slave and de-asserting it for all the others. The slave which has SS asserted (usually this means LOW) configures its MISO pin as an output so that slave, and that slave alone, can respond to the master. The other slaves ignore any incoming clock pulses if SS is not asserted. Thus you need one additional signal for each slave.

!!! Note "More info"
	- Check the [Serial Peripheral Interface - SPI](https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi), to understand why it's important to use **Synchronous** Serial communication.
	- Another great page by [gammon](www.gammon.com.au/spi). Probably the best of the best that you'll ever see

**Libraries for arduino**

- [SPI](https://www.arduino.cc/en/Reference/SPI)

_Example, arduino as master, sending_:

```
#include <SPI.h>

void setup (void)
	{
	digitalWrite(SS, HIGH);  // ensure SS stays high
	SPI.begin ();
	} // end of setup

void loop (void)
	{
	byte c;

	// enable Slave Select
	digitalWrite(SS, LOW);    // SS is pin 10

	// send test string
	for (const char * p = "Fab" ; c = *p; p++)
		SPI.transfer (c);

	// disable Slave Select
	digitalWrite(SS, HIGH);

	delay (100);
	} // end of loop
```

!!! Note "Looking to implement SPI?"
	Check the first response on [this thread](https://arduino.stackexchange.com/questions/16348/how-do-you-use-spi-on-an-arduino)
