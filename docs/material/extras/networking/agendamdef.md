# Weekly Agenda and Class links

#### Class Types*


  - **Local Classes**: This classes are given every Wednesday from 10 Am to 13.30 Pm (CET time)(mandatory)

  - **Global   Class**: This classes are given by Neil Gershenfeld every Wednesday at (16.30pm CET)(mandatory)

  - **LAB LIFE**: Student can come to the fabacademy room and ask for support to instructors.

  - **Local Project review** :This class/openworkshop  timeframe is to review the studio/challenge projects.(mandatory)

![](../../assets/agendamdef.jpg)


#### Weekly Schedule

  **Tuesday**: Students can book equipment of the lab and expect certain support on machine usage from 10pm to 13pm and 15h to 19h.

  **Wednesday - Morning** : Students participate in *Local Lectures* broadcasted from Barcelona  at 10:00 am – 13:30 pm CET (Barcelona time); these classes are mandatory.

  **Wednesday - Afternoon** : Students participate in the Local project review from 15:00 to 16:30  in MDEF Classroom and  Global Lectures broadcasted from MIT  at 16:30 am – 18:00 pm CET ( Barcelona time); these classes are mandatory.

      - FABACADEMY_Wednesday [https://iaac.zoom.us/j/93987967422?pwd=Ty9oUnZBWTVza0Z1Y0RGSWl5cVhDdz09](https://iaac.zoom.us/j/93987967422?pwd=Ty9oUnZBWTVza0Z1Y0RGSWl5cVhDdz09)
      - Meeting ID: 939 8796 7422
      - Passcode: Wednesday
      - (Local in room 301 or Main hall)
      -
    *Afternoon link*
      - Zoom Link : [https://zoom.us/j/89416278965](https://zoom.us/j/89416278965)
      - Meeting ID: 894 1627 8965
      - password: 1234



  **Friday** : Students can book equipment of the lab and expect certain support on machine usage from 10pm to 13pm and 15h to 19h.

    *This schedule may change subject to COVID-19 restrictions or other force majeure circumstances


    *The lectures are recorded and available to students throughout the semester.*
