## Weekly index

- [W1 - Principles and practices](material/week01.md)
- [W2 - Computer-aided design](material/week02.md)
- [W3 - Computer-controlled cutting](material/week03.md)
- [W4 - Electronics production](material/week04.md)
- [W5 - 3D printing and scanning](material/week05.md)
- [W6 - Electronics design](material/week06.md)
- [W7 - Computer-Controlled Machining](material/week07.md)
- [W8 - Embedded Programming](material/week08.md)
- [W9 - Molding and casting](material/week15.md)
- [W10 - Output Devices](material/outputs.md)
	+ [Local Class](material/extras/outputs/local.md)
	+ [DIY Outputs](material/extras/outputs/diy.md)
- [W11 - Mechanical/machine design](material/week17.md)
- [W12 - Break - Easter break](material/weekBreak.md)
- [W13 - Inputs Devices](material/inputs.md)
	+ [Local Class](material/extras/inputs/local.md)
	+ [DSP Basics](material/extras/inputs/dsp.md)
	+ [DIY Sensors](material/extras/inputs/diy.md)
- [W14 - Networking and communications](material/networking.md)
	+ Local Classes: [Introduction](material/extras/networking/local.md), [Wired](material/extras/networking/wired.md) and [Wireless](material/extras/networking/wireless.md)
	+ [What we learnt from onions](material/extras/networking/onions.md)
	+ [MQTT Basics](material/extras/networking/mqtt.md)
	+ [Examples](material/extras/networking/examples.md)
- [W15 - Interface and application programming](material/week12.md)
- [W16 - Wildcard Week](material/week16.md)
- [W17 - Applications and implications](material/week10.md)
- [W18 - Invention, intellectual property and income](material/week18.md)
- [W19 - Project development](course_info/fabacademy/what_is_it.md)
- [W20 - PROJECT PRESENTATIONS - JUN09](http://fabacademy.org/2021/projects.html)
