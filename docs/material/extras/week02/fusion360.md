# Fusion 360

![](https://i.imgur.com/RVKca8j.jpg)

https://www.autodesk.com/products/fusion-360/overview


## overview

- [Tinkercad](https://www.tinkercad.com/)
- [Slicer](https://apps.autodesk.com/FUSION/en/Detail/Index?id=8699194120463301363&os=Win64&appLang=en)

## TIPS

**Calibrate image:**
- https://f360ap.autodesk.com/courses/introduction-to-3d-modeling/lessons/lesson-8-parametric-modeling-import-and-calibrate-a-reference-image

**Components vs Bodies**
- https://www.autodesk.com/products/fusion-360/blog/5-things-you-should-know-about-components-and-bodies/
- https://www.autodesk.com/products/fusion-360/blog/quick-tip-body-v-component/

**How to Export as DXF File**
- [Refernec](https://www.youtube.com/watch?v=PuI2iWmngtM)
- https://www.youtube.com/watch?v=U4s2p2epaeg


**How to use Pluggins**
- https://apps.autodesk.com/FUSION/en/Home/Index
- https://www.youtube.com/watch?v=hpAGHGVRgnw


## REFERENCES & TUTORIALS

- https://www.autodesk.com/products/fusion-360/get-started
- https://academy.autodesk.com/
- https://www.onshape.com/
- https://hackaday.com/2016/02/02/making-parametric-models-in-fusion-360/
- [Tutorial for using Sketch Constraints in Fusion 360:](https://www.youtube.com/watch?v=J_2If5zVp84&t=0s)
- [Official Fusion 360 Youtube channel:](https://www.youtube.com/channel/UCiMwMz3RMbW5mbx0iDcRQ2g)
