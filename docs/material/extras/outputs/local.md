Output devices
==================

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/DoorBell_001.jpg/1920px-DoorBell_001.jpg)

![](https://upload.wikimedia.org/wikipedia/commons/9/95/Electric_Bell_animation.gif)

## Types of actuator

Today we will talk about the following three types of actuators. Basically we will convert electricity either into **motion**, **light**, or **sound**:

* **Electromagnetic**: they use an **induced magnetic field** to make some sort of actuation. We can characterise them by different types:
    - **Motors**: DC, Brushless, Servo or Steppers
    - **Solenoids**: that can be used in Relays or valves...
    - **Speakers**
* **Electroluminescence**: they use the properties or certain materials to emit light (releasing photons) when current goes through them
    * **LED**: RGB, Addressable...
    * **LCD**: (polarized light and liquid crystal)
* **Piezoelectric**: they are able to generate sound by the vibration of a piezoelectric disk when current is applied to them
    * Buzzers, speakers

!!! Note "Fabacademy outputs"
    Complete list [here](http://academy.cba.mit.edu/classes/output_devices/index.html)

## Electromagnetic outputs

![](https://3.bp.blogspot.com/_I19H4hXmpjM/TLvwTJWVD6I/AAAAAAAAAGY/dAo0fsqT2i0/s1600/43.JPG)

### Concepts

!!! Note "The best you can do"
    If you really want to learn about this, [read wikipedia](https://en.wikipedia.org/wiki/Electromagnetic_induction)

- **Induction**: when current goes through a wire, a magnetic field is generated around it. The higher the current, the greater the field
- **Magnetic attraction/repulsion**: similar poles repel each other (N-N, S-S, opposite poles attract each other (N-S)

All the electromagnetic (inductive) outputs work the same way: a magnetic field is induced by putting current through a wire and a magnetic body is attracted or repelled, the only difference is how they manage to achieve this.

![](https://itp.nyu.edu/physcomp/wp-content/uploads/motor-coils.png)

### Solenoids

Coils are simply a twisted wire that generate a magnetic field. If we put a metalic object that can move inside, it will:

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/VFPt_Solenoid_correct2.svg/500px-VFPt_Solenoid_correct2.svg.png)

### Practical stuff

#### Relays

Electromechanical relays: with small DC voltages, we can control a coil and open a switch - and then open AC or DC circuits:

![](https://homofaciens.de/bilder/technik/relay_001a.gif)
Source: _[Homofaciends.de](https://homofaciens.de)_

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.electronics-lab.com%2Fwp-content%2Fuploads%2F2016%2F06%2F4-Channel-Relay-Board-IMG.jpg&f=1&nofb=1)

On the Relay, it gives you: *control voltage for the coil*, *maximum output current* and *maximum output voltage* (that it can open-close):

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fs3-ap-southeast-1.amazonaws.com%2Fa2.datacaciques.com%2F17%2F06%2F29%2Fpx02wo2016or53n3%2F78a873f25098801b.jpg&f=1&nofb=1)

!!! warning "Not electromagnetic, but still a relay"
    SSRs (solid state relays) are another type of relays that do not rely on the coil but on an octocoupler.

    ![](https://static4.arrow.com/-/media/arrow/images/miscellaneous/0/0517-crydom-relay-and-optotransistor.jpg?h=262&w=607&la=en&hash=CDE25E6EB90A32AE2CF663A850693BA0B7331093)

!!! Note "Galvanic isolation"
    [Galvanic isolation](https://en.wikipedia.org/wiki/Galvanic_isolation)

[Comparison](https://www.arrow.com/en/research-and-events/articles/crydom-solid-state-relays-vs-electromechanical-relays):

- Electromechanical: makes noise, slower, ...
- SSRs: thermal management

![](https://static4.arrow.com/-/media/arrow/images/miscellaneous/s/solid-state-vs-electromechanical-relays-chart.jpg?h=704&w=700&la=en&hash=13BE47DBE6E34412F40974F5FD46001A569AB312)
_Source: [Arrow](http://www.arrow.com)_

### PWM

> PWM (Pulse Width Modulation) is a method of digitally controlling an output with a variable equivalent voltage. Essentially if you take the average of the signal over time then it has a varying analog level, however in the short term it is digital. This makes it easy to generate and efficient as transistors are most efficient when on or off rather than partially conducting. Most modern microcontrollers have the ability to generate PWM built in, including the Arduino and derivatives:
> 
> ![](https://nathandumont.com/files/images/quad_pwm.png)

_Source: [this](https://nathandumont.com/blog/h-bridge-tutorial)_

![](http://www.thebox.myzen.co.uk/Tutorial/Media/PWMan.gif)

_Source: [this](http://http://www.thebox.myzen.co.uk)_

### PID Controllers

!!! Note "Read wikipedia"
    https://en.wikipedia.org/wiki/PID_controller

<p><a href="https://commons.wikimedia.org/wiki/File:PID_en.svg#/media/File:PID_en.svg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/PID_en.svg/1200px-PID_en.svg.png" alt="PID en.svg"></a><br>By Arturo Urquizo - <a class="external free" href="https://commons.wikimedia.org/wiki/File:PID.svg">http://commons.wikimedia.org/wiki/File:PID.svg</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=17633925">Link</a></p>

### Motors

!!! Note "The best you can do"
    Let's learn to think, not to memorise.

- **How much torque** (somehow rotational force) the motor can give, depends on the motor's input current. Why? More current implies stronger magnetic field in the coils, and this means that more attraction/repulsion. 
- **How much current** a motor can tolerate depends on how thick the coils are (thicker implies more current and hence more torque). The thinner the coils, the greater resistance (and hence more heat - burning motor), and that depends on the construction of the motor
- **Always remember Newtown**: if there is no load, the motor will not make it up, it will only counteract what is given, drawing more current until it stalls. Every motor has a **stall current** - and they normally cannot handle it well for long time
- When we rotate the coil inside the magnetic field, a voltage is also generated in it (normally called back-emf Voltage - this is just physics, sorry). This back-emf voltage is related to the rotating speed, and all the other things being equal - means that the greater the **voltage we apply (counteracting the one back-emf one), the greater the speed**

#### Controlling Motors

- **Mosfets**: basically a transistor which can cut high currents or voltages but handled by a microcontroller. You can use PWM and it will modify the average voltage supplied and hence the speed (read above it it's not clear!):

!!! Note "Read here for a better explanation"
    https://reibot.org/2011/09/06/a-beginners-guide-to-the-mosfet/

!!! Note "Read here if you want to be a pro"
    https://lcamtuf.coredump.cx/electronics/#19

MOSFET stands for Metal-Oxide-Semiconductor Field Effect Transistors and they are a type of transitor commonly used in power electronics. Before jumping into the hardware implementation, I will detail here what I have learned about them.

**How it works**

How a MOSFET works is quite interesting thanks to the physics of semiconductors. Semiconductors are an intermediate case between conductive materials (mainly metals) and insulators. Semiconductors’ functions are described very nicely in [this link](https://lcamtuf.coredump.cx/electronics/#19) and, it is important to understand that the real magic for semiconductors in electronics occurs when they are doped: either with electrons or lack of them, they become a different type of material and their conductivity changes drastically. They are called N-type when they are doped negatively (with electrons) and P-type when they are doped positively (with electrons removal or addition of holes).

Moving electrons between N and P-types areas can be easy or tricky, depending on the direction they go to: from N-type to P-type areas is easy, and is very difficult the other way around. This property is key for transitors in general and is the base for their functioning.

More into detail, in the case of a MOSFET, we find a combination of three layers: N-P-N (with P-type layer sandwiched between the two N-types) and P-N-P (with N-type sandwiched between two P-types). The NPN is normally called N-channel and PNP is P channel. In the case of the N-channel, there is a layer of insulating material attached to the P-type semiconductor part, and attached to it we find the so called GATE. On the other sides (N terminals), we have the so called DRAIN and SOURCE, being this last one also connected to the non-insulated side of the GATE.

![](assets/MOSFET_principle.png)
_Source: Concise electronics for geeks_

When we apply voltage to the GATE in an NPN, we are attracting electrons on the GATE side of the P-type material, leaving a positive charge on the oposite side. This creates a channel between the two N-type layers. The larger the voltage we apply to the GATE, ideally, the larger the ammount of electrons we can have in the channel and therefore, the lower the resistance, meaning the MOSFET is ON. This means that if 0V are applied in the GATE, electrons will not group in the P-type material and the resistance will be too high and effectively it will create an open circuit, meaning the MOSFET is OFF.

This last part defines two very important parameters: the necessary voltage to turn ON the MOSFET and it’s resistance during that operation. The voltage is normally specified as Drive Voltage (or Vgs) and the resistance is normally specified as RDS(on). The latter is indeed very important, becase the power disipated by the MOSFET will be P = I^2 R, and the larger the resistance is, the larger the heat to disipate. Interestingly, this depends on the Voltage applied in the gate, and the minimum threshold is specified by Vgs (th).

Just to compile some more information about MOSFETS, it is important to know that they can either work as a normally-on or a normally-off switch. These are called depletion type or enhacement type and they are marked as a continuous or a dotted line between DRAIN and SOURCE:

- **Depletion type**: the transistor requires the Gate-Source voltage (Vgs) to switch the device “OFF”. The depletion mode MOSFET is equivalent to a “Normally Closed” switch.
- **Enhancement type**: the transistor requires a Gate-Source voltage, (Vgs) to switch the device “ON”. The enhancement mode MOSFET is equivalent to a “Normally Open” switch.

![](assets/MOSFET_TYPES.png)

**Where to put the MOSFET**

!!! Note "References"
    - http://www.vishay.com/docs/70611/70611.pdf
    - https://www.electronics-tutorials.ws/transistor/tran_7.html
    - https://www.re-innovation.co.uk/docs/open-charge-regulator/charge-controller-project-power-switching/
    - [Videos](https://youtu.be/UwzepcZQyQc)

![](assets/MODES_MOSFET.png)
Image Source: Vishay

If you use a P-channel MOSFET will have to run on the HIGH SIDE of the circuit and it will be easily triggered ON whenever a voltage below the Drive voltage (VDD = 12VDC in my case) is applied to the gate. However, in order to turn OFF the MOSFET, it would need at least the VDD voltage, which in some cases is not available in the circuit by itself, (for instance an Attiny that runs at 5V tops). There is another option to do this and is to use an N-channel MOSFET driven by a lower GATE Voltage which outputs to the GATE of a P-channel MOSFET. In this situation, the N-Channel MOSFET can be driven by a lower GATE voltage (for example coming from the Arduino) and it will Pull down the GATE of the MOSFET below VDD. When the N-channel MOSFET is turned OFF, the GATE of the P-Channel MOSFET will be switched back to VDD and the MOSFET will turn OFF.

![](assets/N-P_Channel_Level_Shifter.png)
Image Source: Vishay

**Cheatsheet**

![](https://reipooom.files.wordpress.com/2013/06/electronics-reference-sheet.png)
_Source: [akafugu.jp](https://akafugu.jp)_

![](https://i.imgur.com/kFhHwv3.jpg)

![](https://i.imgur.com/cIRinZX.jpg)

- **H-bridges**: Notice that if the current always goes the same way, the motor will rotate the same way. If we want to invert them, we use h-bridges:

![](https://cdn.sparkfun.com/assets/learn_tutorials/1/9/3/h-bridge-circuit-600w.gif)

You can build h-bridges with any **controlled switch: relays, transitors or you can buy an IC that contains them**. typical ones are [L298](http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf) (2A/channel, 2 channels or 4A 1 channel) and the [L293](http://www.ti.com/lit/ds/symlink/l293.pdf) (1A):

![](https://nathandumont.com/files/images/hbridge_l298.png)

![](https://i.imgur.com/wfHSsG7.jpg)

![](https://i.imgur.com/ZfaGMG9.jpg)

#### Steppers

![](assets/StepperHowiTworks.gif)

* Unipolar
* Bipolar
* [IC Component](https://www.digikey.com/product-detail/en/allegro-microsystems-llc/A4982SETTR-T/620-1340-1-ND/2237994)
* [Step stick](https://www.pololu.com/product/1182)

![](https://content.instructables.com/FJ1/KUXG/K30YAC6T/FJ1KUXGK30YAC6T.LARGE.jpg?auto=webp&frame=1&width=1024&height=1024&fit=bounds)

![](https://i.imgur.com/gzf5pUh.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/U2HRJoyK8es" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](https://simple-circuit.com/wp-content/uploads/2018/10/arduino-uinpolar-stepper-motor-control-uln2003a-circuit-1024x453.png)

![](https://i1.wp.com/www.circuitspecialists.com/blog/wp-content/uploads/2012/01/bipolar-vs-unipolar-stepper-motors-circuit-specialists-blog.gif?resize=451%2C305&ssl=1)

![](https://i.imgur.com/sObt2x4.jpg)

#### Servos

![](https://i.imgur.com/db4IDdg.jpg)

![](https://i.imgur.com/a2DAcQR.jpg)

![](https://i.imgur.com/ntOaYfM.jpg)

## Electroluminescence outputs basics

### Concepts

You can achieve light by _mainly_, two means:

**LEDs**
![](https://cdn.sparkfun.com/assets/a/0/8/8/5/51f1d073ce395f7120000002.gif)

_Source: Sparkfun_

1. More current - more light
2. Too much current - LED burnt
3. LEDs can have embedded chips inside (making them cycling and addressable)

!!! Note "Uberguide"
    [Great guide for addressable LEDs](https://learn.adafruit.com/adafruit-neopixel-uberguide)

!!! Note "LED strips"
    [SEEED guide](https://www.seeedstudio.com/blog/2019/02/14/ws2812b-vs-ws2813-addressable-rgb-led-strips-how-are-they-different/)

**EL Wire**

![](https://cdn.sparkfun.com/r/400-400/assets/learn_tutorials/3/5/5/10197-Action_EL_Wire.jpg)

1. Much less consumption than LEDs
2. They need inverter

### Practical stuff

#### LED
- For normal LEDs, you can use normal digital output pins and regulate the intensity with PWM

![](https://i.imgur.com/uug5r4L.jpg)

![](https://i.imgur.com/AjcJBRI.jpg)

![](https://i.imgur.com/i4KVRjS.jpg)

- For addressable LEDs, you can use: [FastLED](https://github.com/FastLED/FastLED) or [Neopixel](https://learn.adafruit.com/adafruit-neopixel-uberguide)

- To control many LEDs at the same time, you can use **charlieplexing**

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/3-pin_Charlieplexing_with_common_resistors.svg/1920px-3-pin_Charlieplexing_with_common_resistors.svg.png)

- To control **LEDs asynchronously** (or really any digital output), you can used [JLed library](https://github.com/jandelgado/jled).

#### EL Wire

![](https://cdn.sparkfun.com/r/400-400/assets/learn_tutorials/3/5/5/10197-Action_EL_Wire.jpg)

!!! danger "No batteries!" 
    EL Wire is powered with AC. For this, you need to get an [inverter](https://en.wikipedia.org/wiki/Power_inverter):
    <p><a href="https://commons.wikimedia.org/wiki/File:Squarewave01CJC.png#/media/File:Squarewave01CJC.png"><img src="https://upload.wikimedia.org/wikipedia/commons/e/ec/Squarewave01CJC.png" alt="Squarewave01CJC.png"></a><br>By <a href="https://en.wikipedia.org/wiki/User:C_J_Cowie" class="extiw" title="en:User:C J Cowie">C J Cowie</a> at the <a href="https://en.wikipedia.org/wiki/" class="extiw" title="w:">English language Wikipedia</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=52772789">Link</a></p>

!!! Note "Tutorial"
    Have a look here: https://learn.sparkfun.com/tutorials/getting-started-with-electroluminescent-el-wire/all

## Piezoelectric output basics

### Concepts

<p><a href="https://commons.wikimedia.org/wiki/File:2007-07-24_Piezoelectric_buzzer.jpg#/media/File:2007-07-24_Piezoelectric_buzzer.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/2007-07-24_Piezoelectric_buzzer.jpg/1200px-2007-07-24_Piezoelectric_buzzer.jpg" alt="2007-07-24 Piezoelectric buzzer.jpg"></a><br>By I, <a href="//commons.wikimedia.org/wiki/User:Gophi" title="User:Gophi">Gophi</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=2454869">Link</a></p>

!!! Note "More info here"
    [Tutorial](https://dmohankumar.wordpress.com/2012/05/01/familiarize-electronic-components-part-xiii-piezo-buzzer/#more-7427)

### Practical stuff

* [Arduino Tone function](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/)

![](https://www.arduino.cc/en/uploads/Tutorial/Tone_Fritzing.png)

* [Amplification](https://learn.sparkfun.com/tutorials/transistors/all#applications-ii-amplifiers)

## More practical stuff

### Batteries

Check [Edu's notes here]()

!!! Note "Guides"

    https://hackaday.com/2019/12/13/a-modular-system-for-building-heavy-duty-18650-battery-packs/

    https://hackaday.com/2019/06/12/an-exhaustive-guide-to-building-18650-packs/

### How to wire an output?

* We will always look for well documented output [learn.sparkfun.com](https://learn.sparkfun.com/), [learn.adafruit.com](https://learn.adafruit.com/)
* Download the EAGLES from the Breakout boards to help you [example](https://github.com/adafruit/Adafruit-TB6612-Motor-Driver-Breakout-PCB)
* Electronic recipes
    * [Voltage dividers](https://learn.sparkfun.com/tutorials/voltage-dividers/all)
    * [Power Transistors](https://learn.sparkfun.com/tutorials/transistors/all)
    * Integrated Circuits: [H-Bridges](https://en.wikipedia.org/wiki/H_bridge), Darlington Arrays...

### How to code for an output?

* Output interfaces:
    * On/Off: [DigitalWrite](https://www.arduino.cc/reference/en/language/functions/digital-io/digitalwrite/)
    * Pulse With Modulation: [AnalogWrite](https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/)
    * [Pulses](https://www.arduino.cc/reference/en/language/functions/advanced-io/pulsein/)
    * [Digital Communication](https://learn.sparkfun.com/tutorials/serial-communication/all): [Serial / UART](https://www.arduino.cc/en/reference/serial), [SPI](https://www.arduino.cc/en/Reference/SPI), [I2C](https://www.arduino.cc/en/reference/wire)

!!! warning "No need to be a hero"
    We will always use a specific library implementing what we want.

## Debugging inputs and outputs

* Divide and conquer.
* Always subdivide the system as much as posible.
* Metodic tests

### Hardware

* Prototyping before building
![](http://3.bp.blogspot.com/-OlQldAQqZq0/TyeizTbznNI/AAAAAAAAAEU/E8HemqLdZqg/s1600/IMG_0633.JPG) vs ![](https://i.imgur.com/ZfLc9OQ.jpg)
* Multimeter checks
* Continuity before and after soldering components
* Voltages
* Isolating hardware parts, cutting traces, solder bridges, jumpers, designing for debug.
* Unconsistent tests --> heat or power problems.

### Software

* Creating small sketches for testing specific parts
* Using Serial print.
* Using defines to build a better Serial Print debug
* Using comments to disable code
* Structuring the code in functions
* Always review compiler error messages.
* Get someone else to review your code
