# Computer-controlled cutting

## Classes

### Global Class

- [Computer-controlled cutting](http://academy.cba.mit.edu/classes/computer_cutting/index.html)

!!! Note "Assignments and assessment"
    **Fabacademy students**: visit the [assessment](https://fabacademy.org/2021/docs/fabacademy-assessment/computer-controlled_cutting.html)

### Local Class

**LASER CUTTER (CAM)**

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT0BWKXFiqYcHg4tUMDiM3hMuyBG2y06cQ3ZkR2KtF7eSboRzuctbTIlKNvAoafyd2tVD9JgNqZ04Ut/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="420" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


#### Computer-control Laser/Vinyl Equipment Usage Videos

<iframe width="560" height="315" src="https://www.youtube.com/embed/5GTeIffOlzg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/8oCOzy_Zx2o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Materials

[Laser_Cutter_Materials](http://atxhackerspace.org/wiki/Laser_Cutter_Materials)

**NEVER CUT THESE MATERIALS**
![](extras/week03/assets/week03-82b9f3a8.png)

**Safe Materials**
![](extras/week03/assets/week03-1d852fd4.png)

 - [WHY YOU SHOULD TRY TO AVOID MDF](https://www.hse.gov.uk/woodworking/faq-mdf.htm)
**Biomaterials Cut**

[Cutting Pasta on lasercutter](https://shiraczerninski.wixsite.com/thepastaproject)

![](extras/week03/assets/pasta.jpg)

## Laser types

[List of laser types](https://en.wikipedia.org/wiki/List_of_laser_types)

![](extras/week03/assets/week03-e0901ab6.png)

## Tutorials

### Vinyl Cutter

 * [How to use a vinyl cutter](http://archive.fabacademy.org/archives/2016/doc/vinyl_cutter.html)
 * [How to use vinyl cutter for PCB - how to use SILHOUETTE CAMEO](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/flexible_pcb_windows_mac.html)
 * [How to use Roland GX-24 vinyl cutter - short guide](http://wiki.fablab.is/wiki/How_to_use_the_Roland_GX-24)
 * [How to use Roland GX-24 vinyl cutter - extended guide](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week03_computer_controlled_cutting/rolandgx24.html)

### MULTICAM 2000 Laser Cutter (2D CAM)

Please, visit
- [FAB LAB BCN Wiki](http://wiki.fablabbcn.org/Multicam_Laser_Cutter) :
- [LASERCUT STEP BY STEP -PROTOCOL GUIDE]

* *technical specifications*

* *materials and configuration ( wood, playwood, cardboard... )*

* *how to prepare and send files*

* *how to turn on the machine*

* *how to cut*

* *what not to do*

** EnRoute Software **

1. Learn EnRoute: [Youtube EnRoute Channel](https://www.youtube.com/user/EnRouteCNCSoftware/playlists)

** Downloads **

1. [EnRoute Instructions](http://wiki.fablabbcn.org/images/d/d6/EnRoute_Instructions.pdf)

2. [Multicam Instructions](http://wiki.fablabbcn.org/images/2/2b/MultiCam_Instructions.pdf)

3. [Multicam Material Settings](http://wiki.fablabbcn.org/images/0/09/MultiCam_Settings.pdf) *only suggestions!!!!*

### Trotec Speedy 400 (2D CAM)

- [Trotec Speedy 400 - Menual](https://www.troteclaser.com/fileadmin/content/images/Contact_Support/Manuals/Speedy-400-Manual-EN.pdf)
- [LASERCUT STEP BY STEP - PROTOCOL GUIDE](https://drive.google.com/file/d/15n6L-T7Xf9T2ejN9rBHu0hW_4uYi5NBh/view?usp=sharing)

Please, visit [FAB LAB BCN Wiki](https://wiki.fablabbcn.org/Category:Machines) for:

* *technical specifications*

* *materials and configuration ( wood, playwood, cardboard... )*

* *CAM*

* *machine workflow (how to cut)*

* *commands*

### Spirit GE Laser Cutter

[Spirit GE Laser Cutter - Menual](http://www.laserprona.com/wp-content/uploads/2014/11/Spirit-GE-User-Manual.pdf)

Please, visit [FAB LAB BCN Wiki](https://wiki.fablabbcn.org/Spirit_GE_Laser_Cutter) for:

* *technical specifications*

* *materials and configuration ( wood, playwood, cardboard... )*

* *CAM*

* *machine workflow (how to cut)*

* *commands*

----

## Resources

### Example Documentation

**Student's documentation:**

* [STAR STUDENT DOCUMENTATION](http://archive.fabacademy.org/2018/labs/barcelona/students/aitor-aloa/computer_controlled_cutting.html)
* [GOOD STUDENT DOCUMENTATION](http://fabacademy.org/archives/2015/eu/students/haldin.anders/week3.html)

**Other Resources:**

* [Massimo Menichinelli - Laser Cutting lecture](http://archive.fabacademy.org/archives/2016/doc/laser-cutting-milan.html)
* [HOW TO LASERCUT LIKE A BOSS](https://lasercutlikeaboss.weebly.com/)
* [Paper Models](https://feadi.github.io/)
* [How to use a caliper](https://www.youtube.com/watch?v=FNdkYIVJ3Vc)
 ![](https://hcie.csail.mit.edu/research/laserorigami/images/laserorigami-brandenburg-gate.png)
* [Bending Plastic in lasercutter](https://hcie.csail.mit.edu/research/laserorigami/laserorigami.html)
### Softwares:

- [Sketchair](http://sketchchair.cc/) is a free, open-source software tool that allows anyone to easily design and build their own digitally fabricated furniture.
- [Pepakura](http://www.tamasoft.co.jp/pepakura-en/)

**Laser Safe Font**

- [Stencilfy](http://stencilfy.herokuapp.com/)

**Templates for geometries**

- [Paper base geometries-boxes](https://www.templatemaker.nl/en/)
- [Make boxes and complex geometries](https://www.festi.info/boxes.py/?language=en0)
- [Maker case - box maker](https://es.makercase.com/#/basicbox)

**Origami:**

- [tt](http://www.tsg.ne.jp/TT/software/)
- [Origami simulator](https://origamisimulator.org/)
- [treemaker](http://www.langorigami.com/article/treemaker)

**Multiple Tools -Machine Software**

 - [Fabmodules](http://fabmodules.org/)
 - [Visicut](https://hci.rwth-aachen.de/visicut)
 - [inkcut](http://inkcut.sourceforge.net/)
 - [INKCUT](https://www.codelv.com/projects/inkcut/)
 - [flatfab](http://flatfab.com/)
 - [BoxMaker: A free tool for creating boxes using tabbed construction](https://github.com/paulh-rnd/TabbedBoxMaker)
 - [Inkscape living hinge plugin](https://github.com/drphonon/Inkscape_LivingHinge)
 - [Inkscape SILHOUETTE CAMEO plugin](https://github.com/fablabnbg/inkscape-silhouette)
 - [svgnest](https://svgnest.com/)
 - [deepnest](https://deepnest.io/)

### Inspirational Ideas

  - [Lasersaur, an open source laser cutter](http://www.lasersaur.com/)
  - [Monsters](http://georgehart.com/sculpture/monsters.html)
  - [Straw Bees](http://strawbees.com/)
  - [Open Source straw bees](http://archive.fabacademy.org/archives/2016/fablabbcn2016/students/4/exercise03.html)
  - [Rubik's Snake](https://en.wikipedia.org/wiki/Rubik's_Snake)
  - [cardboard-surfboard](http://gizmodo.com/you-can-build-a-cardboard-surfboard-that-holds-up-to-wa-1639087805)
  - [Karton](http://kartongroup.com.au/)
  - [20-creative-cardboard-lamp-ideas](http://www.designrulz.com/design/2015/02/diy-20-creative-cardboard-lamp-ideas/)
  - [FAB MIT](http://fab.cba.mit.edu/)
  - [Thingiverse](http://www.thingiverse.com/thing:4764)
  - [Laser origami](http://fffff.at/free-universal-construction-kit/)
  - [Press-fit construction](https://de.pinterest.com/karstennebe/press-fit-construction/)
  - [Flat Fab](http://flatfab.com/)
  - [Obrary](https://obrary.com/products/living-hinge-patterns?variant=798259727)
  - [Paper Sculptures](http://www.bertsimons.nl/portfolio/papersculptures/)

### Test files

  - [Test](http://www.thingiverse.com/thing:728579)
  - [Test2](http://www.thingiverse.com/thing:17327)
