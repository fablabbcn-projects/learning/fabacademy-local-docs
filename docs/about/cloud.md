# Access to IaaC Cloud

[IAAC Cloud](https://cloud.iaac.net:5001/) is a small cloud storage used to transfer and store files and designs during your stay in Fablab BCN, avoiding the usage of USB Drives in the lab computers.

!!! Note
    Please, contact with your local instructors to access to the cloud and create your own student folder inside:

    `FILE STATION > FAB STUDENTS > FabAcademy_{{ year }}`

    - User name : fab
    - Password: fab
