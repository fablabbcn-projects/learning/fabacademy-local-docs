# Fablab BCN - IAAC Campuses

![](../assets/fablab.jpeg)

## Where

### [Fab Lab Barcelona](https://fablabbcn.org/) [Virtual360 Tour](https://360.iaac.net/)


<iframe width="560" height="315" src="https://360.iaac.net/" title="360 FabLab BCN tour" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Address: [Pujades 102 baixos. Poble Nou, Barcelona, 08005, Spain](https://goo.gl/maps/zzs55mQxnQ52)

[![Fab Lab BCN Map](https://fablabbcn.github.io/fabacademybcn/img/fablabbcn_map.png)](https://goo.gl/maps/zzs55mQxnQ52)

### [Green Fab Lab](http://greenfablab.org/)

- Address: [Ctra. BV-1415 (Horta-Cerdanyola), km 7 08290 Cerdanyola del Vallès Barcelona](https://www.google.com/maps/d/u/0/viewer?mid=1NrFJXn65BasK1-tpXGakKDWBPBc&ll=41.44266990282529%2C2.144425500000011&z=15)

[![Green Fab Lab Map](https://fablabbcn.github.io/fabacademybcn/img/greenfablab_map.png)](https://www.google.com/maps/d/u/0/viewer?mid=1NrFJXn65BasK1-tpXGakKDWBPBc&ll=41.44266990282529%2C2.144425500000011&z=15)

### [IaaC Atelier](https://iaac.net/)  [Virtual360 Tour](https://fablabbcn-projects.gitlab.io/iaac-atelier-virtual-360-tour/)

- Address:  [Pujades 59 baixos. Poble Nou, Barcelona, 08005, Spain](https://goo.gl/maps/ykDDJgFdeyka5Wkc8)

[![IaaC Atelier](https://fablabbcn.github.io/fabacademybcn/img/fablabbcn_map.png)](https://goo.gl/maps/zzs55mQxnQ52)

## Who

* **Fab Lab Barcelona Coordinator**:
    - Luciana Asinari: <luciana.asinari@iaac.net>

* **Local Instructors**:
    - Santiago Fuentemilla: <santi@fablabbcn.org>
    - Eduardo Chamorro: <eduardo.chamorro@iaac.net>
    - Xavi Domínguez: <xavi@fablabbcn.org>
    - Óscar González: <oscar@fablabbcn.org>
    - Josep Marti: <josep@fablabbcn.org>

* **FabAcademy Email Alias**:
    - Barcelona class: <academy@fablabbcn.org>

## How

**Access to the Fab Lab Bcn**

As a student, you will have:

- an electronic access card to access the building (monetary deposit)
- free WiFi access
- access to the Fab Lab BCN facilities/machines

**Opening Hours**

!!! warning "Double-check"
    Please, make sure these guidelines are valid with Coordination due to COVID-19 pandemic

- Students have access to the Fab Lab Bcn and the Fab Academy Classroom from Monday to Friday, 10:00hs to 19:00hs.
- Prior agreement with Green Fab Lab Coordination, students may use the facilities in our Valldaura Labs.
- Saturdays and Sundays, the lab is closed.

!!! danger "Very important"
    It is absolutely forbidden to stay at IaaC overnight

**Machine Tests**

!!! warning "For anyone wanting to use the lab"
    **[FABLAB PROTOCOL](http://rules.fablabbcn.org/)**

- [Test for using Laser Cutting Machines](https://docs.google.com/forms/d/e/1FAIpQLScgRrkBb5JOtW_vhQUCHzT6of2vkIXgqccfY8HtWmpQAJoiZA/viewform)
- [Test for using 3D Printing Machines](https://docs.google.com/forms/d/e/1FAIpQLSeWUSLsVOIRUqUH5jy4TPGNLeR7kpCzxqRd4wLgQwPIfA7hOw/viewform)
![](/assets/3d.jpg)
- [Test for using CNC Machines](https://docs.google.com/forms/d/e/1FAIpQLScu7YlGbV15AOFY176b3x9_DFryYz129yTzRwUP9YcpGK88xg/viewform)
![](/assets/cnc.jpg)

!!! danger "Most important rules of the lab"

    - **Clean up your mess!**
    - **Update the electronics inventory if you take components**
