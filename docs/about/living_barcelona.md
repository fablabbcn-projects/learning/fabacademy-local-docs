Some helpful information when you come to Barcelona from other places.

![](assets/barcelona.jpg)

## Getting around with paperwork

* [NIE Guide for EU Students](https://drive.google.com/open?id=0B1WvpUt-D07aRnFMZTFmcXc3S2tOOHdsZ1REc3VEUHV6MktZ)
* [TIE Appointment](https://drive.google.com/open?id=0B1WvpUt-D07aYmdYYk9ua3BrRjA0MUxmaXh1TGhjSThTdTU4)
* [TIE Guide for NON-EU Students](https://drive.google.com/open?id=0B1WvpUt-D07aYmdYYk9ua3BrRjA0MUxmaXh1TGhjSThTdTU4)
