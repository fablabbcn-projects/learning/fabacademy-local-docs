# Booking system

![](../assets/fablab.jpeg)

!!! warning "For anyone using the Fablab BCN Facilities"
    [FablabBCN Protocol](http://rules.fablabbcn.org/)

!!! tip "Go book it"
    - [Visit Booking System](https://fablabbcn.simplybook.me/index/about)
    - **Username: student name**
    - **password: surname**

!!! example "Step-by-step"

    1. Go to [Booking System](https://fablabbcn.simplybook.me/index/about).
    2. Select Service: the amount of time you are booking
    3. Select Provider: the machine you want to use
    4. Select the Date: only the available slots will show.
    5. Personal data: we need that to send a confirmation of the appointment and the link to cancel the slot.
    6. To finish, click BOOK NOW

## Things to consider

- Students must come to the Fab Lab 15 min before their slot time starts in order to load the file for fabrication.
- Students can book with 5 weeks advance maximum and a minimum of 24 hours before.
- If a student makes a reservation and does not come, or comes late, the student will lose the slot and may be restricted for further use
- Only in force majeure cases, students can cancel their booking using the cancellation link in their confirmation email.
- The student may only use the machine for the time booked.