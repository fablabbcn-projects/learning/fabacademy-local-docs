## Classes Calendar

!!! Note
    * [Add Fab Academy Barcelona Calendar](https://calendar.google.com/calendar/embed?src=fablabbcn.org_tnser77b14o852cnqjarn3ce3g%40group.calendar.google.com&ctz=Europe%2FMadrid)
    * [Class Schedule](http://fab.academany.org/202/schedule.html)


## GLobal FabAcademy Links

- [Links compilation](https://fabl.ink/)
- [FAB Labs Community (fablabs.io)](https://www.fablabs.io/labs)
- [Academany](http://docs.academany.org/)
- [Fabacademy Inventory](https://docs.google.com/a/fablabbcn.org/spreadsheets/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html)
- [Fab Foundation](https://fabfoundation.org/)
- [SCOPES DF Project](https://www.scopesdf.org/)
- [Fab Event](https://fabevent.org/)
- [Fabacademy](http://fabacademy.org/)
- [Fab Academy Staff](https://gitlab.fabcloud.org/academany/fabacademy/2019/staff)
- [Bluejeans/pin](https://bluejeans.com/u/academany/)
- [Jobs](http://jobs.fabeconomy.com/)

## Projects in Fablab Barcelona

A compilation of some projects in Fablab Barcelona (related or not to Fabacademy itself):

- [FablabBCN](https://fablabbcn.org/)
- [SmartCitizen](https://smartcitizen.me)
- [CNC-Machines](https://gitlab.com/fablabbcn-projects/cnc-machines)
- [Electronics and Barduino](https://gitlab.com/fablabbcn-projects/electronics)

## Previous Fablab Barcelona Documentation

From 2021 on, the documentation will be the same every year. However, you can still find previous years below:

- [2020](http://fab.academany.org/2020/labs/barcelona/local/)
- [2019](http://fab.academany.org/2019/labs/barcelona/local/)
- [2018](http://fab.academany.org/2018/labs/barcelona/local/)
- [2017](https://fablabbcn.github.io/fabacademybcn/#/)
