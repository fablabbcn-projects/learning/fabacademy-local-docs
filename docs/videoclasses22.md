This section is a compilation of the video classes given by the Fab Lab Barcelona team, for the Fabacademy / MDEF courses


<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Firefly - Kinect</h2>
        <h3 class="class">[3D printing and scanning]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/WFQLSY9NmDg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">General Concepts & Kicad</h2>
        <h3 class="class">[Electronics design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/-_Ww6-VPfP0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

</div>



<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">3D Printing Paste & Photogrametry</h2>
        <h3 class="class">[3D printing and scanning]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/T4qJKZOWU8o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">Overview & General Concepts</h2>
        <h3 class="class">[3D printing and scanning]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/ZfrgBuCVfgo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

</div>




<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">MODS</h2>
        <h3 class="class">[Electronic Production]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/ikoO7qGf4ag" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">What is a microcontroller?</h2>
        <h3 class="class">[Electronic Production]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/s08JDfM1zS4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

</div>



<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Overview & General Concepts</h2>
        <h3 class="class">[Electronic Production]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/AdEfqPQ53jM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">Rhino tips for Laser cut</h2>
        <h3 class="class">[Computer-controlled cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/bSoLYqWG738" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

</div>


<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">MDEF review + Parametric class</h2>
        <h3 class="class">[Computer-controlled cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/rV26JKDbYvo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">CCC Overview</h2>
        <h3 class="class">[Computer-controlled cutting]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/4_IVxQ89wnA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>


<div class="iframe-container">
    <div class="iframe-item">

        <h2 class="video">Rhinoceros</h2>
        <h3 class="class">[Computer-Aided Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/ohebeXrUCzo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="iframe-item">

        <h2 class="video">Fusion 360</h2>
        <h3 class="class">[Computer-Aided Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/sA6VWSZqhEo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    </div>

</div>

<div class="iframe-container">

    <div class="iframe-item">

        <h2 class="video">Blender Introduction</h2>
        <h3 class="class">[Computer-Aided Design]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/M1J3qLWVCzU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>


    <div class="iframe-item">

        <h2 class="video">Static Side Generators (SSGs)</h2>
        <h3 class="class">[Project management]</h3>

        <iframe height="200" src="https://www.youtube.com/embed/njnxazm1xsc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

  </div>

</div>
